// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "AppOn/SprayShader" {
Properties {
    _MainTex ("Texture", 2D) = "white" { }
    _CanColor ("Can Color", Color) = (0,0,0,1)
}
	CGINCLUDE
	#include "UnityCG.cginc"

	struct v2f {
		float4 pos : POSITION;
		float2 uv[2] : TEXCOORD0;
	};
	

	v2f vert( appdata_img v ) { 
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);
		o.uv[0] =  v.texcoord.xy;			// Brush UV
		o.uv[1] =  (o.pos.xy + 1.0) / 2.0;	// Bg UV
		return o;
	}
    
	uniform sampler2D _MainTex;
	uniform half4	  _CanColor;

	
	half4 frag (v2f i) : COLOR  
	{
		half4 tex_color   = tex2D(_MainTex, i.uv[0]);

		half4 result = _CanColor*tex_color.r;
		result.a 	 = tex_color.a;

		return result;
	}
	ENDCG
	
	SubShader {
        Tags { 
            "Queue"="Transparent" 
            "IgnoreProjector"="True" 
            "RenderType"="Transparent" 
        }
		
        Pass {
			//Blend color channel using default, but alpha channel with max
			Blend SrcAlpha OneMinusSrcAlpha, OneMinusDstColor One  
			//BlendOp Add, Max
			ZTest always
			CGPROGRAM
      		#pragma fragmentoption ARB_precision_hint_fastest
      		#pragma vertex vert
      		#pragma fragment frag
      		ENDCG
			
        }
    }
    
	FallBack off
}

