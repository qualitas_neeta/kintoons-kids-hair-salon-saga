using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using UnityEngine.UI;
using UnityEngine.Audio;
using System.Xml.Linq;
#if firebase
using Firebase.Analytics;
#endif
namespace AppOn.HairGame
{
	public class AppOn_HairGame : MonoBehaviour 
	{
		//rutuja
		//character
		public const  int CHARACTER_JANET=0;
		public const  int CHARACTER_PAULO=2;
		public const  int CHARACTER_MERLE=3;
		public const  int CHARACTER_UGA=1;


		//spray color
		public const  int COLOR_PINK=0;
		public const  int COLOR_BLACK=1;
		public const  int COLOR_PURPLE=2;//slate blue
		public const  int COLOR_GREEN=3;
		public const  int COLOR_RED=4;// harley_devidson_orange
		public const  int COLOR_BROWN=5;//CHOCOLATE
		public const  int COLOR_BLUE=6;//maya_blue
		public const  int COLOR_BLONDE=7;//Corvette

		//
		public const  int HAIR_STYLE_EQUIPMENT_1=0;//comb,shaver,hair grow,scissors
		public const  int HAIR_STYLE_EQUIPMENT_2=1;//drier,hairwash,straightener,curler
		public const  int HAIR_STYLE_ACCESSORIES_4=2;//clips and hats,goggles
		public const  int HAIR_STYLE_EQUIPMENT_3=3;//hair color

		public GameObject drierCollision;

		public static int currentCapIndex = 0;
		public static int currentGlaresIndex = 0;
		public static int currentClipIndex = 0;
		//end rutuja 

		public static  AppOn_HairGame Instance;
		public List<Texture> curlyHairTexture;
		public List<Texture> straightHairTexture;
		public DragDrop script;
		private GameObject 						  _hair_root;
		public List<Material>   				  _hair_material = new List<Material>();
		private Vector3    						  _cur_start_pt;
		private Vector3    						  _cur_end_pt;
		private List<List<AUPathMesh.VertexData>> _hair_meshes = new List<List<AUPathMesh.VertexData>>();

		private List<GameObject>				  _hair_gos	   = new List<GameObject>();
			
		
		public string 	   						  _cur_hair_style = "None";
		private bool	   						  _gui_button_clicked = false;
		private List<AUPathMesh.HitInfo>  		  _hit_meshes  = new List<AUPathMesh.HitInfo>();
		private List<GameObject> 				  _cut_hair_gos = new List<GameObject>();
		
		public Color32 _hair_color;
		private bool							  _show_color_picker = false;
		
//		private Vector3							  _cur_offset_pt;
		private UnityEngine.Object				   _hair_color_particle_effect_prefab;

//		private List<GameObject>				  _all_color_tools_go	   	= new List<GameObject>();
		private List<GameObject>				  _available_color_tools_go	= new List<GameObject>();
//		private static List<string>				  _color_tools_names	    = new List<string>();
//		private static int						  m_transit_type	= 0;
//		private Texture2D						  _curl_hair_texture; 
//		private Texture2D						  _original_hair_texture;
		private AudioSource 					  _dryerAudioSource;
		private bool							  _dryerAudioStopLoop = false;

		private Vector2 						  _previousMouseClick;

		public Button 							  growButton;
		public Button 							  cutButton;
		public Button 							  combButton;
		public Button 							  combButton2;
		public Button 							  trimButton;
		public GameObject defaultTool;
		public int 					      colorInt;
		public float							  radius;
		public Transform 						  center;
		public int angleAdder;
		public float angleDefaultValue;
		public Vector2 hair_pt;
		public GameObject hair_bottom;

//		public Animator MerleAnim;
//		public Animator PaoloAnim;
//		public Animator JanetAnim;
//		public Animator UGAAnim;


		private List<AUPathMesh.VertexData>       _input_points = new List<AUPathMesh.VertexData>();

		private GameObject                           _cur_hair_go;
		private float _hair_half_width=22;

		public static string filePath;

		public string characterHairFileName;
		public static int templateIndex;
//		public Text StrPath;
		public static void LoadResources(string resources_name, System.Action<UnityEngine.Object> callback) 
		{
			UnityEngine.Object result = Resources.Load(resources_name);
//			if (result == null)
//				Debug.LogError ("Asset not found at path: " + resources_name);

			callback(result);	
		}
		// Use this for initialization
		void Awake ()
		{
			
			Instance = this;
			#if UNITY_EDITOR
			filePath = Application.dataPath+Path.DirectorySeparatorChar+"Game"+Path.DirectorySeparatorChar+ "HairGame"+Path.DirectorySeparatorChar+"Resources";
			#else
			filePath	= Application.persistentDataPath;
			#endif

//			Debug.Log (filePath);
			_hair_root = GameObject.Find("hairs");
//			_character_hair_go = GameObject.Find("2_hairgame_character_hair");
//			_dryerAudioSource  = GameObject.Find("34_AudioDryer").GetComponent<AudioSource>();	

			int Avatar_No = GameManager.Avatar_No;
			templateIndex = 0;
			if (Avatar_No == 0) {
				LoadResources ("AurBkEngCrazyHair_original_hair", //LoadResources ("AurBkEngCrazyHair_original_hair"
					val => {
//									Debug.Log("finish load AurBkEngCrazyHair_original_hair"); 
						_hair_material.Add ((Material)val);
						 LoadDefaultHair ();
					});
			}

//			LoadResources ("", val => {
//			});
//			LoadResources ("AurBkEngCrazyHair_original_hair_1", 
//				val => {
//														Debug.Log("finish load AurBkEngCrazyHair_original_hair"); 
//					_hair_material.Add ((Material)val);
//					LoadDefaultHair ();
//				});
//			LoadResources ("AurBkEngCrazyHair_original_hair_2", 
//				val => {
//														Debug.Log("finish load AurBkEngCrazyHair_original_hair"); 
//					_hair_material.Add ((Material)val);
//					LoadDefaultHair ();
//				});
//			LoadResources ("AurBkEngCrazyHair_original_hair_3", 
//				val => {
//														Debug.Log("finish load AurBkEngCrazyHair_original_hair"); 
//					_hair_material.Add ((Material)val);
//					LoadDefaultHair ();
//				});

//			LoadResources( "AurBkEngCrazyHairDay_hairgame_colorparticle_prefab", 
//							val=>{
////									Debug.Log("finish load AurBkEngCrazyHairDay_hairgame_colorparticle_prefab"); 
//									_hair_color_particle_effect_prefab = (UnityEngine.Object)val;
//								});

//			LoadResources( "curlyhair", 
//			LoadResources( "Original_pimgpsh_fullsize_distr-1a", 
//									val=>{Debug.Log("finish load curlyhair");_curl_hair_texture = (Texture2D)val;});
//			
//			LoadResources( "spike", 
//									val=>{Debug.Log("finish load spike");_spike_hair_texture = (Texture2D)val;});
//			
//			LoadResources( "original_hair_texture", 
//			LoadResources( "original_pimgpsh_fullsize_distr-2a",
//									val=>{Debug.Log("finish load hair");_original_hair_texture = (Texture2D)val;});

		}

		void Start()
		{
			_cur_hair_style = "None";
//			Instance = this;
			StartCoroutine ("LateStart");
//			DragBottomPanel.Instance.init ();
		}

		public void LateStart(){
			if ((GameManager.Avatar_No == CHARACTER_JANET  && GameManager.BoughtJanet) || (GameManager.BoughtUga && GameManager.Avatar_No==CHARACTER_UGA) || (GameManager.BoughtPaulo && GameManager.Avatar_No==CHARACTER_PAULO) || (GameManager.Avatar_No==CHARACTER_MERLE ) ) {
//					CombHair (false);
			}
		}

//		public void LoadHair1111(){
//			LoadResources( "AurBkEngCrazyHair_original_hair", 
//				val=>{
//					//									Debug.Log("finish load AurBkEngCrazyHair_original_hair"); 
//					_hair_material.Add((Material)val);
//					LoadDefaultHair();
//				});
//		}
		public void LoadDefaultHair()
		{
			if(defaultTool.GetComponent<Animator>()!=null){
				defaultTool.GetComponent<Animator> ().SetBool ("selected", false);
			}
			switch(GameManager.Avatar_No)
			{
			case CHARACTER_JANET:
				hair_bottom.GetComponent<SpriteRenderer> ().color = new Color32 (190, 44, 21, 255);
				switch(templateIndex)
				{
				case 0:
					characterHairFileName ="janet_0";
					break;
				case 1:
					hair_bottom.GetComponent<SpriteRenderer> ().color = new Color32 (24,163,186, 255);
					characterHairFileName = "janet_v4";
					break;
				case 2:
					characterHairFileName = "janet_v1";
					break;
				case 3:
					hair_bottom.GetComponent<SpriteRenderer> ().color = new Color32 (211,172,0, 255);
					characterHairFileName = "janet_v3";

					break;
				}

				break;
			case CHARACTER_UGA:
				hair_bottom.GetComponent<SpriteRenderer> ().color = new Color32 (12,118,12, 255);
				switch(templateIndex)
				{
				case 0:
					characterHairFileName = "uga_0";//uga_0
					break;
				case 1:
					characterHairFileName = "uga_v1";
					break;
				case 2:
					hair_bottom.GetComponent<SpriteRenderer> ().color = new Color32 (113,0,9, 255);
					characterHairFileName = "uga_v3";
					break;
				case 3:
					hair_bottom.GetComponent<SpriteRenderer> ().color = new Color32 (85,0,114, 255);
					characterHairFileName = "uga_v2";
					break;
				}

				break;
			case CHARACTER_PAULO:
				hair_bottom.GetComponent<SpriteRenderer> ().color =new Color32 (0,0,109, 255);
				switch(templateIndex)
				{
				case 0:
					characterHairFileName = "paulo_0";
					break;
				case 1:
					characterHairFileName = "paulo_v1";//paulo_v1
					break;
				case 2:
					characterHairFileName = "paulo_v2";//paulo_v3
					break;
				case 3:
					hair_bottom.GetComponent<SpriteRenderer> ().color =new Color32 (106,2,126, 255);
					characterHairFileName = "paulo_v3";
					break;
				}

				break;
			case CHARACTER_MERLE:
				hair_bottom.GetComponent<SpriteRenderer> ().color = new Color32 (177,8,0,255);//
				switch(templateIndex)
				{
				case 0:
					characterHairFileName ="merle_0";// "Merle";//merle_0
					break;
				case 1:
					characterHairFileName ="merle_v2";//merle_v1
					break;
				case 2:
					characterHairFileName ="merle_v3";// "merle_v2";
					hair_bottom.GetComponent<SpriteRenderer> ().color = new Color32 (158,9,155,255);//
					break;
				case 3:
					characterHairFileName ="merle_v1";// "merle_v3";
					break;
				}

				break;
			}
			string book_data_file_dir;
			book_data_file_dir =filePath + "/" +characterHairFileName;
			TextAsset textAsset = (TextAsset) Resources.Load(characterHairFileName);    

			if(textAsset){
				if(!File.Exists(book_data_file_dir)){
					System.Xml.XmlDocument xmldoc = new System.Xml.XmlDocument ();
					xmldoc.LoadXml ( textAsset.text );
					xmldoc.Save(book_data_file_dir);
				}

				SetButtonClicked();
				ClearHair();
				AUPathMesh.LoadDefaultPathData(book_data_file_dir, 1.0f, _hair_material, out _hair_meshes, out _hair_gos);
				AddHair();
				if ((_cur_hair_style == "Comb") && ((GameManager.Avatar_No == 0 && GameManager.BoughtJanet   ) || (GameManager.BoughtUga && GameManager.Avatar_No == 1) || (GameManager.BoughtPaulo && GameManager.Avatar_No == 2) || (GameManager.Avatar_No == 3))) {
					AUPathMesh.AddMeshCollider(ref _hair_gos);
					if(SelectedTool!=null)
					{
						SelectedTool.SetBool ("selected", false);
					}
					SelectedTool=defaultTool.GetComponent<Animator>();
					SelectedTool.SetBool ("selected", true);
					_cur_hair_style="Comb";
				}
				if(GameManager.instance!=null)
				{
					isClickAvailable=false;
					GameManager.instance.callEntryAnim ();
				}
			}
		}

//		IEnumerator setTool()
//		{
//			yield return new WaitForSeconds (0.5f);
//			if(SelectedTool!=null)
//			{
//				SelectedTool.SetBool ("selected", false);
//			}
//			SelectedTool=defaultTool.GetComponent<Animator>();
//			SelectedTool.SetBool ("selected", true);
				
//		}
		public void ResetDefaultAccessories()
		{
//			StartCoroutine ("setTool");
			if(SelectedTool!=null)
			{
				SelectedTool.SetBool ("selected", false);
			}
			SelectedTool=defaultTool.GetComponent<Animator>();
			SelectedTool.SetBool ("selected", true);
			_cur_hair_style="Comb";

		}
		public void addHairBase(GameObject obj ,AUPathMesh.VertexData data)
		{
			{
				GameObject obj1 = Instantiate (AppOn_HairGame.Instance.hair_bottom) as GameObject;
				obj1.SetActive (true);
				obj1.transform.parent = obj.transform;
				Vector3 v3 = data._pos;
				v3.y -= -5;
				v3.z = -47;
				obj1.transform.position = v3;
			}
		}
		public void Delete(){
			File.Delete (filePath + "/" + characterHairFileName);
		}

		public void Save(){
			AUPathMesh.SaveHairs(filePath,characterHairFileName,_hair_meshes);
		}

		public void ClearHair()
		{
			foreach (Transform child in _hair_root.transform)
			{
				if(child != null)
				{
					GameObject.Destroy(child.gameObject);
				}
			}
			
			_cut_hair_gos.Clear();
		}
	
		private void AddHair()
		{
			for(int i = 0; i < _hair_gos.Count; i++)
			{
				if(_hair_gos[i] != null)
				{
					_hair_gos[i].transform.parent = _hair_root.transform;
					float a = SetHair_Zvalue (_hair_root.transform.childCount);
					_hair_gos[i].transform.localPosition = new Vector3(0, 0, -_hair_meshes[i][0]._pos.y * a);
					_hair_gos[i].name = "hair"+i.ToString();
				}
			}
		}

	
		List<int> randomList = new List<int>();
		public void initList()
		{
			if (randomList.Count == 0) {
				for (int i = 6; i <=12; i++) {
					randomList.Add (i);
				}
			}
		}

		public int getRandom()
		{
			initList ();
//			for(int i=0;i<randomList.Count;i++)
//			{
//				Debug.Log (" randomList [i] "+ randomList [i]);
//			}
			int random = UnityEngine.Random.Range (0, randomList.Count);
			int realRandom = randomList [random];

			randomList.Remove(realRandom);
//			Debug.Log ("realRandom : "+realRandom);
			return realRandom;


		}

		bool isClickAvailable = false;

		public void Happy_Anims(){
			if(CharacterSoundManager.IsEntryAnimPlaying){
				return;
			}
			if (isClickAvailable) {
				int i = getRandom ();
				if (GameManager.Avatar_No ==CHARACTER_MERLE) {
					GameManager.instance.Merle_Anim.SetInteger ("State", i);

				}
				else if (GameManager.Avatar_No == CHARACTER_PAULO && GameManager.BoughtPaulo) {
					GameManager.instance.Paulo_Anim .SetInteger ("State", i);

				}
				else if (GameManager.Avatar_No == CHARACTER_JANET && GameManager.BoughtJanet) {
					GameManager.instance.Janet_Anim.SetInteger ("State", i);

				}
				else if (GameManager.Avatar_No == CHARACTER_UGA && GameManager.BoughtUga) {
					GameManager.instance.UGA_Anim.SetInteger ("State", i);

				}
				isClickAvailable = false;
			}
			else 
			{
				if (GameManager.Avatar_No == CHARACTER_JANET&& GameManager.BoughtJanet) {
					GameManager.instance.Janet_Anim.SetInteger ("State", 0);
				}
				else if (GameManager.Avatar_No == CHARACTER_MERLE ) {
					GameManager.instance.Merle_Anim.SetInteger ("State", 0);
				}
				else if (GameManager.Avatar_No == CHARACTER_PAULO && GameManager.BoughtPaulo) {
					GameManager.instance.Paulo_Anim.SetInteger ("State", 0);
				}
				else if (GameManager.Avatar_No ==CHARACTER_UGA && GameManager.BoughtUga) {
					GameManager.instance.UGA_Anim.SetInteger ("State", 0);
				}
			}

		}

		private Vector2 FindIntersectionPos(Ray ray)
		{
			if(Input.GetMouseButtonDown(0)){
				isClickAvailable = true;
			}
			Vector3 worldPos3 = transform.position;
			Plane plane = new Plane(transform.forward, worldPos3);
			float enter;
			if (plane.Raycast (ray, out enter)) {
				Vector3 intersectWorldPos = ray.GetPoint (enter);
				Vector3 intersectVector = intersectWorldPos - worldPos3;
				Vector2 worldVector = new Vector2 (Vector3.Dot (intersectVector, transform.right), Vector3.Dot (intersectVector, transform.up));

				return new Vector2 (worldVector.x / transform.lossyScale.x, worldVector.y / transform.lossyScale.y);

			} else {
				return new Vector2 (1000000, 1000000);
			}
		}

		public float SetHair_Zvalue(int ChildCount){
			float a;
//			if (ChildCount >= 18) {
//				
//			
//				a = 0.001f;
//			}
//			else{
//				a = 0.001f;
//			
//			}

			a = 0.001f;
			return a;
		}

		public void generateHair(Vector2 center)
		{
			float a ;
			for(int j = 0;j<1;j++){
				for(a = -100 + angleDefaultValue; a < 100 - angleDefaultValue;){
					Quaternion startAngle = Quaternion.Euler(0,0,a);
					Quaternion angle = transform.rotation * startAngle;
					Vector2 tdirection = angle * Vector2.up;
					hair_pt = center + (tdirection * radius);
					OnMyTouchBegin (hair_pt);
					for(int i = 0;i <100;i++)
					{
						OnMyTouchMoved (hair_pt + (Vector2.up * i),true);
						_previousMouseClick = hair_pt + (Vector2.up * i);	
					}
					OnMyTouchEnded (hair_pt);
					a += angleAdder;
				}
			}
		}
		void Update ()
		{
			if(GameManager.instance!=null && GameManager.instance.MainMenu.activeSelf)
			{
				disableDrierBlowAnim ();
				return;
			}
//			Debug.Log ("DragBottomPanel.scrollPanel : " + DragBottomPanel.scrollPanel + " : " + GameManager.instance.CamPanel + " :  " + GameManager.instance.Shop_Panel + " :  " + GameManager.instance.AvatarBuy_Panel + " : " + GameManager.instance.allAvatarBuy_Panel + " : " + GameManager.instance.CallGrownUp_Panel + " : " + BG_Change.instance.BuyBG_Panel + " : " + GameManager.instance.ForParent_Panel);
			if (DragBottomPanel.scrollPanel||(GameManager.instance != null && (GameManager.instance.CamPanel.activeSelf ||GameManager.instance.Shop_Panel.activeSelf || GameManager.instance.AvatarBuy_Panel.activeSelf || GameManager.instance.allAvatarBuy_Panel.activeSelf || GameManager.instance.CallGrownUp_Panel.activeSelf || BG_Change.instance.BuyBG_Panel.activeSelf  || GameManager.instance.ForParent_Panel.activeSelf))) {
				disableDrierBlowAnim ();
				if (!GameManager.instance.CamPanel.activeSelf  && !DragBottomPanel.scrollPanel) {
					disableShower ();
				}
				stopShower ();
				return;
			}
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Input.GetMouseButtonDown (0)) 
			{
				OnMyTouchBegin (FindIntersectionPos(ray));
			}
			else if (Input.GetMouseButtonUp (0)) 
			{
//				if (_cur_hair_style == "Add") {
//					for (int i = 0; i < 100; i++) {
//						OnMyTouchMoved (FindIntersectionPos (ray) + (Vector2.up * i), true);
//						_previousMouseClick = FindIntersectionPos (ray) + (Vector2.up * i);
//					}
//				}
				OnMyTouchEnded (FindIntersectionPos(ray));
			}
			else if (Input.GetMouseButton (0)) 
			{
				Vector2 currMouseClick = FindIntersectionPos(ray);
				if (Vector2.Distance (_previousMouseClick, currMouseClick) > 0.01f) {
					OnMyTouchMoved (currMouseClick, true);
					_previousMouseClick = currMouseClick;
				}
				

			}
			DestroyOffScreenHairs();
		}

		private IEnumerator ResetButtonClicked()
		{
			yield return new WaitForSeconds(1.0f);
			_gui_button_clicked = false;
		}
		
		public void SetButtonClicked()
		{
			_gui_button_clicked = true;
			_show_color_picker = false;
			StartCoroutine(ResetButtonClicked());
		}

		public AudioSource GrowSelect_Audio;
		public AudioSource CutSelect_Audio;
		public AudioSource TrimSelect_Audio;
		public AudioSource CombSelect_Audio;
		public AudioSource Comb1Select_Audio;
		public AudioSource shower_Audio;
		public AudioSource straightener_curler_Audio;
		public AudioSource drier_Audio;
		public void AddMoreHair(){
			SetButtonClicked();
			_cur_hair_style = "Add";
			AUPathMesh.AddMeshCollider(ref _hair_gos);
		}

		public Animator SelectedTool;
		public void SelectTool(CuttingTools toolName){
			Physics.gravity    = new Vector3(0.0f, -9.81f, 0.0f);
			if(toolName.name!="Hair_Wash" &&hairShower.activeSelf )
			{
				disableShower ();
				stopShower ();
			}
			GameManager.instance.ButtonClick_Audio.Play ();
			Deselection ();
			if(SelectedTool!=null){
				SelectedTool.SetBool ("selected", false);
			}
			SelectedTool=toolName.GetComponent<Animator>();
			SelectedTool.SetBool ("selected", true);
			SetButtonClicked();
			if (_cur_hair_style.Equals ("Hair_Dryer") && !_cur_hair_style.Equals (toolName.name)) {
				disableDrierBlowAnim ();
			}
			_cur_hair_style = toolName.name;

			switch(toolName.name){
			case "Grow":
				if(FTUEHandler.isPlaying_AnyTutorial  &&FTUEHandler.CURRENT_TUTORIAL_INDEX ==FTUEHandler.handleHairGrowBottleSelction)
				{
					FTUEHandler.CompleteTutorial (FTUEHandler.handleHairGrowBottleSelction);
					if(FTUEHandler.CanPlayHelp(FTUEHandler.handleHairGrow)){
						FTUEHandler.setTutorial (FTUEHandler.handleHairGrow);
						GameManager.instance.hairGrowTutorial.GetComponent<Animator> ().SetInteger ("tutorialID", 1);
					}

				}
				AUPathMesh.AddMeshCollider(ref _hair_gos);
				GoogleAnalytics.Instance.LogCustomEvent ("Grow", "AccessoriesUsed");
				break;
			case "Cut":
				AUPathMesh.RemoveMeshCollider(ref _hair_gos);
				GoogleAnalytics.Instance.LogCustomEvent ("Cut", "AccessoriesUsed");
				break;
			case "Comb":
				AUPathMesh.AddMeshCollider(ref _hair_gos);
				GoogleAnalytics.Instance.LogCustomEvent ("Comb", "AccessoriesUsed");
				break;
			case "Comb2":
				AUPathMesh.AddMeshCollider(ref _hair_gos);
				GoogleAnalytics.Instance.LogCustomEvent ("CrazyComb", "AccessoriesUsed");
				break;
			case "Trim":
				AUPathMesh.AddMeshCollider(ref _hair_gos);
				GoogleAnalytics.Instance.LogCustomEvent ("Trimmer", "AccessoriesUsed");
				break;
			case "Hair_Dryer":
				AUPathMesh.RemoveMeshCollider(ref _hair_gos);
				break;
			case "Straightener":
				AUPathMesh.AddMeshCollider(ref _hair_gos);
				break;
			case "Curler":
				AUPathMesh.AddMeshCollider(ref _hair_gos);
				break;
			case "Hair_Wash":
				hairShower.SetActive (true);
				AUPathMesh.AddMeshCollider(ref _hair_gos);
				break;
			}
		
		}

		public void CombHair(bool sendEvent = true){
			if(sendEvent)
				GameManager.instance.ButtonClick_Audio.Play ();
			Deselection ();
			SetButtonClicked();
			_cur_hair_style = "Comb";
			AUPathMesh.AddMeshCollider(ref _hair_gos);

			if (sendEvent) {
				GoogleAnalytics.Instance.LogCustomEvent ("Comb", "AccessoriesUsed");
			}
		}
	
		Animator SelectedBottle;
		public void SelectColor(ColorData data){
			GameManager.instance.ButtonClick_Audio.Play ();
			_hair_color = data.color;
			colorInt = data.colorID;
			Deselection ();
			if(SelectedBottle!=null){
				SelectedBottle.SetBool("Selected", false);
			}
			data.bottle.SetBool ("Selected", true);
			SelectedBottle = data.bottle;
			SetButtonClicked();
			_show_color_picker = true;
			_cur_hair_style = "Color";
			AUPathMesh.AddMeshCollider(ref _hair_gos);
			GoogleAnalytics.Instance.LogCustomEvent ("Color", "AccessoriesUsed");
		}

		public void Deselection(){
			if(SelectedTool!=null){
				SelectedTool.SetBool ("selected",false);
			}
		
//			SelectionAnim_Trim.SetBool ("Trim", false);
//			SelectionAnim_Grow.SetBool ("Grow", false);
//			SelectionAnim_Comb.SetBool ("Comb", false);
//			SelectionAnim_Cut.SetBool ("Cut", false);
//
//			SelectionAnim_HairDrier.SetBool ("Hair_Drier", false);
//			SelectionAnim_Streightener.SetBool ("Straightener", false);
//			SelectionAnim_curler.SetBool ("Curler", false);
//			SelectionAnim_hairwash.SetBool ("Hair_Wash", false);
			if(SelectedBottle!=null){
				SelectedBottle.SetBool("Selected", false);
			}
		
		}

		public void LoadHair()
		{
			SetButtonClicked();
				
			ClearHair();
			AUPathMesh.LoadPathData(characterHairFileName, 1.0f, _hair_material, out _hair_meshes, out _hair_gos);
			AddHair();
		}


		public Color32 ColorPicker()
	    {
	        //Finally return the modified value.
			if (colorInt == COLOR_PINK)//F63C99FF
				return new Color32(246, 60, 153, 255);//return Color.yellow;
			else if (colorInt == COLOR_BLACK)
				return new Color32(0,0,0,255);//return Color.black;
			else if (colorInt == COLOR_PURPLE)
				return new Color32(154,80,243,255);//return new Color32(154,80,243,255);
			else if (colorInt == COLOR_GREEN)
				return new Color32(101, 227, 105, 255);//return Color.green;
			else if (colorInt == COLOR_RED)
				return new Color32(163,1,1, 255);//return new Color32(190, 44, 21, 255);
			else if (colorInt == COLOR_BROWN)
				return new Color32(174,117,50,255);
			else if (colorInt == COLOR_BLUE)//6DA9FFFF
				return new Color32(109,169,255,255);//return new Color32(112,172,255,255);
			else if (colorInt == COLOR_BLONDE)
				return new Color32(235,185,125,255);
			else
			return Color.clear;
	    }
		bool OnMyTouchBegin(Vector2 pos)
		{
			if(_gui_button_clicked)
				return false;
			if(_cur_hair_style == "Cut")
			{
//				SoundManager.instance.cutAudio ();
//				GameObject.Find("32_AudioCommon").GetComponent<AudioSource>().Play();
				_cur_start_pt = new Vector3(pos.x, pos.y, 0);
				_cur_end_pt   = _cur_start_pt;
			}
			else if(_cur_hair_style == "Comb")
			{
				_cur_start_pt = new Vector3(pos.x, pos.y, 0);
				_cur_end_pt   = _cur_start_pt;
				_hit_meshes = GetAllHitHairs(20.0f);
			}
			else if(_cur_hair_style == "Hair_Wash")
			{
				
				_hit_meshes = GetAllHitHairs(20.0f,true);
			}
			else if(_cur_hair_style == "Curler")
			{
				_hit_meshes = GetAllHitHairs(20.0f);
			}
			else if(_cur_hair_style == "Straightener")
			{
				_hit_meshes = GetAllHitHairs(20.0f);
			}
			else if(_cur_hair_style == "Band")
			{
//				GameObject.Find("32_AudioCommon").GetComponent<AudioSource>().Play();
				_hit_meshes = GetAllHitHairs(20.0f);
			}
			else if(_cur_hair_style == "Color")
			{
//				GameObject.Find("32_AudioCommon").GetComponent<AudioSource>().Play();
				_hit_meshes = GetAllHitHairs(20.0f);
			}
			else if(_cur_hair_style == "Grow")
			{
//				if(!GrowSelect_Audio.isPlaying)
//					SoundManager.instance.growAudio ();
				_hit_meshes = GetAllHitHairs(20.0f);
			}
			else if(_cur_hair_style == "Trim")
			{
				
//				LoadResources( "pg04_sfx_trimmer", 
//							val=>{ 
////									AudioClip temp = val as AudioClip;
////									GameObject.Find("33_AudioTrimmer").GetComponent<AudioSource>().clip = temp;
////									GameObject.Find("33_AudioTrimmer").GetComponent<AudioSource>().loop = true;
////									GameObject.Find("33_AudioTrimmer").GetComponent<AudioSource>().Play();			
//				});
				_hit_meshes = GetAllHitHairs(20.0f);
//				SoundManager.instance.trimeAudio ();
			}
			else if(_cur_hair_style == "Comb2" || _cur_hair_style == "Comb3")
			{
				_cur_start_pt = new Vector3(pos.x, pos.y, 0);
				_cur_end_pt   = _cur_start_pt;
				_hit_meshes = GetAllHitHairs(20.0f);
			}
			else if(_cur_hair_style == "Hair_Dryer")
			{
				
//				LoadResources( "pg04_sfx_hair_dryer_start", 
//									val=>{ 
//											AudioClip temp = val as AudioClip;
//											_dryerAudioSource.clip = temp;
//											StartCoroutine("playDrierAudio");
//				});
			}else  if(_cur_hair_style == "Add")
			{
				_input_points.Clear();
				_input_points.Add(new AUPathMesh.VertexData(new Vector3(pos.x, pos.y, 0), _hair_half_width));
				_cur_hair_go = null;
				_input_points.Add(new AUPathMesh.VertexData(new Vector3(pos.x, pos.y, 0), _hair_half_width));

				if(_cur_hair_go != null)
					Destroy(_cur_hair_go);

				List<AUPathMesh.VertexData> out_data;
				_cur_hair_go = AUPathMesh.CreateMesh(_input_points, 1.0f, _hair_material, false, out out_data);

				if(_cur_hair_go != null)
				{
					_cur_hair_go.transform.parent = _hair_root.transform;
					float a = SetHair_Zvalue (_hair_root.transform.childCount);
					_cur_hair_go.transform.localPosition = new Vector3(0, 0, -out_data[0]._pos.y * a);
					_cur_hair_go.name = "hair"+_hair_gos.Count.ToString();
				}
			}else if(_cur_hair_style == "Delete"){
				_hit_meshes = GetAllHitHairs(10.0f);
				for(int i=0;i<_hit_meshes.Count;i++){
					int indx=_hit_meshes[i].m_index;
					_hair_meshes.Remove(_hair_meshes[indx]);
					GameObject obj=_hair_gos[indx];
					_hair_gos.Remove(obj);
					Destroy(obj);
					break;
				}
			}
			return false;
		}
		bool canUseHairDrier = false;
		public static bool isAnyChangeDoneInHairStyle=false;
		bool OnMyTouchMoved(Vector2 pos , bool enableAddHair)
		{
			
			if(_gui_button_clicked)
				return false;
			if(_cur_hair_style == "Cut")
			{
				_cur_end_pt = new Vector3(pos.x, pos.y, 0);
				DoHairCut();
				_cur_start_pt = _cur_end_pt;
				
			}
			else if(_cur_hair_style == "Hair_Dryer")
			{
				 canUseHairDrier = false;
//				_hit_meshes = GetAllHitHairs(20.0f);
				for(int i=0;i<_hair_gos.Count;i++)
				{
					MeshRenderer mr = _hair_gos [i].GetComponent<MeshRenderer> ();
					if(mr!=null && mr.enabled)
					{
						canUseHairDrier = true;
						break;
					}
				}
				if (isClickInAvtarRect (pos)  && canUseHairDrier) {
					AUPathMesh.ApplyDrierHair (1.0f, _hair_material, new Vector3 (pos.x, pos.y, 0), _hair_meshes, out _hair_meshes, ref _hair_gos);
				}
			}
			else if(_cur_hair_style == "Comb")
			{
				_cur_end_pt = new Vector3(pos.x, pos.y, 0);
				_hit_meshes = GetAllHitHairs(20.0f);
//				_cur_offset_pt = _cur_end_pt - _cur_start_pt;
				AUPathMesh.ApplyCombHair(1.0f, _hair_material, _cur_end_pt, _cur_start_pt, _hit_meshes, ref _hair_meshes, ref _hair_gos);
				_cur_start_pt = _cur_end_pt;
			}
			else if(_cur_hair_style == "Hair_Wash")//Hair_Wash
			{

				_hit_meshes = GetAllHitHairs(20.0f,true);
				if(_hit_meshes!=null &&_hit_meshes.Count!=0)
					AUPathMesh.ApplyWaterHair(1.0f, _hair_material, new Vector3(pos.x, pos.y, 0), _hit_meshes, ref _hair_meshes, ref _hair_gos);
			}
			else if(_cur_hair_style == "Curler")
			{
				_cur_end_pt = new Vector3(pos.x, pos.y, 0);
				_hit_meshes = GetAllHitHairs(20.0f);
				_cur_start_pt = _cur_end_pt;
				if (_hit_meshes != null && _hit_meshes.Count != 0) {
					SoundManager.instance.straightener_curlerAudio ();
					AUPathMesh.ApplyCurlerOnHair (1.0f, _hair_material, _cur_end_pt, _cur_start_pt, _hit_meshes, ref _hair_meshes, ref _hair_gos, curlyHairTexture [GameManager.Avatar_No]);
				}
			}
			else if(_cur_hair_style == "Straightener")
			{
				_cur_end_pt = new Vector3(pos.x, pos.y, 0);
				_hit_meshes = GetAllHitHairs(20.0f);
				//				_cur_offset_pt = _cur_end_pt - _cur_start_pt;
				_cur_start_pt = _cur_end_pt;
				if (_hit_meshes != null && _hit_meshes.Count != 0) {
					SoundManager.instance.straightener_curlerAudio ();
					AUPathMesh.ApplyStraightenerOnHair (1.0f, _hair_material, _cur_end_pt, _cur_start_pt, _hit_meshes, ref _hair_meshes, ref _hair_gos, straightHairTexture [GameManager.Avatar_No]);
				}
			}
			else if(_cur_hair_style == "Color")
			{
				_hit_meshes = GetAllHitHairs(20.0f);
				AUPathMesh.ApplyColorHair(1.0f, _hair_material, _hair_color, new Vector3(pos.x, pos.y, 0), _hit_meshes, ref _hair_meshes, ref _hair_gos);
				
//				GameObject color_particle_effect = (GameObject)Instantiate(_hair_color_particle_effect_prefab);
//				color_particle_effect.GetComponent<ParticleSystem>().startColor = _hair_color;
//				color_particle_effect.GetComponent<ParticleSystem>().Emit(10);
//				color_particle_effect.transform.parent = _hair_root.transform;
//				color_particle_effect.transform.localPosition = new Vector3(pos.x, pos.y, -200);
//				StartCoroutine(DestroyColorParticle(color_particle_effect));
			}
			else if(_cur_hair_style == "Grow")
			{
				_hit_meshes = GetAllHitHairs(20.0f);
				AUPathMesh.ApplyGrowHair(1.0f, _hair_material, new Vector3(pos.x, pos.y, 0), _hit_meshes, ref _hair_meshes, ref _hair_gos);
				if(FTUEHandler.isPlaying_AnyTutorial &&FTUEHandler.CURRENT_TUTORIAL_INDEX ==FTUEHandler.handleHairGrow && _hit_meshes.Count>2)
				{
					FTUEHandler.CompleteTutorial (FTUEHandler.handleHairGrow);
					GameManager.instance.CloseHairGrowTuorial ();
				}
			}
			else if(_cur_hair_style == "Trim")
			{
				_hit_meshes = GetAllHitHairs(20.0f);
				List<GameObject> cut_gos;
				DoHairCut ();
				if(_hit_meshes.Count>2 &&FTUEHandler.CanPlayHelp(FTUEHandler.handleHairGrowBottleSelction))
				{
					isshowHairGrowTut = true;
				}
				AUPathMesh.ApplyTrimHair(1.0f, _hair_material, new Vector3(pos.x, pos.y, 0), _hit_meshes, ref _hair_meshes, ref _hair_gos, out cut_gos);
				for(int i = 0; i < cut_gos.Count; i++)
				{
					if(cut_gos[i] != null)
					{
						cut_gos[i].transform.parent = _hair_root.transform;
						float a = SetHair_Zvalue (_hair_root.transform.childCount);
						cut_gos[i].transform.localPosition = new Vector3(0, 0, -_hair_meshes[i][0]._pos.y* a );
						cut_gos[i].name = "haircut"+i.ToString();
						_cut_hair_gos.Add(cut_gos[i]);
					}
				}
			}
			else if(_cur_hair_style == "Comb2")
			{
				_cur_end_pt = new Vector3(pos.x, pos.y, 0);
				_hit_meshes = GetAllHitHairs(20.0f);
//				_cur_offset_pt = _cur_end_pt - _cur_start_pt;
				_cur_start_pt = _cur_end_pt;
				AUPathMesh.ApplyCombHair2(1.0f, _hair_material, new Vector3(pos.x, pos.y, 0), _hit_meshes, ref _hair_meshes, ref _hair_gos);
			}else   if(_cur_hair_style == "Add")
			{
				if (!enableAddHair)
					return false;
				_input_points.Add(new AUPathMesh.VertexData(new Vector3(pos.x, pos.y, 0), _hair_half_width));

				if(_cur_hair_go != null)
					Destroy(_cur_hair_go);

				List<AUPathMesh.VertexData> out_data;
				_cur_hair_go = AUPathMesh.CreateMesh(_input_points, 1.0f, _hair_material, false, out out_data);

				if(_cur_hair_go != null)
				{
					_cur_hair_go.transform.parent = _hair_root.transform;
					float a = SetHair_Zvalue (_hair_root.transform.childCount);
					_cur_hair_go.transform.localPosition = new Vector3(0, 0, -out_data[0]._pos.y* a);
					_cur_hair_go.name = "hair"+_hair_gos.Count.ToString();
				}
			}
			return false;
		}

			
		private IEnumerator playDrierAudio()
		{
			_dryerAudioStopLoop = false;
			_dryerAudioSource.loop = false;
			_dryerAudioSource.Play();
			
			while(_dryerAudioSource.isPlaying)
				yield return null;
			
			if (_dryerAudioStopLoop == false)
			{
				LoadResources( "pg04_sfx_hair_dryer_looping", 
							val=>{
									AudioClip temp = val as AudioClip;
									_dryerAudioSource.clip = temp;
									_dryerAudioSource.loop = true;
									_dryerAudioSource.Play();			
				});
			}
		}
		
		private IEnumerator DestroyColorParticle(GameObject particle_go)
		{
			yield return new WaitForSeconds(1.0f);
			Destroy(particle_go);
		}
		private bool isshowHairGrowTut = false;
		bool OnMyTouchEnded(Vector2 pos)
		{
			
//			enableAllButtons();
			if(_gui_button_clicked)
				return false;
			
			if(_cur_hair_style == "Trim")
			{
				if(isshowHairGrowTut &&FTUEHandler.CanPlayHelp(FTUEHandler.handleHairGrowBottleSelction))
				{
					FTUEHandler.setTutorial(FTUEHandler.handleHairGrowBottleSelction);
					GameManager.instance.ShowHairGrowTutorial ();
				}
//				GameObject.Find("33_AudioTrimmer").GetComponent<AudioSource>().Stop();
//				LoadResources( "pg04_sfx_trimmer_stop", 
//							val=>{ 
//									AudioClip temp = val as AudioClip;
//									GameObject.Find("33_AudioTrimmer").GetComponent<AudioSource>().clip = temp;
//									GameObject.Find("33_AudioTrimmer").GetComponent<AudioSource>().loop = false;
//									GameObject.Find("33_AudioTrimmer").GetComponent<AudioSource>().Play();			
//				});
			}
			else if(_cur_hair_style == "Cut" || _cur_hair_style == "Color")
			{
				_cur_end_pt = new Vector3(pos.x, pos.y, 0);
				_cur_start_pt = _cur_end_pt;
//				if (GameObject.Find("32_AudioCommon").GetComponent<AudioSource>().isPlaying)
//					GameObject.Find("32_AudioCommon").GetComponent<AudioSource>().Stop();
			}
			else if(_cur_hair_style == "Hair_Dryer")
			{
				_dryerAudioStopLoop = true;
				disableDrierBlowAnim ();
			}
			else if(_cur_hair_style == "Hair_Wash")
			{
				stopShower ();
			}
			else   if(_cur_hair_style == "Add")
			{
				if(_cur_hair_go != null)
					Destroy(_cur_hair_go);

				List<AUPathMesh.VertexData> out_data;
				_cur_hair_go = AUPathMesh.CreateMesh(_input_points, 1.0f, _hair_material, true, out out_data);

				if(_cur_hair_go != null && out_data[out_data.Count-1]._length>0)
				{
					_cur_hair_go.transform.parent = _hair_root.transform;
					float a = SetHair_Zvalue (_hair_root.transform.childCount);
					_cur_hair_go.transform.localPosition = new Vector3(0, 0, -out_data[0]._pos.y* a);
					_cur_hair_go.name = "hair"+_hair_gos.Count.ToString();
					GameObject obj = Instantiate (hair_bottom) as GameObject;
					obj.SetActive (true);
					obj.transform.parent = _cur_hair_go.transform;
					Vector3 v2 = out_data [0]._pos;
					v2.y -= 20;
					obj.transform.position = v2;
					_hair_meshes.Add(out_data);
					_hair_gos.Add(_cur_hair_go);
				}else{
					Destroy(_cur_hair_go);
				}

				_cur_hair_go = null;


			}	
			return false;
		}

		public void DoHairCut()
		{

			for(int i = 0; i < _hair_meshes.Count; i++)
			{
				AUPathMesh.VertexData inter_pt;
				int index = AUPathMesh.IntersectsExistingLines(_hair_meshes[i], _cur_start_pt, _cur_end_pt, out inter_pt);
				if(index != -1)
				{
					List<AUPathMesh.VertexData> remain_path;
					GameObject remain_go;
					GameObject cut_go;
					GameObject current_go = _hair_gos[i];
					
					AUPathMesh.CutPathMesh(_hair_meshes[i], 1.0f, _hair_gos[i].GetComponent<MeshRenderer>().material, index, inter_pt,
						_cur_start_pt, _cur_end_pt, out remain_path, out remain_go, out cut_go, current_go);
					
					if(remain_go != null)
					{
						GameObject child = _hair_gos [i].transform.GetChild (0).gameObject;
						Destroy(_hair_gos[i]);

						remain_go.transform.parent = _hair_root.transform;
						float a = SetHair_Zvalue (_hair_root.transform.childCount);
						remain_go.transform.localPosition = new Vector3(0, 0, -_hair_meshes[i][0]._pos.y* a);
						remain_go.name = "hair"+i.ToString();
						child.transform.parent = remain_go.transform;
						_hair_meshes[i] = remain_path;
						_hair_gos[i]    = remain_go;

					}
					
					if(cut_go != null)
					{
						cut_go.transform.parent = _hair_root.transform;
						float a = SetHair_Zvalue (_hair_root.transform.childCount);
						cut_go.transform.localPosition = new Vector3(0, 0, -_hair_meshes[i][0]._pos.y* a );
						cut_go.name = "haircut"+i.ToString();
						_cut_hair_gos.Add(cut_go);
					}
					if(_hair_gos[i].GetComponent<MeshCollider>()==null && _cur_hair_style=="Trim")
					{
						_hair_gos [i].GetComponent<MeshRenderer> ().enabled = false;
					}
				}
						
			}
		}
		
		private void DestroyOffScreenHairs()
		{
			for(int i = 0; i < _cut_hair_gos.Count; i++)
			{
				if(_cut_hair_gos[i].transform.position.y < -(_cut_hair_gos[i].GetComponent<Renderer>().bounds.max.y - _cut_hair_gos[i].GetComponent<Renderer>().bounds.min.y)-768)
				{
					GameObject one_hair = _cut_hair_gos[i];
					_cut_hair_gos.RemoveAt(i);
					Destroy(one_hair);
					i--;
				}
			}
		}
		public  RaycastHit [] getRayscastHits()
		{
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			ray.origin = new Vector3(ray.origin.x, ray.origin.y,_hair_root.transform.position.z-10);
			RaycastHit[] hits;
			hits = Physics.SphereCastAll(ray, 10);
			return hits;
		}

		Ray gizmoRay;

		private List<AUPathMesh.HitInfo> GetAllHitHairs(float radius,bool rayCast=false)
		{
			List<AUPathMesh.HitInfo> hit_result = new List<AUPathMesh.HitInfo>();
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			ray.origin = new Vector3(ray.origin.x, ray.origin.y, _hair_root.transform.position.z-radius);
			RaycastHit[] hits;
			if (rayCast) {
				if (!shower2.IsAlive ())
					return null;
						hits =Physics.CapsuleCastAll(ray.origin,new Vector3(ray.origin.x,ray.origin.y-200,ray.origin.z),50, Vector3.down * 200);

			} else
			{
				hits = Physics.SphereCastAll(ray, radius);
			}
        	
			for(int i = 0; i < hits.Length; i++)
			{
				RaycastHit hit = hits[i];
				for(int j = 0; j < _hair_gos.Count; j++)
				{
					if(hit.collider.gameObject == _hair_gos[j])
					{
						if((_cur_hair_style!="Grow" &&_cur_hair_style!="Trim"  &&_cur_hair_style!="Hair_Dryer" && _cur_hair_style!="Comb") && !_hair_gos[j].GetComponent<MeshRenderer>().enabled)
						{
							break;
						} else 
						{
							hit_result.Add (new AUPathMesh.HitInfo (j, 1.0f - hit.distance / radius));
							break;
						}

					}
				}
			}
			return hit_result;
		}


	
		void OnDestroy()
		{
			_hair_root = null;
			for(int i = 0; i < _hair_material.Count; i++)
				_hair_material[i] = null;
			_hair_material = null;
			
			for(int i = 0; i < _hair_meshes.Count; i++)
				_hair_meshes[i] = null;
			_hair_meshes = null;
			
			for(int i = 0; i < _hair_gos.Count; i++)
				Destroy(_hair_gos[i]);
			_hair_gos = null;
			_hit_meshes = null;
			
			for(int i = 0; i < _cut_hair_gos.Count; i++)
				Destroy(_cut_hair_gos[i]);
			_cut_hair_gos = null;
		
			_hair_color_particle_effect_prefab = null;
			
			for(int i = 0; i < _available_color_tools_go.Count; i++)
				_available_color_tools_go[i] = null;
				_available_color_tools_go = null;

//			_spike_hair_texture = null;
//			_curl_hair_texture	= null;
//			_original_hair_texture = null;
			_dryerAudioSource	= null;
		}
//		public SavedHairStyle savedhairStyle=new SavedHairStyle();
//		public  GameObject hatIndex;
//		public  GameObject hairClipIndex;
//		public  GameObject glassesIndex;
//		public static HairAccessories hatObject;
//		public static HairAccessories hairClipObject;
//		public static HairAccessories glassesObject;

//		void OnGUI()
//		{
//			if(GUI.Button(new Rect(100,0,100,100),"Save")){
//				savedhairStyle.saveData (filePath, "savedhairStyle", _hair_meshes, GameManager.Avatar_No, hatObject,hairClipObject,glassesObject);
//			}
//
//			if(GUI.Button(new Rect(100,250,100,100),"loadHairStyle")){
//				loadUserData=true;
//				characterHairFileName = "savedhairStyle";
//				HairStyleData data = savedhairStyle.loadData (filePath, characterHairFileName);
//				GameManager.Avatar_No= data._characterID;
//				hatObject=((HairStyleData)data)._hatObject;
//				hairClipObject=((HairStyleData)data)._hairClipObject;
//				glassesObject=((HairStyleData)data)._glassesObject;
//				switch(GameManager.Avatar_No)
//				{
//				case CHARACTER_JANET:
//					hair_bottom.GetComponent<SpriteRenderer> ().color = new Color32 (190, 44, 21, 255);
//					break;
//				case CHARACTER_UGA:
//					hair_bottom.GetComponent<SpriteRenderer> ().color = new Color32 (40, 80, 189, 255);
//					break;
//				case CHARACTER_PAULO:
//					hair_bottom.GetComponent<SpriteRenderer> ().color = new Color32 (135, 34, 204, 255);
//					break;
//				case CHARACTER_MERLE:
//					hair_bottom.GetComponent<SpriteRenderer> ().color = new Color32 (246, 108, 33, 255);
//					break;
//				}
//				Deselection ();
//				LoadDefaultHair ();
//				GameManager.instance.SetSelectedAvatarProperties ();
//				if(hatObject!=null && !hatObject.name.Equals(""))
//				{
//					hatIndex.transform.name = hatObject.name;
//					hatIndex.transform.position = new Vector3(hatObject.x, hatObject.y, hatObject.z);
//					hatIndex.GetComponent<DragDrop> ().setObject ();
//				}
//				if(hairClipObject!=null && !hairClipObject.name.Equals(""))
//				{
//					hairClipIndex.transform.name = hairClipObject.name;
//					hairClipIndex.transform.position = new Vector3(hairClipObject.x, hairClipObject.y, hairClipObject.z);
//					hairClipIndex.GetComponent<DragDrop> ().setObject ();
//				}
//				if(glassesObject!=null && !glassesObject.name.Equals(""))
//				{
//					glassesIndex.transform.name = glassesObject.name;
//					glassesIndex.transform.position = new Vector3(glassesObject.x, glassesObject.y, glassesObject.z);
//					glassesIndex.GetComponent<DragDrop> ().setObject ();
//				}
//			}
//		}

		public Text templateFileName;
//		void OnGUI()
//		{
//			//			GUI.BeginGroup(new Rect(0, 0, Screen.width, Screen.height));
//			//			GUI.Box(new Rect(0, 0, 800, 600), "This box is now centered! - here you would put your main menu");
//			//			GUI.EndGroup();
//
//
//			Vector3 tScale =new Vector3(1.0f*Screen.width/800, 1.0f*Screen.height/1280, 1.0f);
//			GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.AngleAxis(0, new Vector3(0, 1, 0)), tScale);
//			GUIStyle myStyle = new GUIStyle(GUI.skin.button);
//			myStyle.fontSize = 20;
//			int diff = 110;
//			int x = 10 - diff;
//			int y = 10;
//			int xSize=100;
//			int ySize=80;
//
//			////			if (GUI.Button (new Rect (x += diff, y, xSize, ySize), "More",myStyle))
//			////			{
//			////				LoadResources( "AurBkEngCrazyHair_original_hair", 
//			////					val=>{
//			////						_hair_material.Add((Material)val);
//			////						LoadDefaultHair();
//			////					});
//			////				_showMoreMenu = !_showMoreMenu;
//			//			}
//
//			//			if (GUI.Button(new Rect(x+=diff, y, xSize, ySize), "Cut",myStyle))
//			//			{
//			//				SetButtonClicked();
//			//				_cur_hair_style = "Cut";
//			//				AUPathMesh.RemoveMeshCollider(ref _hair_gos);
//			//			}
//			//
//			//			if (GUI.Button(new Rect(x+=diff, y, xSize, ySize), "Grow",myStyle))
//			//			{
//			//				SetButtonClicked();
//			//				_cur_hair_style = "Grow";
//			//				AUPathMesh.AddMeshCollider(ref _hair_gos);
//			//			}
//
//			//			if(!_show_color_picker)
//			//			{
//			//				if(GUI.Button(new Rect(x+=diff, y, xSize, ySize), "Color",myStyle))
//			//				{
//			//					SetButtonClicked();
//			//					_show_color_picker = true;
//			//					_cur_hair_style = "Color";
//			//					AUPathMesh.AddMeshCollider(ref _hair_gos);
//			//					
//			//				}
//			//			}
//
////			if(_show_color_picker)
////			{
////				_hair_color = ColorPicker(new Rect(x+=diff,10,160,160),_hair_color);
////				//				x += 110;
////			}
//
//			//			if (GUI.Button(new Rect(x+=diff, y, xSize, 80), "Comb",myStyle))
//			//			{
//			//				SetButtonClicked();
//			//				_cur_hair_style = "Comb";
//			//				AUPathMesh.AddMeshCollider(ref _hair_gos);
//			//			}
//
//			//			if (GUI.Button(new Rect(x+=diff, y, xSize, 80), "Comb2",myStyle))
//			//			{
//			//				SetButtonClicked();
//			//				_cur_hair_style = "Comb2";
//			//				AUPathMesh.AddMeshCollider(ref _hair_gos);
//			//			}
//			//
//			//			x = 10 - diff;
//			//			y += 90;
//			//
//
//			//			if (_showMoreMenu && GUI.Button(new Rect(x+=diff, y, xSize, 80), "Reset",myStyle))
//			//			{
//			//				LoadHair();
//			//			}
//
////						if ( GUI.Button(new Rect(x+=diff, y, xSize, 80), "Drier",myStyle))
////						{
////							SetButtonClicked();
////							_cur_hair_style = "Drier";
////							AUPathMesh.RemoveMeshCollider(ref _hair_gos);
////						}
//			
////						if ( GUI.Button(new Rect(x+=diff, y, xSize, 80), "Water",myStyle))
////						{
////							SetButtonClicked();
////							_cur_hair_style = "Water";
////							AUPathMesh.AddMeshCollider(ref _hair_gos);
////						}
//			//
//			//
//			//			if (_showMoreMenu && GUI.Button(new Rect(x+=diff, y, xSize, 80), "Trim",myStyle))
//			//			{
//			//				SetButtonClicked();
//			//				_cur_hair_style = "Trim";
//			//				AUPathMesh.AddMeshCollider(ref _hair_gos);
//			//			}
//			if ( GUI.Button(new Rect(x+=diff, y, xSize, 80), "stopAnim",myStyle))
//			{
//				switch(GameManager.Avatar_No)
//				{
//				case CHARACTER_MERLE:
//					GameManager.instance.Merle.GetComponentInChildren<Animator> ().enabled = false;
//					break;
//				case CHARACTER_JANET:
//					GameManager.instance.Janet.GetComponentInChildren<Animator> ().enabled = false;
//					break;
//				case CHARACTER_UGA:
//					GameManager.instance.UGA.GetComponentInChildren<Animator> ().enabled = false;
//					break;
//				case CHARACTER_PAULO:
//					GameManager.instance.Paulo_Anim.GetComponentInChildren<Animator> ().enabled = false;
//					break;
//				}
//			}
//			if (GUI.Button(new Rect(x+=diff, y, xSize, 80), "Hair",myStyle))
//			{
//				SetButtonClicked();
//				_cur_hair_style = "Add";
//				AUPathMesh.RemoveMeshCollider(ref _hair_gos);
//			}
//			if (GUI.Button(new Rect(x+=diff, y, xSize, 80), "SaveFile",myStyle))
//			{
//				AUPathMesh.SaveHairs(filePath,characterHairFileName,_hair_meshes);
//			}
//			if (GUI.Button(new Rect(x+=diff, y, xSize, 80), "Delete",myStyle))
//			{
//				SetButtonClicked();
//				_cur_hair_style = "Delete";
//				AUPathMesh.AddMeshCollider(ref _hair_gos);
//			}
//			if (GUI.Button(new Rect(x+=diff, y, xSize, 80), "topPanel",myStyle))
//			{
//				GameManager.instance.DrawerConatainer.SetActive (!GameManager.instance.DrawerConatainer.activeSelf);
//			}
////			if (GUI.Button(new Rect(x+=diff, y, xSize, 80), "Grow",myStyle))
////			{
////				SetButtonClicked();
////				_cur_hair_style = "Add";
////				for(int i = 0 ; i < _hair_gos.Count ; i++)
////				{
////					Destroy (_hair_gos[i]);
////				}
////				_hair_gos.Clear ();
////				_hair_meshes.Clear ();
//////				generateHair (center.transform.position);
////				AUPathMesh.AddMeshCollider(ref _hair_gos);
////
////			}
//		}

		public ParticleSystem shower1;
		public ParticleSystem shower2;
		public GameObject hairShower;
		public void startShower()
		{
			SoundManager.instance.showerAudio ();
			if(!shower1.isPlaying)
			{
				AppOn_HairGame.Instance.shower2.Clear (true);
				shower1.Play ();
			}
		}
		public void disableShower()
		{
			hairShower.SetActive (false);
		}
		public void enableShower()
		{
			hairShower.SetActive (true);
		}
		public void stopShower()
		{
			if (shower_Audio.isPlaying) {
				shower_Audio.Stop ();
			}
			if(shower1.isPlaying)
			{
				shower1.Stop ();
				AppOn_HairGame.Instance.shower1.Clear (true);
			}
		}
//		void OnParticleCollision(GameObject other)
//		{
//			int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents);
//
//			Rigidbody rb = other.GetComponent<Rigidbody>();
//			int i = 0;
//
//			while (i < numCollisionEvents)
//			{
//				if (rb)
//				{
//					Vector3 pos = collisionEvents[i].intersection;
//					Vector3 force = collisionEvents[i].velocity * 10;
//					rb.AddForce(force);
//				}
//				i++;
//			}
//		}
		public void setHairDrierAnim()
		{
			if (DragBottomPanel.scrollPanel||(GameManager.instance != null && (GameManager.instance.CamPanel.activeSelf ||GameManager.instance.Shop_Panel.activeSelf || GameManager.instance.AvatarBuy_Panel.activeSelf || GameManager.instance.allAvatarBuy_Panel.activeSelf || GameManager.instance.CallGrownUp_Panel.activeSelf || BG_Change.instance.BuyBG_Panel.activeSelf  || GameManager.instance.ForParent_Panel.activeSelf))) {
				return;
			}
			SoundManager.instance.drierAudio ();
			if(!DragBottomPanel.scrollPanel &&GameManager.Avatar_No==CHARACTER_JANET /*&& !JanetAnim.GetBool("UseDrier")*/)
			{
				GameManager.instance.Janet_Anim.SetInteger ("State",13);
			}
			else if(!DragBottomPanel.scrollPanel &&GameManager.Avatar_No==CHARACTER_MERLE /*&& !MerleAnim.GetBool("UseDrier")*/)
			{
				GameManager.instance.Merle_Anim.SetInteger ("State",13);
			}
			else if(!DragBottomPanel.scrollPanel &&GameManager.Avatar_No==CHARACTER_UGA/* && !UGAAnim.GetBool("UseDrier")*/)
			{
				GameManager.instance.UGA_Anim.SetInteger ("State",13);
			}else if(!DragBottomPanel.scrollPanel &&GameManager.Avatar_No==CHARACTER_PAULO/* && !UGAAnim.GetBool("UseDrier")*/){
				GameManager.instance.Paulo_Anim.SetInteger ("State",13);
			}

		}

		public void disableDrierBlowAnim()
		{
			if (drier_Audio.isPlaying) {
				drier_Audio.Stop ();
			}

			if(GameManager.Avatar_No==CHARACTER_JANET /*&&JanetAnim.GetBool("UseDrier")*/ )
			{
				GameManager.instance.Janet_Anim.SetInteger ("State",0);
			}
			else if(GameManager.Avatar_No==CHARACTER_MERLE /*&&MerleAnim.GetBool("UseDrier") */)
			{
				GameManager.instance.Merle_Anim.SetInteger ("State",0);
			}
			else if(GameManager.Avatar_No==CHARACTER_UGA /*&&UGAAnim.GetBool("UseDrier") */)
			{
				GameManager.instance.UGA_Anim.SetInteger ("State",0);
			}else if(GameManager.Avatar_No==CHARACTER_PAULO /*&&UGAAnim.GetBool("UseDrier") */)
			{
				GameManager.instance.Paulo_Anim.SetInteger ("State",0);
			}
		}
		public bool isClickInAvtarRect(Vector2 clciked)
		{
			BoxCollider boxCollider = drierCollision.GetComponent<BoxCollider> ();
			if (boxCollider.bounds.Contains (clciked)) {
				return true;
			}
			return false;
		}
	}
}
