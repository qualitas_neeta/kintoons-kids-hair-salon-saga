using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using UnityEngine.Rendering;
namespace AppOn.HairGame
{

	public class AUPathMesh
	{
		public static int   m_iterations 		 = 2;		// number of smooth iteration
		public static float m_simplify_tolerance = 25.0f * 25.0f;   // simplify tolerance
		public static float m_min_distance    	 = 1;		// min points distance   sqr
		public static float m_init_distance   	 = 1;		// min start points distance sqr
		public static float m_sample_interval 	 = 10;		// spline sample interval 
		public static float m_min_cut_length	 = 20.0f;   // min cutable length
		public static Texture originalTexture;
		// Save hit information
		public struct HitInfo
		{
			public int 	 m_index;
			public float m_weight;

			public HitInfo(int index, float weight)
			{
				m_index  = index;
				m_weight = weight;
			}
		}

		// Save Hair material
		public struct HairMaterial
		{
			public Material m_straight_material;
			public Material m_curl_material;
		}


		public class MeshPointData
		{
			public Vector3 _pos;			// current vertex position
			public Color32 _color;
			public Vector2 _uv;

			public MeshPointData(Vector3 pos, Vector2 uv, Color32 color)
			{
				_pos	= pos;
				_uv  	= uv;
				_color 	= color;
			}
		}

		[XmlInclude(typeof(AUPathMeshData))]
		public class VertexData
		{
			public Vector3 _original_dir;	
			public Vector3 _pos;			// current vertex position
			public Color32 _color;
			public Color32 _origcolor;
			public float   _half_width;		// current vertex width
			public float   _length_ratio;	// current vertex length percentage
			public float   _length;
			public int 	   _material_index;
			public VertexData(Vector3 pos, float half_width, float length_ratio, float length, Color32 color, int material_index)
			{
				_pos 		  = pos;
				_half_width   = half_width;
				_length_ratio = length_ratio;
				_length		  = length;
				_color		  = color;
				_material_index = material_index;
				_origcolor = color;
			}

			public VertexData(Vector3 pos, float half_width, float length_ratio, float length)
			{
				_pos 		  = pos;
				_half_width   = half_width;
				_length_ratio = length_ratio;
				_length		  = length;
				_color		  = new Color32(255, 255, 255, 255);
				_material_index = GameManager.Avatar_No;
				_origcolor = _color;
			}

			public VertexData(Vector3 pos, float half_width, float length_ratio)
			{
				_pos 		  = pos;
				_half_width   = half_width;
				_length_ratio = length_ratio;
				_color		  = new Color32(255, 255, 255, 255);
				_material_index = GameManager.Avatar_No;
				_origcolor = _color;
			}

			public VertexData(Vector3 pos, float half_width)
			{
				_pos 		  = pos;
				_half_width   = half_width;
				_color		  = new Color32(255, 255, 255, 255);
				_material_index = GameManager.Avatar_No;
				_origcolor = _color;
			}

			public VertexData()
			{
				_pos		  = Vector3.zero;
				_half_width	  = 1.0f;
				_color		  = new Color32(255, 255, 255, 255);
				_material_index = GameManager.Avatar_No;
				_origcolor = _color;
			}

			public VertexData(Vector3 pos, float half_width, Color32 color)
			{
				_pos 		  = pos;
				_half_width   = half_width;
				_color		  = color;
				_material_index = GameManager.Avatar_No;
			}

			public VertexData(VertexData copy)
			{
				_pos 		  = copy._pos;
				_half_width   = copy._half_width;
				_length_ratio = copy._length_ratio;
				_length		  = copy._length;
				_color		  = copy._color;
				_material_index = copy._material_index;
				_origcolor = _origcolor;
			}
		}
		public struct AUPathMeshData
		{
			public List<List<VertexData>> m_mesh_data;
		}

		private static float ComputePathLength(List<VertexData> v)
		{
			float sum = 0;

			for(int i = 0; i < v.Count; i++)
			{
				if(i > 0)
				{
					sum += Vector3.Distance(v[i-1]._pos, v[i]._pos);
				}
				v[i]._length	   = sum;
			}
			return sum;
		}

		public static void ComputeLengthRatio(List<VertexData> v)
		{
			float sum = ComputePathLength(v);

			for(int i = 0; i < v.Count; i++)
			{
				v[i]._length_ratio = v[i]._length / sum;
			}




		}

		public static void calclulateOriginalDirection(List<VertexData> v)
		{

			for(int i = 0; i < v.Count -1; i++)
			{
				v[i]._original_dir = (v[i+1]._pos-v[i]._pos ).normalized;	
			}

		}


		// given a 2D vector, return the normalized Bisector vector
		// it points to the left side of cur_vec
		private static Vector3 BiSector(Vector3 prev_vec, Vector3 cur_vec, Vector3 cur_vec_norm)
		{
			Vector3 bisector, prev_vec_n, cur_vec_n;

			prev_vec_n = prev_vec.normalized;
			cur_vec_n  = cur_vec.normalized;
			bisector   = (prev_vec_n + cur_vec_n) / 2;

			if(bisector.magnitude < 0.001f)
			{
				bisector.x = -cur_vec_n.y;
				bisector.y = cur_vec_n.x;
				bisector.z = cur_vec_n.z;
			}
			else
			{
				bisector   = bisector.normalized;
			}

			if(bisector.x * cur_vec_norm.x + bisector.y * cur_vec_norm.y < 0)
			{
				bisector = -bisector;
			}

			return bisector;
		}


		public static List<VertexData> CorrectInputData(List<VertexData> v, float min_distance=1, float init_distance=1, 
			int iterations=2, float simplify_tolerance=625.0f)
		{
			List<VertexData> result = new List<VertexData>();

			result.Add(v[0]);
			for(int i = 1; i < v.Count-1; i++)
			{
				float dist = (v[i-1]._pos - v[i]._pos).sqrMagnitude;
				if(dist < min_distance && (i > 1 || dist < init_distance))
				{
				}
				else
				{
					result.Add(v[i]);
				}
			}

			result.Add(v[v.Count-1]);
			//simplify our new line
			result = ResolveData(result, iterations, simplify_tolerance);
			return result;
		}

		private static List<VertexData> ResolveData(List<VertexData> input, int iterations, float simplify_tolerance)
		{

			List<VertexData> tmp = new List<VertexData>();
			List<VertexData> output = new List<VertexData>();

			output.Clear();
			if (input.Count <= 1) 
			{ 
				//simple copy
				output.AddRange(input);

				return output;
			}

			//simplify with squared tolerance
			if (simplify_tolerance > 0 && input.Count >= 2) 
			{
				simplify(input, simplify_tolerance, tmp);
				input = tmp;
			}

			//perform smooth operations
			if (iterations <= 0) 
			{ 
				//no smooth, just copy input to output
				output.AddRange(input);
			} 
			else if (iterations==1) 
			{ 
				//1 iteration, smooth to output
				smooth(input, output);
			} 
			else 
			{ 
				//multiple iterations.. ping-pong between arrays
				int iters = iterations;
				//subsequent iterations
				do 
				{
					smooth(input, output);
					tmp.Clear();
					tmp.AddRange(output);
					//List<VertexData> old = output;
					input = tmp;
					//output = old;
				} while (--iters > 0);
			}

			return output;
		}

		private static void smooth(List<VertexData> input, List<VertexData> output) 
		{
			//expected size
			output.Clear();

			//first element
			output.Add(input[0]);

			//average elements
			for (int i = 0; i < input.Count-1; i++) 
			{
				Vector3 p0 = input[i]._pos;
				Vector3 p1 = input[i+1]._pos;

				Vector3 Q = Vector3.Lerp(p0, p1, 0.25f);
				Vector3 R = Vector3.Lerp(p0, p1, 0.75f);

				float Q_width = Mathf.Lerp(input[i]._half_width, input[i+1]._half_width, 0.25f);
				float R_width = Mathf.Lerp(input[i]._half_width, input[i+1]._half_width, 0.75f);	
				Color32 Qc  = Color32.Lerp(input[i]._color, input[i+1]._color, 0.25f);
				Color32 Rc	= Color32.Lerp(input[i]._color, input[i+1]._color, 0.75f);
				output.Add(new VertexData(Q, Q_width, Qc));
				output.Add(new VertexData(R, R_width, Rc));
			}

			//last element
			output.Add(input[input.Count-1]);
		}

		//simple distance-based simplification
		//adapted from simplify.js
		private static void simplify(List<VertexData> points, float sqTolerance, List<VertexData> output) 
		{
			int len = points.Count;

			VertexData point = points[0];
			VertexData prevPoint = points[0];

			output.Clear();
			output.Add(prevPoint);

			for (int i = 1; i < len; i++) {
				point = points[i];
				if ((point._pos-prevPoint._pos).sqrMagnitude > sqTolerance) 
				{
					output.Add(point);
					prevPoint = point;
				}
			}

			if (prevPoint._pos != point._pos) {
				output.Add(point);
			}
		}

		private static void generate(List<VertexData> input, float repeat_uvs,
			out Vector3[] verts, out Vector2[] uvs, out int[] tris, out Color32[] colors, bool should_update_verts = true, 
			bool should_update_uvs = true, bool should_update_triangles = true, float uv_offset = 0)
		//out Color[] colors, out Vector3[] norms) 
		{
			int num_verts = 3 * input.Count;
			verts  		  = new Vector3[num_verts];
			uvs    		  = new Vector2[num_verts];
			tris   		  = new int[12*(input.Count-1)];
			colors 		  = new Color32[num_verts];
			//norms  = new Vector3[num_verts];

			for(int i = 0, three_times_i = 0; i < input.Count; i++, three_times_i += 3)
			{
				int three_times_i_plus_1 = three_times_i + 1;
				int three_times_i_plus_2 = three_times_i + 2;

				if(should_update_verts)
				{
					Vector3 cur_vec, prev_vec, norm, bisector, perp;
					if(i == 0)
					{
						cur_vec    = input[i+1]._pos - input[i]._pos;
						prev_vec   = -cur_vec;
						prev_vec.z = cur_vec.z;
					}
					else if(i == input.Count - 1)
					{
						prev_vec  = input[i-1]._pos - input[i]._pos;
						cur_vec   = -prev_vec;
						cur_vec.z = prev_vec.z;
					}
					else
					{
						prev_vec = input[i-1]._pos - input[i]._pos;
						cur_vec  = input[i+1]._pos - input[i]._pos;
					}

					// normal vector of cur_vec
					norm = new Vector3(-cur_vec.y, cur_vec.x, cur_vec.z);
					norm = norm.normalized;

					// compute bisector
					bisector = input[i]._half_width * BiSector(prev_vec, cur_vec, norm);

					float dot_product = Vector3.Dot(bisector, norm);
					// scale 
					float scale_factor = dot_product < Mathf.Epsilon ? 2.0f : Mathf.Min(2.0f, input[i]._half_width / dot_product);

					perp = scale_factor * bisector;

					verts[three_times_i		  ] = input[i]._pos + perp;
					verts[three_times_i_plus_1] = input[i]._pos;
					verts[three_times_i_plus_2] = input[i]._pos - perp;

					colors[three_times_i	   ] = input[i]._color;
					colors[three_times_i_plus_1] = input[i]._color;
					colors[three_times_i_plus_2] = input[i]._color;
				}


				if(should_update_uvs)
				{
					float v_value 			  = uv_offset + input[i]._length_ratio * repeat_uvs;

					uvs[three_times_i		] = new Vector2(0.0f, v_value);
					uvs[three_times_i_plus_1] = new Vector2(0.5f, v_value);
					uvs[three_times_i_plus_2] = new Vector2(1.0f, v_value);
				}


				if(should_update_triangles)
				{
					int tweleve_times_i = 12 * i;

					if(i>0)
					{
						tris[tweleve_times_i-12] = three_times_i - 3;
						tris[tweleve_times_i-11] = three_times_i + 1;
						tris[tweleve_times_i-10] = three_times_i - 2;

						tris[tweleve_times_i-9 ] = three_times_i - 3;
						tris[tweleve_times_i-8 ] = three_times_i    ;
						tris[tweleve_times_i-7 ] = three_times_i + 1;

						tris[tweleve_times_i-6 ] = three_times_i - 2;
						tris[tweleve_times_i-5 ] = three_times_i + 2;
						tris[tweleve_times_i-4 ] = three_times_i - 1;

						tris[tweleve_times_i-3 ] = three_times_i - 2;
						tris[tweleve_times_i-2 ] = three_times_i + 1;
						tris[tweleve_times_i-1 ] = three_times_i + 2;
					}
				}

				/*colors[two_times_i]	 	  = Color.white;
					colors[two_times_i_plus_1]= Color.white;
					norms[two_times_i] 	 	  = -Vector3.forward;
					norms[two_times_i_plus_1] = -Vector3.forward;
					*/
			}
		}


		public static GameObject CreateMeshFromPath(List<VertexData> path, float repeat_uvs, Material material, GameObject ref_go=null, int start_index=0,
			float uv_offset = 0)
		{

			try{
				if (path.Count<2)
					return null;

				//CombineInstance[] mesh_combine_instance = new CombineInstance[path.Count-1];
				Mesh result_mesh;

				GameObject result_go = new GameObject ();
				result_go.name  = "OnePathMesh";

				MeshFilter mf 	= result_go.AddComponent<MeshFilter>  ();
				MeshRenderer mr = result_go.AddComponent<MeshRenderer>();
				//float path_len = ComputePathLength(path);


				mr.material 	= new Material(material);


				result_mesh 	= new Mesh();

				mf.mesh     	= result_mesh;



				//mr.material.mainTexture = (Texture2D)(Resources.Load("1_1", typeof(Texture2D)));

				Vector3[] verts;
				Vector2[] uvs;
				int[]     tris;
				Color32[] colors;
				//			Vector3[] norms;

				generate(path, repeat_uvs, out verts, out uvs, out tris, out colors, true, true, true, uv_offset);

				/*List<Color> colors = new List<Color>(left_colors);
			colors.AddRange(right_colors);
			
			List<Vector3> norms = new List<Vector3>(left_norms);
			norms.AddRange(right_norms);
			*/

				//				Debug.Log("verts-----------"+verts.Length);
				//				Debug.Log("tris-----------"+tris.Length);
				result_mesh.vertices  = verts;
				result_mesh.triangles = tris;
				result_mesh.uv		  = uvs;
				result_mesh.colors32  = colors;

				if(ref_go != null)
				{
					colors 				  = result_mesh.colors32;
					Color32[]	ref_color = ref_go.GetComponent<MeshFilter>().mesh.colors32;
					//					Debug.Log("ref_color-----------"+ref_color.Length);
					for(int i = 0; i < colors.Length; i++)
					{
						if(start_index + i < ref_color.Length)
						{
							colors[i] = ref_color[start_index+i];
						}
					}

					result_mesh.colors32 = colors;
				}

				//result_mesh.normals   = norms.ToArray();
				//result_mesh.colors    = colors.ToArray();

				result_mesh.RecalculateBounds();
				//mc.sharedMesh = null;
				//mc.sharedMesh = result_mesh;
				return result_go;
			}catch(UnityException e)
			{
				Debug.LogError ("" + e.Message);
			}


			return null;
		}

		private static void UpdateGameObjectVertex(List<VertexData> path, float repeat_uvs, GameObject go)
		{
			if (path.Count<2)
				return ;

			Mesh result_mesh = go.GetComponent<MeshFilter> ().mesh;
			//MeshCollider mc  = go.GetComponent<MeshCollider>();

			Vector3[] verts;
			Vector2[] uvs;
			int[]     tris;
			Color32[]   colors;
			//			Vector3[] norms;

			generate(path, repeat_uvs, out verts, out uvs, out tris, out colors, true, false, false);

			result_mesh.vertices  = verts;
			//result_mesh.triangles = tris.ToArray();
			//result_mesh.uv		  = uvs.ToArray();
			//result_mesh.normals   = norms.ToArray();
			//result_mesh.colors    = colors.ToArray();

			result_mesh.RecalculateBounds();

			//mc.sharedMesh = null;
			//mc.sharedMesh = result_mesh;
		}

		private static void UpdateGameObjectVertexAndUV(List<VertexData> path, float repeat_uvs, GameObject go)
		{
			if (path.Count<2)
				return ;

			Mesh result_mesh = go.GetComponent<MeshFilter> ().mesh;
			//MeshCollider mc  = go.GetComponent<MeshCollider>();

			Vector3[] verts;
			Vector2[] uvs;
			int[]     tris;
			Color32[]   colors;
			//			Vector3[] norms;

			generate(path, repeat_uvs, out verts, out uvs, out tris, out colors, true, true, false);

			result_mesh.vertices  = verts;
			//result_mesh.triangles = tris.ToArray();
			result_mesh.uv		  = uvs;
			//result_mesh.normals   = norms.ToArray();
			//result_mesh.colors    = colors.ToArray();

			result_mesh.RecalculateBounds();

			//mc.sharedMesh = null;
			//mc.sharedMesh = result_mesh;
		}

		private static void UpdateGameObjectVertexAndUVAndTriangles(List<VertexData> path, float repeat_uvs, GameObject go)
		{
			if (path.Count<2)
				return ;

			Mesh result_mesh = go.GetComponent<MeshFilter> ().mesh;
			/*if(result_mesh != null)
				GameObject.Destroy(result_mesh);
			
				
			MeshFilter mf 	= go.GetComponent<MeshFilter>  ();
			result_mesh 	= new Mesh();
            mf.mesh     	= result_mesh;
			*/
			Vector3[] verts;
			Vector2[] uvs;
			int[]     tris;
			Color32[] colors;
			//			Vector3[] norms;

			generate(path, repeat_uvs, out verts, out uvs, out tris, out colors, true, true, true);

			result_mesh.Clear();
			result_mesh.vertices  = verts;
			result_mesh.uv		  = uvs;
			result_mesh.colors32  = colors;
			result_mesh.triangles = tris;


			//result_mesh.RecalculateBounds();

			//mc.sharedMesh = null;
			//mc.sharedMesh = result_mesh;
		}


		//		// TODO: remove this function
		//		public static GameObject CreateMazeHintPath(List<VertexData> v, List<Material> material, 
		//			out Vector3[] pos, out Vector2[] uv, ref List<VertexData> path, float uv_offset, bool apply_repeat_uv = true, 
		//			bool apply_random_uv = true, int iterations = 4, float simplify_tolerance = 16.0f)
		//	    {
		//			
		//			if(v.Count < 2)
		//			{
		//				pos = null;
		//				uv  = null;
		//				return null;
		//			}
		//			path = new List<VertexData>();
		//			
		//			// Copy data
		//			for(int i = 0; i< v.Count; i++)
		//				path.Add(new VertexData(v[i]));
		//			
		//			if(iterations > 0)
		//			{
		//				path = CorrectInputData(path, m_min_distance, m_init_distance, iterations, simplify_tolerance);
		//			}
		//			
		//			ComputeLengthRatio(path);
		//			Material select_material = material[Random.Range(0, material.Count)];
		//			float repeat_uv;
		//			
		//			if(apply_repeat_uv)
		//			{
		//				float uv_times = path[path.Count-1]._length / select_material.mainTexture.height;
		//				
		//				
		//				if(apply_random_uv)
		//				{
		//					repeat_uv = Random.Range(0.5f*uv_times, 2.0f*uv_times);
		//				}
		//				else
		//				{
		//					repeat_uv = path[path.Count-1]._length * select_material.mainTexture.width / (select_material.mainTexture.height * path[path.Count-1]._half_width * 2.0f);	
		//				}
		//			}
		//			else
		//			{
		//				repeat_uv = 1.0f;
		//			}
		//			GameObject result_go =  CreateMeshFromPath(path, repeat_uv, select_material, null, 0, uv_offset);
		//			
		//			
		//			Mesh old_mesh = result_go.GetComponent<MeshFilter> ().mesh;
		//			pos = old_mesh.vertices;
		//			uv  = old_mesh.uv;
		//			return result_go;
		//		}
		//		
		//		public static void HideMazeHintPath(int prev_index,  int cur_index, float ratio, Vector3[] orig_verts, Vector2[] orig_uvs,
		//			ref GameObject go, out float uv_offset)
		//		{
		//			Mesh mesh = go.GetComponent<MeshFilter> ().mesh;
		//			
		//			int three_time_index 		   = 3 * cur_index;
		//			int three_time_index_plus_one = three_time_index + 1;
		//			int three_time_index_plus_two = three_time_index + 2;
		//			
		//			int three_time_index2 		   = 3 * (cur_index+1);
		//			int three_time_index_plus_one2 = three_time_index2 + 1;
		//			int three_time_index_plus_two2 = three_time_index2 + 2;
		//			
		//			Vector3[] verts;
		//			verts = mesh.vertices;
		//			
		//			verts[three_time_index		   ] = Vector3.Lerp(orig_verts[three_time_index],orig_verts[three_time_index2], ratio);
		//			verts[three_time_index_plus_one] = Vector3.Lerp(orig_verts[three_time_index_plus_one],orig_verts[three_time_index_plus_one2], ratio);
		//			verts[three_time_index_plus_two] = Vector3.Lerp(orig_verts[three_time_index_plus_two],orig_verts[three_time_index_plus_two2], ratio);
		//			mesh.vertices  = verts;
		//			
		//			
		//			
		//			Vector2[] uvs;
		//			uvs = mesh.uv;
		//			uvs[three_time_index		 ] = Vector2.Lerp(orig_uvs[three_time_index],orig_uvs[three_time_index2], ratio);
		//			uvs[three_time_index_plus_one] = Vector2.Lerp(orig_uvs[three_time_index_plus_one],orig_uvs[three_time_index_plus_one2], ratio);
		//			uvs[three_time_index_plus_two] = Vector2.Lerp(orig_uvs[three_time_index_plus_two],orig_uvs[three_time_index_plus_two2], ratio);
		//			mesh.uv  = uvs;
		//			
		//			uv_offset = uvs[three_time_index].y;
		//			
		//			Color32[] colors;
		//			colors = mesh.colors32;
		//			for(int i = prev_index, idx = 3*prev_index; i < cur_index; i++, idx += 3)
		//			{
		//				colors[idx  ].a = 0;
		//				colors[idx+1].a = 0; 
		//				colors[idx+2].a = 0;
		//			}
		//			mesh.colors32  = colors;
		//		}
		//		
		//		public static GameObject CreateMeshWithoutSmooth(List<VertexData> v, List<Material> material, bool apply_random_uv = true)
		//	    {
		//			if(v.Count < 2)
		//				return null;
		//			
		//			
		//			ComputeLengthRatio(v);
		//			Material select_material = material[Random.Range(0, material.Count)];
		//			float uv_times = v[v.Count-1]._length / select_material.mainTexture.height;
		//			float repeat_uv;
		//			
		//			if(apply_random_uv)
		//			{
		//				repeat_uv = Random.Range(0.5f*uv_times, 2.0f*uv_times);
		//			}
		//			else
		//			{
		//				repeat_uv = v[v.Count-1]._length * select_material.mainTexture.width / (select_material.mainTexture.height * v[v.Count-1]._half_width * 2.0f);	
		//			}
		//			return CreateMeshFromPath(v, repeat_uv, select_material, null, 0);
		//		}
		//		
		public static GameObject CreateMesh(List<VertexData> v, float repeat_uvs, List<Material> material, bool is_finish_input, out List<VertexData> path)
		{
			//			List<VertexData> v2;
			path = new List<VertexData>();

			if(v.Count < 2)
				return null;

			path = CorrectInputData(v, m_min_distance, m_init_distance, m_iterations, m_simplify_tolerance);
			//int last_pt_idx = v2.Count-1;

			/*if(!is_finish_input)
			{
				last_pt_idx = v2.Count - 2;
				//if(v2.Count < 3)
				//	return null;
			}*/



			/*	
			for(int i = 0; i < last_pt_idx; i++)
			{
				int p0 = Mathf.Max(0, i-1);
				int p1 = i;
				int p2 = Mathf.Min(i+1, v2.Count-1);
				int p3 = Mathf.Min(i+2, v2.Count-1);

				sample_v.AddRange(DeriveCatmullRomSamples(v2[p0], v2[p1], v2[p2], v2[p3], m_sample_interval));
			}
			*/
			ComputeLengthRatio(path);

			return CreateMeshFromPath(path, repeat_uvs, material[path[0]._material_index], null, 0);
		}	

		public static int IntersectsExistingLines(List<VertexData> points, Vector3 a, Vector3 b, out VertexData inter_pt)
		{
			for (int i = 0; i < points.Count-1; i++)
			{
				if (LinesIntersect(a, b, points[i], points[i+1], out inter_pt))
				{
					return i;
				}
			}

			inter_pt = null;
			return -1;
		}

		private static bool LinesIntersect(Vector3 point1, Vector3 point2, VertexData vt1, VertexData vt2, out VertexData inter_pt)
		{
			Vector3 point3 = vt1._pos;
			Vector3 point4 = vt2._pos;

			if (point1 == point3 || point1 == point4 || point2 == point3 || point2 == point4)
			{
				inter_pt = null;
				return false;
			}

			float ua = (point4.x - point3.x) * (point1.y - point3.y) - (point4.y - point3.y) * (point1.x - point3.x);
			float ub = (point2.x - point1.x) * (point1.y - point3.y) - (point2.y - point1.y) * (point1.x - point3.x);
			float denominator = (point4.y - point3.y) * (point2.x - point1.x) - (point4.x - point3.x) * (point2.y - point1.y);

			if (Mathf.Abs(denominator) <= 0.00001f)
			{
				if (Mathf.Abs(ua) <= 0.00001f && Mathf.Abs(ub) <= 0.00001f)
				{
					inter_pt = vt1;
					if(vt1._length < m_min_distance)
						return false;
					return true;
				}
			}
			else
			{
				ua /= denominator;
				ub /= denominator;

				if (ua >= 0 && ua <= 1 && ub >= 0 && ub <= 1)
				{
					inter_pt = new VertexData(Vector3.Lerp(point3, point4, ub), Mathf.Lerp(vt1._half_width, vt2._half_width, ub));		
					inter_pt._length_ratio = Mathf.Lerp(vt1._length_ratio, vt2._length_ratio, ub);	
					inter_pt._length       = Mathf.Lerp(vt1._length, vt2._length, ub);
					inter_pt._color		   = Color.Lerp(vt1._color, vt2._color, ub);
					if(inter_pt._length < m_min_cut_length)
						return false;
					return true;
				}
			}

			inter_pt = null;
			return false;
		}

		public static void CutPathMesh(List<VertexData> input_path, float repeat_uvs, Material material, 
			int split_vertex_index,  VertexData inter_pt, Vector3 start_pt, Vector3 end_pt,
			out List<VertexData> remain_path, out GameObject remain_obj, out GameObject cut_obj, GameObject orig_obj)
		{
			remain_path = new List<VertexData>();
			remain_path = input_path.GetRange(0, split_vertex_index+1);
			remain_path.Add(inter_pt);


			List<VertexData> cut_path = new List<VertexData>();
			cut_path = input_path.GetRange(split_vertex_index+1, input_path.Count - split_vertex_index - 1);
			// if split_vertix_index is between the last two vertex, then we just add the last
			if(cut_path.Count == 0)
				cut_path.Add(input_path[input_path.Count-1]);
			cut_path.Insert(0, new VertexData(inter_pt));

			ComputeLengthRatio(remain_path);
			remain_obj = CreateMeshFromPath(remain_path, repeat_uvs, material, orig_obj, 0);
			cut_obj = CreateMeshFromPath(cut_path, repeat_uvs, material, orig_obj, 3*split_vertex_index);

			if(cut_obj != null)
			{
				//GameObject.DestroyImmediate(cut_obj.GetComponent(typeof(MeshCollider)));
				Rigidbody rb = cut_obj.AddComponent<Rigidbody>();
				rb.mass = 200.0f;
				rb.drag = 0.0f;
				rb.angularDrag = 0.0f;
				rb.centerOfMass = cut_path[0]._pos;
				const float gravityStrength = 500.0f;
				Vector3 upForce    = Vector3.up * 100;
				Physics.gravity    = new Vector3(0.0f, -gravityStrength, 0.0f);
				Vector3 cut_vector = end_pt - start_pt; 
				float cut_vector_mag = Mathf.Clamp(10*cut_vector.magnitude, 30, 150);
				Vector3 sideForce  = cut_vector.normalized * Random.Range(0.2f*cut_vector_mag, 1.2f*cut_vector_mag);
				Vector3 combinedForce = sideForce + upForce;

				rb.useGravity = true;
				rb.velocity = cut_obj.transform.TransformDirection(combinedForce);
				//rb.AddTorque(Vector3.up * 10);
				rb.angularVelocity = new Vector3(0, 0, Random.Range(-2, 2));
			}
		}

		//		public static void RandomCreatePaths(float repeat_uvs, List<Material> material, int num_of_path, Rect reg, 
		//			float half_width, out List<List<VertexData>> paths, out List<GameObject> gos)
		//		{
		//			paths = new List<List<VertexData>>();
		//			gos   = new List<GameObject>();
		//			
		//			for(int i = 0; i < num_of_path; i++)
		//			{
		//				List<VertexData> one_path = new List<VertexData>();
		//				List<VertexData> one_result_path;
		//				
		//				Vector3 start_pt = new Vector3(Random.Range(reg.xMin, reg.xMax), Random.Range(reg.yMin, reg.yMax) - 768, 0);
		//				one_path.Add(new VertexData(start_pt, half_width));
		//				one_path[0]._material_index = Random.Range(0, material.Count);
		//				
		//				int path_num_of_points = Random.Range(10, 30);
		//				float angle = 0;
		//				
		//				for(int j = 0; j < path_num_of_points; j++)
		//				{
		//					Vector3 new_pt;
		//					float step_size = Random.Range(10.0f, 20.0f);
		//					
		//					if(j == 0)
		//					{
		//						angle = Random.Range(0, Mathf.PI*2);
		//						new_pt = start_pt + new Vector3(step_size*Mathf.Cos(angle), step_size*Mathf.Sin(angle), 0);
		//					}
		//					else
		//					{
		//						angle = angle + Random.Range(-Mathf.PI/8, Mathf.PI/8);
		//						new_pt = start_pt + new Vector3(step_size*Mathf.Cos(angle), step_size*Mathf.Sin(angle), 0);
		//					}
		//					
		//					start_pt = new_pt;
		//					one_path.Add(new VertexData(start_pt, half_width));
		//				}
		//				
		//				gos.Add(CreateMesh(one_path, repeat_uvs, material, true, out one_result_path));
		//				one_path.Clear();
		//				paths.Add(one_result_path);
		//			}
		//		}


		public static void Serialize (string path, System.Type type, object o)
		{
			string absolutePath  = path;
			string fileName      = Path.GetFileName(absolutePath);
			string directoryPath = absolutePath.Replace(fileName, "");

			DirectoryInfo dir = new DirectoryInfo(directoryPath);
			if (!dir.Exists)
				dir.Create();

			XmlSerializer serializer   = new XmlSerializer(type);

			Debug.Log(path);

			TextWriter    textWriter   = new StreamWriter(absolutePath);
			serializer.Serialize(textWriter, o);
			textWriter.Close();
		}

		public static object Deserialize (string path, System.Type type)
		{
			string absolutePath  = path;
			if (!File.Exists(absolutePath))			
				return null;

			//			Debug.Log(absolutePath);
			XmlSerializer serializer   = new XmlSerializer(type);
			TextReader    textReader   = new StreamReader(absolutePath);
			object o = serializer.Deserialize(textReader);
			textReader.Close();

			return o;
		}

		public static void LoadPathData(string name, float repeat_uvs, List<Material> material, out List<List<VertexData>> out_mesh, out List<GameObject> out_gos)
		{
			string data_file = Application.persistentDataPath + "/char_1";
			//PageManager.Instance.userDataDir+"/" + name;
			out_mesh = new List<List<VertexData>>();
			out_gos  = new List<GameObject>();

			if(File.Exists(data_file))
			{
				// Read saved info
				object obj = Deserialize(data_file, typeof(AUPathMeshData));
				if (obj != null)
				{
					out_mesh = ((AUPathMeshData)obj).m_mesh_data;
					out_gos  = new List<GameObject>();
					for(int i = 0; i < out_mesh.Count; i++)
					{
						out_mesh[i] = CorrectInputData(out_mesh[i], m_min_distance, m_init_distance, m_iterations, m_simplify_tolerance);

						ComputeLengthRatio(out_mesh[i]);
						GameObject one_go = CreateMeshFromPath(out_mesh[i], repeat_uvs, material[out_mesh[i][0]._material_index], null, 0);
						out_gos.Add(one_go);
					}
				}
			}
		}
//		public static Texture curlyTexture;
		public static void LoadDefaultPathData(string data_file, float repeat_uvs, List<Material> material, out List<List<VertexData>> out_mesh, out List<GameObject> out_gos)
		{

			out_mesh = new List<List<VertexData>>();
			out_gos  = new List<GameObject>();
			if(File.Exists(data_file))
			{
				// Read saved info
				object obj = Deserialize(data_file, typeof(AUPathMeshData));
				if (obj != null)
				{
					out_mesh = ((AUPathMeshData)obj).m_mesh_data;
					if(out_mesh.Count>52)
					{
						out_mesh.RemoveAt (0);
					}
					out_gos  = new List<GameObject>();

//					for (int i = 0; i < out_mesh.Count; i++) {
//						for (int j = 0; j < out_mesh[i].Count; j++) {
//							out_mesh [i] [j]._color = new Color32 (0,255,255,255);
//						}
//					}

					for(int i = 0; i < out_mesh.Count; i++)
					{

						out_mesh[i] = CorrectInputData(out_mesh[i], m_min_distance, m_init_distance, m_iterations, m_simplify_tolerance);

						ComputeLengthRatio(out_mesh[i]);
						calclulateOriginalDirection (out_mesh [i]);
						GameObject one_go = CreateMeshFromPath(out_mesh[i], repeat_uvs, material[out_mesh[i][0]._material_index], null, 0);
						one_go.AddComponent<SortingGroup> ();
						int count = out_mesh [i].Count;
						if (count > 0) {
							VertexData data = out_mesh [i] [0];
						
							AppOn_HairGame.Instance.addHairBase (one_go,data);
						}
						//						float a = AppOn_HairGame.Instance.SetHair_Zvalue (i);
						//						Vector3 ve = new Vector3 (one_go.transform.position.x,one_go.transform.position.y,-out_mesh[i][0]._pos.y *  a);
						//						one_go.transform.position = ve;
						out_gos.Add(one_go);
						MeshRenderer mr = out_gos[i].GetComponent<MeshRenderer>();
						originalTexture=mr.material.GetTexture ("_MainTex");
					}
				}
			}
			else
			{
				Debug.Log(data_file+" is not exits");
			}
		}
		public static string path="";
		 
		public static void SaveHairs(string filePath, string fileName,
			List<List<VertexData>> out_mesh,string name="AuBkEngCrazyHair_hair_style1"){
			#if UNITY_EDITOR
			 path=filePath+"/" +fileName;
			#else
			 path= Application.persistentDataPath+"/" +fileName;
			#endif
			AUPathMeshData listOfMesh=new AUPathMeshData();

			listOfMesh.m_mesh_data=out_mesh;
			for (int i = 0; i < listOfMesh.m_mesh_data.Count; i++) {
				for (int j = 0; j < listOfMesh.m_mesh_data[i].Count; j++) {

					listOfMesh.m_mesh_data [i] [j]._color =new Color32 (211,172,0, 255);
					listOfMesh.m_mesh_data [i] [j]._origcolor = new Color32 (255,255,255, 255);
				}
			}

			Serialize(path,typeof(AUPathMeshData),listOfMesh);
		}

		public static void RemoveMeshCollider(ref List<GameObject> gos)
		{
			for(int i = 0; i < gos.Count; i++)
			{
				GameObject.DestroyImmediate(gos[i].GetComponent(typeof(MeshCollider)));
			}
		}

		public static void AddMeshCollider(ref List<GameObject> gos)
		{
			for(int i = 0; i < gos.Count; i++)
			{
				if(gos[i].GetComponent(typeof(MeshCollider)) == null)
				{
					//Debug.Log(gos[i].name);
					MeshCollider mc  = gos[i].AddComponent<MeshCollider>();
					mc.sharedMesh 	 = null;
					Mesh mesh = gos[i].GetComponent<MeshFilter>().mesh;
					try{
						mc.sharedMesh 	 = mesh;
					}catch(UnityException e)
					{
						Debug.LogError ("" + e.Message);
					}

				}
			}
		}

		// Compute the main axis of a curve
		private static Vector3 ComputeMainAxis(List<VertexData> points)
		{
			float sum_x = 0.0f, sum_y = 0.0f;

			for(int i = 0; i < points.Count; i++)
			{
				sum_x += points[i]._pos.x;
				sum_y += points[i]._pos.y;
			}

			return new Vector3(sum_x / points.Count - points[0]._pos.x, sum_y / points.Count - points[0]._pos.y, 0).normalized;
		}
		static bool isUseDrieOnHair=false;
		// Apply drier effect to hair
		public static void ApplyDrierHair(float repeat_uvs, List<Material> material, Vector3 position, List<List<VertexData>> in_mesh, 
			out List<List<VertexData>> out_mesh, ref List<GameObject> out_gos)
		{
			isUseDrieOnHair=false;
			List<List<VertexData>> in_mesh_data = in_mesh;
			out_mesh = new List<List<VertexData>>();


			for(int i = 0; i < in_mesh_data.Count; i++)
			{
				if(in_mesh_data[i].Count > 0)
				{
					List<VertexData> one_hair = new List<VertexData>();

					// Add the hair root (first vertex)
					one_hair.Add(in_mesh_data[i][0]);

					// Get wind direction
					Vector3 vdir = (in_mesh_data[i][0]._pos - position).normalized;	

					Vector3 axis_dir = ComputeMainAxis(in_mesh_data[i]);

					// Perturb direction
					float rot_angle  = Mathf.Atan2(vdir.y, vdir.x) - Mathf.Atan2(axis_dir.y, axis_dir.x) + Random.Range(-Mathf.PI/18.0f, Mathf.PI/18.0f);
					//vdir 		 = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), vdir.z);
					float cos_rot_angle = Mathf.Cos(rot_angle);
					float sin_rot_angle = Mathf.Sin(rot_angle);

					for(int j = 1; j < in_mesh_data[i].Count; j++)
					{
						Vector3 offset = in_mesh_data[i][j]._pos - in_mesh_data[i][0]._pos;
						Vector3 new_pt = new Vector3(in_mesh_data[i][0]._pos.x + offset.x * cos_rot_angle - offset.y * sin_rot_angle, 
							in_mesh_data[i][0]._pos.y + offset.x * sin_rot_angle + offset.y * cos_rot_angle,
							in_mesh_data[i][0]._pos.z);

						one_hair.Add(new VertexData(new_pt, in_mesh_data[i][j]._half_width, 
							in_mesh_data[i][j]._length_ratio, in_mesh_data[i][j]._length, in_mesh_data[i][j]._color, in_mesh_data[i][j]._material_index));
					}

					out_mesh.Add(one_hair);
				}
			}

			for(int i = 0; i < out_mesh.Count; i++)
			{
				if(out_gos.Count > i)
				{
					isUseDrieOnHair = true;
					UpdateGameObjectVertex(out_mesh[i],  repeat_uvs,  out_gos[i]);
//					out_gos [i].GetComponent<SortingGroup> ().sortingOrder = 0;
				}
			}
			if(isUseDrieOnHair)
			{
				AppOn_HairGame.Instance.setHairDrierAnim ();
			}
		}

		private static float ComputeFallOffWeight(float distance)
		{
			float v = (1.0f - distance);
			return v * v;
		}

		// Compute the point to line segment distance
		private static float PointToLineSqrDistance(Vector3 l1, Vector3 l2, Vector3 pt) 
		{
			Vector3 vdir = l2 - l1;
			float len = vdir.sqrMagnitude;

			if (len == 0.0f) 
				return (pt - l1).sqrMagnitude;

			float t = Vector3.Dot((pt - l1), vdir) / len;
			if(t < 0.0f)
				return (pt - l1).sqrMagnitude;
			else if(t > 1.0f)
				return (pt - l2).sqrMagnitude;
			Vector3 projection = l1 + t * vdir;
			return (pt - projection).sqrMagnitude;
		}

		private static float LerpAngle (float a, float b, float t)
		{
			float num = Mathf.Repeat (b - a, 2*Mathf.PI);
			if (num > Mathf.PI)
			{
				num -= Mathf.PI * 2;
			}
			return a + num * Mathf.Clamp01 (t);
		}

		// Apply comb effect to hair : comb1
		public static void ApplyCombHair(float repeat_uvs, List<Material> material, Vector3 position, Vector3 prev_position, List<HitInfo> hit_mesh, 
			ref List<List<VertexData>> out_mesh, ref List<GameObject> out_gos)
		{
			//			AppOn.HairGame.AppOn_HairGame.Instance.Comb1Select_Audio.Play ();
			Vector3 vdir 		= (position - prev_position);
			float length_weight = Mathf.Clamp(vdir.magnitude / 20.0f, 0, 1);
			const float affect_radius_sqr = 20.0f * 20f;

			// Compute the user defined motion angle
			float motion_angle  = Mathf.Atan2(vdir.y, vdir.x);

			for(int i = 0; i < hit_mesh.Count; i++)
			{
				int idx = hit_mesh[i].m_index;
				if(out_mesh[idx].Count > 0)
				{
					List<VertexData> one_hair = new List<VertexData>();
					for(int j = 0; j < out_mesh[idx].Count; j++)
					{
						one_hair.Add(out_mesh[idx][j]);
					}

					for(int j = 0; j < out_mesh[idx].Count-1; j++)
					{
						float pt_to_line_dist = PointToLineSqrDistance(prev_position, position,one_hair[j]._pos); 

						float distance   = Mathf.Clamp(pt_to_line_dist / affect_radius_sqr, 0.0f, 1.0f);

						if(distance >= 1.0f)
							continue;

						float weight        = ComputeFallOffWeight(distance) * length_weight;
						Vector3 orig_offset = one_hair[j+1]._pos - one_hair[j]._pos;
						float  orig_angle	= Mathf.Atan2(orig_offset.y, orig_offset.x);
						float rot_angle  	= LerpAngle(0, motion_angle-orig_angle, weight);
						for(int k = j+1; k < one_hair.Count; k++)
						{
							Vector3 offset   = one_hair[k]._pos - one_hair[j]._pos;

							float cos_rot_angle = Mathf.Cos(rot_angle);
							float sin_rot_angle = Mathf.Sin(rot_angle);

							one_hair[k]._pos.x = one_hair[j]._pos.x + offset.x * cos_rot_angle - offset.y * sin_rot_angle;
							one_hair[k]._pos.y = one_hair[j]._pos.y + offset.x * sin_rot_angle + offset.y * cos_rot_angle;
						}
					}

					// Smooth result
					float length = one_hair[one_hair.Count-1]._length;
					one_hair = CorrectInputData(one_hair, m_min_distance, m_init_distance, m_iterations, m_simplify_tolerance);
					ComputeLengthRatio(one_hair);

					// Rescale curve to keep original length
					float scale = length / one_hair[one_hair.Count-1]._length;
					List<VertexData> scale_hair = new List<VertexData>();

					scale_hair.Add(one_hair[0]);

					for(int j = 1; j < one_hair.Count; j++)
					{
						Vector3 vdir2 	  = (one_hair[j]._pos - one_hair[j-1]._pos);	
						Vector3 new_pt 	  = scale_hair[scale_hair.Count-1]._pos + vdir2 * scale;
						scale_hair.Add(new VertexData(new_pt, one_hair[j]._half_width, one_hair[j]._color));
					}

					ComputeLengthRatio(scale_hair);
					out_mesh[idx] = scale_hair;

					if(out_gos.Count > idx)
					{
						// Update mesh info
						UpdateGameObjectVertexAndUVAndTriangles(out_mesh[idx],  repeat_uvs,  out_gos[idx]);

						MeshCollider mc  = out_gos[idx].GetComponent<MeshCollider>();
						if(mc == null)
						{
							mc  = out_gos[idx].AddComponent<MeshCollider>();
						}

						mc.sharedMesh 	 = null;
						mc.sharedMesh 	 = out_gos[idx].GetComponent<MeshFilter>().mesh;
//						out_gos [idx].GetComponent<SortingGroup> ().sortingOrder = 0;
					}

				}
			}
		}

		// Apply comb effect to hair : new comb2
		public static void ApplyCombHair2(float repeat_uvs, List<Material> material, Vector3 position, List<HitInfo> hit_mesh, 
			ref List<List<VertexData>> out_mesh, ref List<GameObject> out_gos)
		{
			//			AppOn.HairGame.AppOn_HairGame.Instance.Comb1Select_Audio.Play ();
			for(int i = 0; i < hit_mesh.Count; i++)
			{
				int idx = hit_mesh[i].m_index;
				if(out_mesh[idx].Count > 0)
				{
					List<VertexData> one_hair = new List<VertexData>();

					// Add the hair root (first vertex) 
					one_hair.Add(out_mesh[idx][0]);

					// Get wind direction
					Vector3 vdir = (position - out_mesh[idx][0]._pos).normalized;	

					// average vector for entire hair
					Vector3 axis_dir = ComputeMainAxis(out_mesh[idx]);

					// Perturb direction, difference of angle between wind and avg vector
					float rot_angle  = Mathf.Atan2(vdir.y, vdir.x) - Mathf.Atan2(axis_dir.y, axis_dir.x);

					// Convert to (-pi, pi]
					if(rot_angle > Mathf.PI)
						rot_angle = rot_angle - Mathf.PI * 2;

					// Interpolate rotate angle based on weight
					rot_angle	= LerpAngle(0, rot_angle, hit_mesh[i].m_weight);

					//vdir 		 = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), vdir.z);
					float cos_rot_angle = Mathf.Cos(rot_angle);
					float sin_rot_angle = Mathf.Sin(rot_angle);

					for(int j=1; j < out_mesh[idx].Count; j++)
					{
						Vector3 offset = out_mesh[idx][j]._pos - out_mesh[idx][0]._pos;
						Vector3 new_pt = new Vector3(out_mesh[idx][0]._pos.x + offset.x * cos_rot_angle - offset.y * sin_rot_angle, 
							out_mesh[idx][0]._pos.y + offset.x * sin_rot_angle + offset.y * cos_rot_angle,
							out_mesh[idx][0]._pos.z);

						one_hair.Add (new VertexData (new_pt, out_mesh [idx] [j]._half_width, 
							out_mesh [idx] [j]._length_ratio, out_mesh [idx] [j]._length, out_mesh [idx] [j]._color, out_mesh [idx] [j]._material_index));

					}


					out_mesh[idx] = one_hair;

					if(out_gos.Count > idx)
					{
						UpdateGameObjectVertex(out_mesh[idx],  repeat_uvs,  out_gos[idx]);

						MeshCollider mc  = out_gos[idx].GetComponent<MeshCollider>();
						mc.sharedMesh 	 = null;
						mc.sharedMesh 	 = out_gos[idx].GetComponent<MeshFilter>().mesh;
//						out_gos [idx].GetComponent<SortingGroup> ().sortingOrder = 0;
					}
				}
			}
		}

		// Apply Water effect to hair
		public static void ApplyWaterHair(float repeat_uvs, List<Material> material, Vector3 position, List<HitInfo> hit_mesh, 
			ref List<List<VertexData>> out_mesh, ref List<GameObject> out_gos)
		{

			for(int i = 0; i < hit_mesh.Count; i++)
			{
				int idx = hit_mesh[i].m_index;
				if(out_mesh[idx].Count > 0)
				{
					List<VertexData> one_hair = new List<VertexData>();

					one_hair.Add (out_mesh [idx] [0]);
					// Get wind direction
					Vector3 vdir = Vector3.down;	

					Vector3 axis_dir = ComputeMainAxis(out_mesh[idx]);

					// Perturb direction
					float rot_angle  = Mathf.Atan2(vdir.y, vdir.x) - Mathf.Atan2(axis_dir.y, axis_dir.x);

					float cos_rot_angle = Mathf.Cos(rot_angle);
					float sin_rot_angle = Mathf.Sin(rot_angle);
					for(int j =1; j < out_mesh[idx].Count; j++)
					{
						Vector3 offset = out_mesh[idx][j]._pos - out_mesh[idx][0]._pos;
						Vector3 new_pt = new Vector3(out_mesh[idx][0]._pos.x + offset.x * cos_rot_angle - offset.y * sin_rot_angle, 
							out_mesh[idx][0]._pos.y + offset.x * sin_rot_angle + offset.y * cos_rot_angle,
							out_mesh[idx][0]._pos.z);

						one_hair.Add(new VertexData(new_pt, out_mesh[idx][j]._half_width, 
							out_mesh[idx][j]._length_ratio, out_mesh[idx][j]._length, out_mesh[idx][j]._color, out_mesh[idx][j]._material_index));
						
					}


					out_mesh[idx] = one_hair;

					if(out_gos.Count > idx)
					{
						
						UpdateGameObjectVertex(out_mesh[idx],  repeat_uvs,  out_gos[idx]);

						MeshCollider mc  = out_gos[idx].GetComponent<MeshCollider>();
						mc.sharedMesh 	 = null;
						mc.sharedMesh 	 = out_gos[idx].GetComponent<MeshFilter>().mesh;
//						out_gos [idx].GetComponent<SortingGroup> ().sortingOrder = 73;
//						Debug.Log (out_gos [idx].name);
					}
				}
			}
		}

		// Apply color effect to hair
		public static void ApplyColorHair(float repeat_uvs, List<Material> material, Color color, List<HitInfo> hit_mesh, 
			ref List<List<VertexData>> out_mesh, ref List<GameObject> out_gos)
		{
			for(int i = 0; i < hit_mesh.Count; i++)
			{
				int idx = hit_mesh[i].m_index;

				if(out_gos[idx] != null)
				{
					MeshRenderer mr = out_gos[idx].GetComponent<MeshRenderer>();
					mr.material.color = color;

				}
			}
		}

		// Apply grow effect to hair
		public static void ApplyGrowHair(float repeat_uvs, List<Material> material, Vector3 position, List<HitInfo> hit_mesh, 
			ref List<List<VertexData>> out_mesh, ref List<GameObject> out_gos)
		{
			AppOn.HairGame.AppOn_HairGame.Instance.GrowSelect_Audio.Play ();
			const float max_length = 300.0f;
			//			float grow_step_size = 10.0f;

			for(int i = 0; i < hit_mesh.Count; i++)
			{
				int idx = hit_mesh[i].m_index;
				float scale = 1.0f + 10.0f / out_mesh[idx][out_mesh[idx].Count-1]._length;
				if(out_mesh[idx].Count > 1 && out_mesh[idx][out_mesh[idx].Count-1]._length < max_length)
				{
					List<VertexData> one_hair = new List<VertexData>();

					one_hair.Add(out_mesh[idx][0]);
					for(int j = 1; j < out_mesh[idx].Count; j++)
					{
						Vector3 vdir = (out_mesh[idx][j]._pos - out_mesh[idx][j-1]._pos);	
						//						Vector3 vdir = out_mesh[idx][j]._original_dir;
						Vector3 new_pt = one_hair[one_hair.Count-1]._pos + vdir * scale;

						one_hair.Add(new VertexData(new_pt, out_mesh[idx][j]._half_width, out_mesh[idx][j]._color));
					}

					ComputeLengthRatio(one_hair);



					out_mesh[idx] = one_hair;

					if(out_gos.Count > idx)
					{


						Color color = out_gos [idx].transform.GetChild (0).GetComponent<SpriteRenderer> ().color;
						color.a = 255;
						out_gos [idx].transform.GetChild (0).GetComponent<SpriteRenderer> ().color = color;
						UpdateGameObjectVertex(out_mesh[idx],  repeat_uvs,  out_gos[idx]);
						out_gos[idx].GetComponent<MeshRenderer>().enabled = true;
						MeshCollider mc  = out_gos[idx].GetComponent<MeshCollider>();
						mc.sharedMesh 	 = null;
						mc.sharedMesh 	 = out_gos[idx].GetComponent<MeshFilter>().mesh;
					}
				}
			}
		}

		// Apply trim effect to hair
		public static void ApplyTrimHair(float repeat_uvs, List<Material> material, Vector3 position, List<HitInfo> hit_mesh, 
			ref List<List<VertexData>> out_mesh, ref List<GameObject> out_gos, out List<GameObject> cut_gos)
		{

			const float total_length = 0.1f;
			cut_gos = new List<GameObject>();

			for(int i = 0; i < hit_mesh.Count; i++)
			{
				int idx = hit_mesh[i].m_index;
				float scale = total_length / out_mesh[idx][out_mesh[idx].Count-1]._length;

				if(out_mesh[idx].Count > 1 && out_mesh[idx][out_mesh[idx].Count-1]._length > 2 )
				{
					List<VertexData> one_hair = new List<VertexData>();
					List<VertexData> cut_path = new List<VertexData>();

					one_hair.Add(out_mesh[idx][0]);
					cut_path.Add(out_mesh[idx][0]);
					for(int j = 1; j < out_mesh[idx].Count; j++)
					{
						cut_path.Add(out_mesh[idx][j]);
						Vector3 vdir = (out_mesh[idx][j]._pos - out_mesh[idx][j-1]._pos).normalized;	
						//						Vector3 vdir = out_mesh[idx][j]._original_dir;
						//						vdir =Vector3.Cross(vdir, out_mesh[idx][j]._original_dir).normalized;
						float orig_length = out_mesh[idx][j]._length - out_mesh[idx][j-1]._length;
						vdir.y = Mathf.Abs (vdir.y);
						vdir.x = 0f;
						//						Debug.Log(vdir);
						Vector3 new_pt = one_hair[one_hair.Count-1]._pos + vdir * scale * orig_length;
						one_hair.Add(new VertexData(new_pt, out_mesh[idx][j]._half_width, out_mesh[idx][j]._color));

					}
					ComputeLengthRatio(one_hair);

					cut_path = out_mesh[idx].GetRange(0, out_mesh[idx].Count);
					GameObject cut_obj = CreateMeshFromPath(cut_path, repeat_uvs, out_gos[idx].GetComponent<MeshRenderer>().material, out_gos[idx], 0);
					Color color = out_gos [idx].transform.GetChild (0).GetComponent<SpriteRenderer> ().color;
					color.a = 0;

					out_gos [idx].transform.GetChild (0).GetComponent<SpriteRenderer> ().color = color;
					MeshRenderer mr = out_gos[idx].GetComponent<MeshRenderer>();
					mr.material.SetTexture("_MainTex",originalTexture);
					if (cut_obj != null) {
						Rigidbody rb = cut_obj.AddComponent<Rigidbody> ();
						rb.mass = 200.0f;
						rb.drag = 0.0f;
						rb.angularDrag = 0.0f;
						rb.centerOfMass = cut_path [0]._pos;
						const float gravityStrength = 500.0f;
						Vector3 upForce = Vector3.up * 100;
						Physics.gravity = new Vector3 (0.0f, -gravityStrength, 0.0f);
						Vector3 sideForce = Vector3.right * Random.Range (-200, 200);
						Vector3 combinedForce = sideForce + upForce;

						rb.useGravity = true;
						rb.velocity = cut_obj.transform.TransformDirection (combinedForce);
						rb.angularVelocity = new Vector3 (0, 0, Random.Range (-2, 2));
						cut_gos.Add (cut_obj);

					} 


					out_mesh[idx] = one_hair;

					if(out_gos.Count > idx)
					{
						UpdateGameObjectVertex(out_mesh[idx],  repeat_uvs,  out_gos[idx]);
						// Hide
						out_gos[idx].GetComponent<MeshRenderer>().enabled = false;
						MeshCollider mc  = out_gos[idx].GetComponent<MeshCollider>();
						if (mc != null) {
							mc.sharedMesh = null;
							mc.sharedMesh = out_gos [idx].GetComponent<MeshFilter> ().mesh;
						}

//						out_gos [idx].GetComponent<SortingGroup> ().sortingOrder = 0;
					}

				}
			}
		}


		private static void UpdateGameObjectColor(Color32 color, Vector3 position, GameObject go, List<VertexData> path)
		{
			Mesh result_mesh = go.GetComponent<MeshFilter> ().mesh;
			Vector3[] verts  = result_mesh.vertices;
			Color32[] colors = result_mesh.colors32;
			const float sqr_radius = 20.0f * 20.0f;
			for(int i = 0; i < colors.Length; i++)
			{
				if((verts[i] - position).sqrMagnitude <= sqr_radius)
				{
					colors[i] = color;
				}
			}

			result_mesh.colors32  = colors;


			if( go.transform.GetChild(0) !=null)
			{
				go.transform.GetChild (0).transform.position = new Vector3 (go.transform.GetChild (0).transform.position.x, go.transform.GetChild (0).transform.position.y, 0);
				if((go.transform.GetChild (0).transform.position - position).sqrMagnitude <= sqr_radius && go.transform.GetChild (0).GetComponent<SpriteRenderer> ().color.a !=0)
				{
					go.transform.GetChild (0).GetComponent<SpriteRenderer> ().color = color;
				}
			}
			for(int i = 0; i < path.Count; i++)
			{
				if((path[i]._pos - position).sqrMagnitude <= sqr_radius)
				{
					path[i]._color = color;
				}
			}


		}

		// Apply color effect to hair
		public static void ApplyColorHair(float repeat_uvs, List<Material> material, Color32 color, Vector3 position, List<HitInfo> hit_mesh, 
			ref List<List<VertexData>> out_mesh, ref List<GameObject> out_gos)
		{


			for(int i = 0; i < hit_mesh.Count; i++)
			{
				int idx = hit_mesh[i].m_index;


				if(out_gos[idx] != null)
				{
					MeshRenderer  rendere= out_gos [idx].GetComponent<MeshRenderer> ();
					if (rendere != null && rendere.enabled) {
						UpdateGameObjectColor (color, position, out_gos [idx], out_mesh [idx]);
					}
				}
			}
		}
		public static void ApplyCurlerOnHair(float repeat_uvs, List<Material> material, Vector3 position, Vector3 prev_position, List<HitInfo> hit_mesh, 
			ref List<List<VertexData>> out_mesh, ref List<GameObject> out_gos,Texture curlyTexture)
		{
			for(int i = 0; i < hit_mesh.Count; i++)
			{
				int idx = hit_mesh[i].m_index;

				if(out_gos[idx] != null)
				{
					MeshRenderer mr = out_gos[idx].GetComponent<MeshRenderer>();
					mr.material.SetTexture("_MainTex",curlyTexture);
				}
			}
		}

		public static void ApplyStraightenerOnHair(float repeat_uvs, List<Material> material, Vector3 position, Vector3 prev_position, List<HitInfo> hit_mesh, 
			ref List<List<VertexData>> out_mesh, ref List<GameObject> out_gos,Texture straightHairTexture)
		{
			for(int i = 0; i < hit_mesh.Count; i++)
			{
				int idx = hit_mesh[i].m_index;

				if(out_gos[idx] != null)
				{
					MeshRenderer mr = out_gos[idx].GetComponent<MeshRenderer>();
					mr.material.SetTexture("_MainTex",straightHairTexture);
				}
			}
		}

	}

}
