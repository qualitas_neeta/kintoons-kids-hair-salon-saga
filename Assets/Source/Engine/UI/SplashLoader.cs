using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using KidozSDK;

public class SplashLoader : MonoBehaviour {

    [SerializeField]
    float waitTimeInSeconds = 1.0f;

    [SerializeField]
    string nextScene;
    public GameObject Ads;
    void Start () {
        StartCoroutine(WaitAndLoadMainMenu());
        #if ADS
        if (KidozManager.Instance != null) {
            KidozManager.Instance.init();
        }
        //     if (AdsManager.IS_AD_SHOWN) 
        //        if(IS_ADS_ENABLE)
//        {
            //          AdsManager.Insatnce.Initilize();
//            DontDestroyOnLoad(Ads);
//            DontDestroyOnLoad(KidozManager.Instance);
//            DontDestroyOnLoad(Kidoz.Instance);
//        }
        #endif 
    }

    private IEnumerator WaitAndLoadMainMenu() {
        
        // Wait for prescribed amount of time
        yield return new WaitForSeconds(waitTimeInSeconds);
        // Navigate to next scene        
		SceneManager.LoadSceneAsync(nextScene, LoadSceneMode.Single);

    }
}
