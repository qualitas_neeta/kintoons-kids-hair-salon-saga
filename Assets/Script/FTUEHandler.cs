﻿using System;
using UnityEngine;
public class FTUEHandler
{
	
	public const  int handleDragPanel=0;
	public const int handleCharacterDropDown=handleDragPanel+1;
	public const int handleChangeCharacter=handleCharacterDropDown+1;
	public const int handleHairGrowBottleSelction=handleChangeCharacter+1;
	public const int handleHairGrow=handleHairGrowBottleSelction+1;
	private static int totalIndex=handleHairGrow+1;

	public static bool isPlaying_AnyTutorial;
	public static int[] FTUE_Index=null;
	private static int HelpGapCounter = 0;
	public static int CURRENT_TUTORIAL_INDEX = -1;
	public static bool isFTUECalled=false;
	public static void Update()
	{
		if (isFTUECalled) {
			HelpGapCounter++;
		}
	}
	public static void load()
	{
		if (!isFTUECalled) {
			if (FTUE_Index == null) {
				FTUE_Index = new int[totalIndex];
			}
			for (int i = 0; i < FTUE_Index.Length; i++) {
				FTUE_Index [i] = PlayerPrefs.GetInt ("FTUE" + i, -1);
			}
		}
	}
	public static bool isAllFTUEComplete()
	{
		if(FTUE_Index!=null && FTUE_Index.Length>0)
		{
			for (int i = 0; i < FTUE_Index.Length; i++) {
				if(FTUE_Index [i]==-1 && i!=handleHairGrowBottleSelction  &&i!=handleHairGrow)
				{
					return false;
				}
			}
			return true;
		}
		return false;
	}
	public static void setTutorial(int index)
	{
//		Debug.Log ("Start Tutorial : " + index);
		FTUEHandler.isPlaying_AnyTutorial = true;
		CURRENT_TUTORIAL_INDEX =index;
		GameManager.instance.Block_Unblock_UI (false);
	}
	public static void CompleteTutorial(int index){
//		Debug.Log ("CompleteTutorial Tutorial : " + index);
		FTUEHandler.isPlaying_AnyTutorial = false;
		CURRENT_TUTORIAL_INDEX = -1;
		HelpGapCounter = 0;

		FTUE_Index [index] = 0;
		GameManager.instance.Block_Unblock_UI (true);
		if(index==handleCharacterDropDown && FTUE_Index [handleChangeCharacter]==-1)
		{
			return;
		}
		if(index==handleChangeCharacter)
		{
			PlayerPrefs.SetInt ("FTUE" + (index-1), FTUE_Index [ (index-1)]);
		}
		PlayerPrefs.SetInt ("FTUE" + index, FTUE_Index [index]);
		PlayerPrefs.Save ();

	}

	public static bool CanPlayHelp(int index)
	{
		if(isPlaying_AnyTutorial ||!isFTUECalled){
			return false;
		}
		switch(index){
		case handleDragPanel:
			return (FTUE_Index [index] == -1 && HelpGapCounter > 15);
		case handleCharacterDropDown:
			return (FTUE_Index [index] == -1 && FTUE_Index [index-1]==0 && HelpGapCounter > 15);
		case handleChangeCharacter:
			return (FTUE_Index [index] == -1 && FTUE_Index [index-1]==0);
		case handleHairGrowBottleSelction:
			return (FTUE_Index [index] == -1 );
		case handleHairGrow:
			return (FTUE_Index [index] == -1 && FTUE_Index [index-1]==0);
		}

		return false;
	}

}

