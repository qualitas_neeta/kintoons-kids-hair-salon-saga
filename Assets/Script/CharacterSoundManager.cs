﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AppOn.HairGame;
public class CharacterSoundManager : MonoBehaviour {

	SoundManager sound;

	void Start(){
		sound=transform.parent.GetComponent<SoundManager>();
	}
	public void Update()
	{
		if(IsEntryAnimPlaying && !sound.Audio.isPlaying)
		{
			IsEntryAnimPlaying=false;
		}
	}
	public void playSound (int index)
	{
		if ((SoundManager.instance.avatar_no == AppOn_HairGame.CHARACTER_UGA )//&& SoundManager.instance.boughtUga
			|| (SoundManager.instance.avatar_no == AppOn_HairGame.CHARACTER_PAULO ) //&& SoundManager.instance.boughtPaulo
			|| (SoundManager.instance.avatar_no == AppOn_HairGame.CHARACTER_MERLE ) 
			|| (SoundManager.instance.avatar_no == AppOn_HairGame.CHARACTER_JANET)) {//&& SoundManager.instance.boughtJanet
			if(!IsEntryAnimPlaying)
			{
				sound.Audio.clip = sound.AudioClip [index];
				sound.Audio.Play ();
				if (index == 5) {
					IsEntryAnimPlaying = true;
				}
			}
		}
	}
	public static bool IsEntryAnimPlaying=false;
	public void AfterEntryAnim(){
		switch(GameManager.Avatar_No)
		{
		case AppOn_HairGame.CHARACTER_JANET:
			GameManager.instance.Janet_Anim.SetInteger ("State", 11);
			break;
		case AppOn_HairGame.CHARACTER_MERLE:
			GameManager.instance.Merle_Anim.SetInteger ("State", 11);
			break;
		case AppOn_HairGame.CHARACTER_PAULO:
			GameManager.instance.Paulo_Anim.SetInteger ("State",11);
			break;
		case AppOn_HairGame.CHARACTER_UGA:
			GameManager.instance.UGA_Anim.SetInteger ("State",11);
			break;
		}
	}

}
