﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GoogleAnalytics : MonoBehaviour {

	static GoogleAnalytics instance;
	#if firebase
	public GoogleAnalyticsV4  AnalyticsGoogle;
	#endif
	// Use this for initialization
	void Awake()
	{
		instance = this;
	}
	public static GoogleAnalytics Instance{
		get{
			return instance;
		}
	}
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void LogCustomEvent(string action,string tilte)
	{
		FlurryAnalytics.Instance.LogEvent (tilte, action);
//		Debug.LogError (tilte + " : " + action);
		#if !UNITY_EDITOR && UNITY_ANDROID
		#if firebase
			EventHitBuilder hitBuilder = new EventHitBuilder ();
			hitBuilder.SetEventCategory (tilte);
			hitBuilder.SetEventAction (action);
			AnalyticsGoogle.LogEvent (hitBuilder);
		#endif
		#endif
	}
}
