﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using AppOn.HairGame;
public class DragDrop : MonoBehaviour,IDragHandler,IPointerDownHandler,IPointerUpHandler{

	static GameObject itemBeingDraged;
	Vector3 startPosition;

	public static Transform realTargetParent;
	public Transform startParent;
	public Transform targetParent;
	public Transform targetParent1;
	public Transform targetParent2;
	public Transform targetParent3;
	bool draging = false;
	public bool dropPoint;
	bool throwAway = true;
	float a;
	Vector2 mouseStartPosition;
	Vector2 mouseEndPosition;
	float dragStartTime;
	float dragEndTime;
	float dragTotalTime;
	public GameObject obj;
	Vector2 objPositon;
//	public Sprite ObjImage;
//	public Sprite ObjImage1;
//	public Sprite ObjImage2;
//	public Sprite ObjImage3;
//	public Sprite ObjImage4;
//	public Sprite ObjImage5;
//	public Sprite ObjImage6;
//	public Sprite ObjImage7;
//	public Sprite ObjImage8;
//	public Sprite ObjImage9;
	private int currentSpriteIndex=0;
	public Sprite[] ObjImageSprite=new Sprite[10];

	private static int currentIndex = 0;
	public GameObject Box;
	public bool selectionCheck;
//	string HairStyle;
	public static  DragDrop Instance;
	int initial_avatar_no;
	int current_avatar_no;
	BoxCollider collider;
	public GameObject draggedItem;
	public AudioSource AccessoriesPickUp_Audio;
	public AudioSource AccessoriesDrop_Audio;
	bool firstTime;
	float prevTime;
	void Start(){
		Instance = this;
		draggedItem = itemBeingDraged;
		collider = GetComponent<BoxCollider> ();
		if (GameManager.instance != null) {
			current_avatar_no = GameManager.Avatar_No;
		}
		prevTime = Time.time;
	}

	public void Update(){
		if (Settings.instance != null) {
			AccessoriesDrop_Audio.volume = (Settings.instance.sfx_slider.value * 0.2f);
			AccessoriesPickUp_Audio.volume = (Settings.instance.sfx_slider.value * 0.2f);
		}
		selectionCheckUp ();
		initial_avatar_no = GameManager.Avatar_No;

		if (current_avatar_no != initial_avatar_no) {
			NotInPosition ();
			current_avatar_no = initial_avatar_no;
		}

		if (!dropPoint && !draging && startParent != gameObject.transform.parent) {
			float x = gameObject.transform.position.x;
			float y = gameObject.transform.position.y;
			collider.enabled = false;
			if (y < (a + 300) && throwAway) {
				y += 25;
				gameObject.transform.position = new Vector2 (x, y);
				if (y >= (a + 250)) {
					throwAway = false;
				}
			}	

			if (!throwAway) {
				y -= Time.deltaTime * 4000f;
				gameObject.transform.position = new Vector2 (x, y);
			}
			if (y < -1500)
				Destroy (gameObject);
		}
	}

	#region IPointerDownHandler implementation

	public void OnPointerDown (PointerEventData eventData)
	{
		if(((Time.time - prevTime)<1)||GameManager.instance.CameraPanelUpperlayer.activeSelf)
		{
			return;
		}
		prevTime = Time.time;
		if(Input.touchCount>1)
		{
			return;
		}
		draging = true;

//		HairStyle = AppOn.HairGame.AppOn_HairGame.Instance._cur_hair_style;
		AppOn.HairGame.AppOn_HairGame.Instance._cur_hair_style = "None";
//				AppOn.HairGame.AppOn_HairGame.Instance.Deselection ();

		itemBeingDraged = gameObject;
		startPosition = transform.position;
		mouseStartPosition = startPosition;
		objPositon = startPosition;
		dragStartTime = Time.time;
		DropTrigger.Instance.script = this;
		if (startParent == gameObject.transform.parent) {
			CreatingObj ();
			StartCoroutine ("PopOutAnim");
		}
		if(current_avatar_no == 0) {
			gameObject.transform.SetParent (targetParent);
			realTargetParent = targetParent;
		}
		if (current_avatar_no == 1) {
			gameObject.transform.SetParent (targetParent1);
			realTargetParent = targetParent1;
		}
		if (current_avatar_no == 2) {
			gameObject.transform.SetParent (targetParent2);
			realTargetParent = targetParent2;
		}
		if (current_avatar_no == 3) {
			gameObject.transform.SetParent (targetParent3);
			realTargetParent = targetParent3;
		}
		
		gameObject.transform.SetAsLastSibling ();
		gameObject.transform.GetComponent<UnityEngine.UI.Image> ().SetNativeSize ();
		//	GetComponent<CanvasGroup> ().blocksRaycasts = false;

	}

	#endregion

	#region IDragHandler implementation

	public void OnDrag (PointerEventData eventData)
	{

		if(Input.touchCount>1 || !draging||GameManager.instance.CameraPanelUpperlayer.activeSelf)
		{
			return;
		}
		Vector3 v3 = Vector3.zero;
		v3 = Camera.main.ScreenToWorldPoint (Input.mousePosition);
//		v3 = new Vector3 (Input.mousePosition.x, Input.mousePosition.y + (Screen.height / 8), 0f); 
		v3.z = 0;
		transform.position = v3;
//		Debug.Log ("check");
	}

	#endregion

	#region IPointerUpHandler implementation

	public void OnPointerUp (PointerEventData eventData)
	{
//		Debug.Log (firstTime);
		if(GameManager.instance.CameraPanelUpperlayer.activeSelf)
		{
			return;
		}
		if (firstTime) {
			AccessoriesDrop_Audio.Play ();
			firstTime = false;
		}
		itemBeingDraged = null;
//		GetComponent<CanvasGroup> ().blocksRaycasts = true;
		draging = false;
		mouseEndPosition = transform.position;
		dragEndTime = Time.time;

		dragTotalTime = dragEndTime - dragStartTime;
		float distance = (float)Vector2.Distance(mouseStartPosition,mouseEndPosition) ;

		if (distance <= 500 && dragTotalTime <= 0.1) {
			dropPoint = false;
		}
		a = gameObject.transform.position.y;
		StartCoroutine ("PopUpAnim");
//		AppOn.HairGame.AppOn_HairGame.Instance._cur_hair_style = HairStyle;
	}

	#endregion

	public void IsInPosition(){
		dropPoint = true;
	}

	public void NotInPosition(){
		dropPoint = false;
	}

	private IEnumerator PopUpAnim(){
		float pop_Height;
		if (obj != null && obj.name == "Glares") {
			pop_Height = (Box.transform.position.y) + 20;
		} else {
			pop_Height = (Box.transform.position.y) + 10;
		}


		while(objPositon.y <= pop_Height && !draging){
//			Debug.Log (objPositon.y);
			objPositon.y += 2;
			objPositon.x = Box.transform.position.x;

//				objPositon.y = Mathf.Clamp (objPositon.y, Box.transform.position.y, pop_Height);
			if(obj!=null )
				obj.transform.position = objPositon;
			yield return new WaitForSeconds (0.001f);
		}
	}
	private IEnumerator PopOutAnim(){
		
		Vector2 popPosition = startPosition;
		Vector2 currentPos = gameObject.transform.position;

		while (Vector2.Distance(popPosition,currentPos) < 20) {
			popPosition.y += 2;
			gameObject.transform.position = popPosition;
			yield return new WaitForSeconds (0.001f);
		} 
		AccessoriesPickUp_Audio.Play ();
		firstTime = true;
		if (itemBeingDraged != null) {
			if (itemBeingDraged.name == "Glares"){
				transform.Rotate (0, 0, 0);
				if (current_avatar_no == 2  ) {
					itemBeingDraged.transform.localScale += (itemBeingDraged.transform.localScale * 1.75f);
				} else if (current_avatar_no == 3) {
					itemBeingDraged.transform.localScale += (itemBeingDraged.transform.localScale * 0.1f);
				} else if (current_avatar_no == 0) {
					itemBeingDraged.transform.localScale += (itemBeingDraged.transform.localScale) / (0.7f);
				}
				else{
					itemBeingDraged.transform.localScale +=  (itemBeingDraged.transform.localScale * 1.02f);
				}
				if((current_avatar_no == 3&& (currentIndex==0||currentIndex==4||currentIndex==5||currentIndex==8||currentIndex==6||currentIndex==9))||
					(current_avatar_no == 1&& (currentIndex==1||currentIndex==2||currentIndex==4||currentIndex==5||currentIndex==6||currentIndex==8))||
					(current_avatar_no == 0&& (currentIndex==1||currentIndex==5||currentIndex==9||currentIndex==8)))
				{
					itemBeingDraged.transform.localScale += (itemBeingDraged.transform.localScale * 0.2f);
				}
			}
			else if (itemBeingDraged.name == "HairClip"){
				transform.Rotate (0, 0, -90);
				itemBeingDraged.transform.localScale += (itemBeingDraged.transform.localScale * 0.2f);
			}
			else if (itemBeingDraged.name == "Cap") {
				transform.Rotate (0, 0, -45);

				if (current_avatar_no == 2  ) {
					itemBeingDraged.transform.localScale = itemBeingDraged.transform.localScale*(1.50f);
					if(currentIndex==2)
					{
						itemBeingDraged.transform.localScale += (itemBeingDraged.transform.localScale * 0.2f);
					}
					else if(currentIndex==9)
					{
						itemBeingDraged.transform.localScale -= (itemBeingDraged.transform.localScale * 0.2f);
					}
				} else if (current_avatar_no == 3) {
//					itemBeingDraged.transform.localScale = itemBeingDraged.transform.localScale*(1.50f);
					itemBeingDraged.transform.localScale = (itemBeingDraged.transform.localScale * 1.20f);
					if(currentIndex==9)
					{
						itemBeingDraged.transform.localScale -= (itemBeingDraged.transform.localScale * 0.2f);
					}
					else if(currentIndex==2)
					{
						itemBeingDraged.transform.localScale += (itemBeingDraged.transform.localScale * 0.2f);
					}
				} else if (current_avatar_no == 0) {
					itemBeingDraged.transform.localScale = itemBeingDraged.transform.localScale*(1.70f);
					if(currentIndex==3||currentIndex==9)
					{
						itemBeingDraged.transform.localScale -= (itemBeingDraged.transform.localScale * 0.2f);

					}
				}else{
					itemBeingDraged.transform.localScale = itemBeingDraged.transform.localScale*(1.50f);
				}
			}
		}

	}

	public void CreatingObj(){
		objPositon.y -= 20;
		obj = Instantiate (gameObject, objPositon, gameObject.transform.rotation, startParent);
		if(itemBeingDraged.name == "Glares")
		{
			if ((AppOn_HairGame.currentGlaresIndex==3 && !GameManager.SprayBuy)||AppOn_HairGame.currentGlaresIndex==9) {
				AppOn_HairGame.currentGlaresIndex = 0;
			}
			else
			{
				AppOn_HairGame.currentGlaresIndex++;
			}
			currentIndex = AppOn_HairGame.currentGlaresIndex;
			obj.transform.GetComponent<UnityEngine.UI.Image> ().sprite = ObjImageSprite[AppOn_HairGame.currentGlaresIndex];
		}
		else if(itemBeingDraged.name == "HairClip")
		{
			if ((AppOn_HairGame.currentClipIndex==3 && !GameManager.SprayBuy)||AppOn_HairGame.currentClipIndex==9) {
				AppOn_HairGame.currentClipIndex = 0;
			}
			else
			{
				AppOn_HairGame.currentClipIndex++;
			}
			currentIndex = AppOn_HairGame.currentClipIndex;
			obj.transform.GetComponent<UnityEngine.UI.Image> ().sprite = ObjImageSprite[AppOn_HairGame.currentClipIndex];
		}
		else{
			if ((AppOn_HairGame.currentCapIndex==3 && !GameManager.SprayBuy)||AppOn_HairGame.currentCapIndex==9) {
				AppOn_HairGame.currentCapIndex = 0;
			}
			else
			{
				AppOn_HairGame.currentCapIndex++;
			}
			currentIndex = AppOn_HairGame.currentCapIndex;
			obj.transform.GetComponent<UnityEngine.UI.Image> ().sprite = ObjImageSprite[AppOn_HairGame.currentCapIndex];
		}
		if (itemBeingDraged.name == "Glares") {
			obj.name = "Glares";

		} else if (itemBeingDraged.name == "HairClip") {
			obj.name = "HairClip";
		} else {
			obj.name = "Cap";
		}
		obj.transform.SetSiblingIndex(1);
	}

	public void selectionCheckUp(){
		if (gameObject.transform.parent == startParent)
			selectionCheck = true;
		else
			selectionCheck = false;
	}
}