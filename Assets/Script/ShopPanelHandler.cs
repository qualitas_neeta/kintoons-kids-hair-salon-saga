﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ShopPanelHandler : MonoBehaviour {

	public Text text;
	// Use this for initialization
	void Start () {
		
	}
	void OnEnable()
	{
		#if UNITY_ANDROID||UNITY_IOS 
		#if IAP
		text.text = ""+IAP_Manager.instance.getProductPrice (IAP_Manager.Product_CompletePackage);
		#endif
		#endif
	}
	// Update is called once per frame
	void Update () {
		
	}
}
