﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BG_Change : MonoBehaviour {


	public static BG_Change instance;
	public AudioSource BGChange_Audio;
	public GameObject BuyBG_Panel;
	private int currentSelectedBG = 0;

	public GameObject bg_france;
	public GameObject bg_germany;
	public GameObject bg_japan;
	public GameObject bg_usa;
	public GameObject bg_uk;

	public const int BG_FRANCE=0;
	public const int BG_UK=1;
	public const int BG_GERMANY=2;
	public const int BG_JAPAN=3;
	public const int BG_USA=4;
	void Start(){
		instance = this;
//		Settings.instance.music_slider.value = PlayerPrefs.GetFloat ("music_slider", 5f);
//		Settings.instance.sfx_slider.value = PlayerPrefs.GetFloat ("sfx_slider", 5f);
	}

	void Update(){
		if(BGChange_Audio!=null && !BGChange_Audio.isPlaying &&Settings.instance!=null  &&Settings.instance.sfx_slider.value!=0)
		{
			
			BGChange_Audio.Play ();
		}
		if (Settings.instance != null) {
			BGChange_Audio.volume = (Settings.instance.music_slider.value * 0.2f);
		}
	}

	public void BG_Button(int _currentSelectedBG){
		if(DropDown.instance.BG_Drop){
			DropDown.instance.BG_DrawerDrop();
		}
		currentSelectedBG = _currentSelectedBG;
		if ((currentSelectedBG == BG_UK && GameManager.instance.BG1Change) ||
		   (currentSelectedBG == BG_GERMANY && GameManager.instance.BG2Change) ||
		   (currentSelectedBG == BG_JAPAN && GameManager.GameData.BG3_Purchase) ||
			(currentSelectedBG == BG_USA && GameManager.GameData.BG4_Purchase)|
			currentSelectedBG == BG_FRANCE) {
			bg_france.SetActive (false);
			bg_germany.SetActive (false);
			bg_japan.SetActive (false);
			bg_usa.SetActive (false);
			bg_uk.SetActive (false);
		}
		switch(currentSelectedBG){
		case BG_FRANCE:
			bg_france.SetActive (true);
			BGChange_Audio.Play ();
			break;
		case BG_UK:
			if (GameManager.instance.BG1Change) {
				bg_uk.SetActive (true);
				BGChange_Audio.Play ();
			}
			else{
//				GameManager.instance.ButtonClick_Audio.Play ();
				BuyBG(BG_UK);
			}
			break;
		case BG_GERMANY:
			if (GameManager.instance.BG2Change) {
				bg_germany.SetActive (true);
				BGChange_Audio.Play ();
			}
			else{
//				GameManager.instance.ButtonClick_Audio.Play ();
				BuyBG(BG_GERMANY);
			}

			break;
		case BG_JAPAN:
			if (GameManager.GameData.BG3_Purchase) {
				bg_japan.SetActive (true);
				//				BG_Anim.SetInteger ("CurrentBG", 2);
				BGChange_Audio.Play ();
			}
			else{
//				GameManager.instance.ButtonClick_Audio.Play ();
				BuyBG(BG_JAPAN);
			}

			break;
		case BG_USA:
			if (GameManager.GameData.BG4_Purchase) {
				bg_usa.SetActive (true);
				BGChange_Audio.Play ();
			}
			else{
//				GameManager.instance.ButtonClick_Audio.Play ();
				BuyBG(BG_USA);
			}

			break;
		}

	}
	public static int current_bg_id_to_purchase=-1;
//	public bool BG1Bought;
//	public bool BG2Bought;
	public void BuyBG(int bgID){
		current_bg_id_to_purchase = bgID;
		DropDown.instance.BothUp ();
		GameManager.instance.Shop_Button(true);
	}
}
