﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckAnimationEnd : StateMachineBehaviour {

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		if (stateInfo.IsName ("splashscreen")) {
			GameManager.splashAnimStart = true;
		}else {
			if (stateInfo.IsTag ("CameraPos")) {
				animator.SetInteger ("State", 0);
			} else {
				AppOn.HairGame.AppOn_HairGame.Instance.Happy_Anims (); 
			}
		}
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		if (stateInfo.IsName ("splashscreen")) {
			GameManager.splashAnimStart = false;
		}else if (stateInfo.IsName ("FingerTutor")) {
			FTUEHandler.CompleteTutorial (FTUEHandler.handleDragPanel);
		} else {
			if (stateInfo.IsTag ("EntryAnim")) {
				if(!FTUEHandler.isFTUECalled){
					FTUEHandler.isFTUECalled = true;
				}
				for (int i = 0; i < GameManager.instance.SelectAvtarPanelBtn.Length; i++) {
					GameManager.instance.SelectAvtarPanelBtn [i].interactable = true;
				}
			}
		}
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
