﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour {

	public static Settings instance;

	public Slider sfx_slider;
	public Slider music_slider;
	public AudioSource BG_Sound;
	public bool SFX_Sound;
	public GameObject Music_On;
	public GameObject Music_Off;
	public GameObject SFX_On;
	public GameObject SFX_Off;

	// Use this for initialization
	void Awake()
	{
		instance = this;
		music_slider.value = PlayerPrefs.GetFloat ("music_slider", 5f);
		sfx_slider.value = PlayerPrefs.GetFloat ("sfx_slider", 5f);
	}
	void Start () {

	}
	void OnDestroy()
	{

	}
	
	// Update is called once per frame
	void Update () {
		MusicSlider ();

		if (music_slider.value == 0) {
			Music_On.SetActive (false);
			Music_Off.SetActive (true);
		} else {
			Music_On.SetActive (true);
			Music_Off.SetActive (false);
		}

		if (sfx_slider.value == 0) {
			SFX_On.SetActive (false);
			SFX_Off.SetActive (true);
		} else {
			SFX_On.SetActive (true);
			SFX_Off.SetActive (false);
		}
			
	}
	public void SaveData()
	{
		PlayerPrefs.SetFloat ("music_slider", music_slider.value);
		PlayerPrefs.SetFloat ("sfx_slider", sfx_slider.value);
	}
	public void MusicSlider (){
		if(music_slider.value!=0 &&!BG_Sound.isPlaying)
		{
			BG_Sound.Play ();
		}
		BG_Sound.volume = (music_slider.value * 0.2f);
	}

	public void MusicOn_Button(){
		PlayerPrefs.SetFloat("musicSound",music_slider.value);
		music_slider.value = 0;
	}

	public void MusicOff_Button(){
//		music_slider.value=5;
		music_slider.value=PlayerPrefs.GetFloat("musicSound",music_slider.value);
		GameManager.instance.ButtonClick_Audio.Play ();
//		PlayerPrefs.SetFloat("musicSound",music_slider.value);
	}

	public void SFXOn_Button(){
		PlayerPrefs.SetFloat("SFXSound",sfx_slider.value);
		sfx_slider.value = 0;
	}

	public void SFXOff_Button(){
//		sfx_slider.value=5;
		sfx_slider.value=	PlayerPrefs.GetFloat("SFXSound",sfx_slider.value);
		GameManager.instance.ButtonClick_Audio.Play ();
//		PlayerPrefs.SetFloat("SFXSound",sfx_slider.value);
	}
}
