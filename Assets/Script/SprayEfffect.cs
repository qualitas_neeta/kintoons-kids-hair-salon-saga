﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AppOn.HairGame;

public class SprayEfffect : MonoBehaviour {
	public static SprayEfffect instance;

	public ParticleSystem Spray;
	public AudioSource SprayApplyAudio;
	string ColorSelect;

	// Use this for initialization
	void Start () {
		instance = this;
		Spray.Stop ();
	}
	
	// Update is called once per frame
	void Update () {
//		Debug.Log ("check......");

		if(SprayApplyAudio != null&& Settings.instance !=null){
			SprayApplyAudio.volume = (Settings.instance.sfx_slider.value * 0.02f);
		}
		if (Input.GetMouseButtonUp (0) ) {
			SprayApplyAudio.Stop ();
		}
		if( AppOn.HairGame.AppOn_HairGame.Instance == null||(GameManager.instance != null && GameManager.instance.CamPanel.activeSelf)){
			Spray.Stop ();
			SprayApplyAudio.Stop ();
			return;
		}
		ColorSelect = AppOn.HairGame.AppOn_HairGame.Instance._cur_hair_style;
		if (Input.GetMouseButton (0) && ColorSelect == "Color") {
			if (SoundManager.instance.isClickInside()) {
				if (!SprayApplyAudio.isPlaying) {
					SprayApplyAudio.Play ();
					SprayApplyAudio.loop = true;
				}
			}
			ParticleSystem.MainModule modeule = Spray.main;
			modeule.startColor=new ParticleSystem.MinMaxGradient(AppOn_HairGame.Instance.ColorPicker());

			if(!Spray.isPlaying)
				Spray.Play ();
			transform.position = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		}

		if (!Input.GetMouseButton (0)){
			if(Spray.isPlaying)
				Spray.Stop ();
		}
	}
}
