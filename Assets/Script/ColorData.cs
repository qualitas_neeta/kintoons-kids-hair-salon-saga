﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorData : MonoBehaviour {

	public Color32 color;
	public int colorID;
	public Animator bottle;
}
