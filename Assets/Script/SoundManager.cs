﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AppOn.HairGame;
public class SoundManager : MonoBehaviour {

	public static SoundManager instance;


	public AudioSource Audio;
	public AudioClip[] AudioClip;
	public int avatar_no;
	public bool boughtUga;
	public bool boughtPaulo;
	public bool boughtJanet;

	Rect SoundActivePos;
	public RectTransform soundRect;

	public void Start(){


		instance = this;

		SoundActivePos = new Rect (0, (Screen.height / 4), Screen.width, (Screen.height - Screen.height / 3));
	}
	public bool isClickInside()
	{
		if (SoundActivePos.Contains (Input.mousePosition)) {
			return true;
		} 
		return false;
	}
	public void Update (){

		if (Settings.instance != null) {
			Audio.volume = (Settings.instance.sfx_slider.value * 0.2f);
		}
		avatar_no = GameManager.Avatar_No;
		boughtUga = GameManager.BoughtUga;
		boughtPaulo = GameManager.BoughtPaulo;
		boughtJanet = GameManager.BoughtJanet;
		if (Input.GetMouseButtonUp (0)) {
			AppOn.HairGame.AppOn_HairGame.Instance.TrimSelect_Audio.Stop();
			AppOn.HairGame.AppOn_HairGame.Instance.GrowSelect_Audio.Stop();
			AppOn.HairGame.AppOn_HairGame.Instance.CutSelect_Audio.Stop();
			AppOn.HairGame.AppOn_HairGame.Instance.straightener_curler_Audio.Stop();
			AppOn.HairGame.AppOn_HairGame.Instance.drier_Audio.Stop();
//			AppOn.HairGame.AppOn_HairGame.Instance.CutSelect_Audio.Stop();
		}

		if(GameManager.instance != null && (GameManager.instance.CamPanel.activeSelf))
		{
			return;
		}

		if(AppOn.HairGame.AppOn_HairGame.Instance._cur_hair_style == "Trim")
			trimeAudio ();
		if (AppOn.HairGame.AppOn_HairGame.Instance._cur_hair_style == "Cut")
			cutAudio ();
		if (AppOn.HairGame.AppOn_HairGame.Instance._cur_hair_style == "Grow")
			growAudio ();
		if (AppOn.HairGame.AppOn_HairGame.Instance._cur_hair_style == "Straightener"||
			AppOn.HairGame.AppOn_HairGame.Instance._cur_hair_style == "Curler")

//		if (AppOn.HairGame.AppOn_HairGame.Instance._cur_hair_style == "Hair_Dryer")
//			drierAudio ();
		
		if (GameManager.instance.MainMenu_Bool || GameManager.instance.ForParentPanel_Bool || GameManager.instance.ShopPanel_Bool) {
			TurnOffSound ();
		}

	}



	public void trimeAudio(){
		if (Input.GetMouseButtonDown (0) && SoundManager.instance.isClickInside() ) {
			AppOn.HairGame.AppOn_HairGame.Instance.TrimSelect_Audio.Play ();
			if (Settings.instance != null) {
				AppOn.HairGame.AppOn_HairGame.Instance.TrimSelect_Audio.volume = (Settings.instance.sfx_slider.value * 0.2f);
			}
			AppOn.HairGame.AppOn_HairGame.Instance.TrimSelect_Audio.loop = true;
		}
	}

	public void cutAudio(){
		if (Input.GetMouseButtonDown (0)  && SoundManager.instance.isClickInside()) {

			AppOn.HairGame.AppOn_HairGame.Instance.CutSelect_Audio.Play ();
			if (Settings.instance != null) {
				AppOn.HairGame.AppOn_HairGame.Instance.CutSelect_Audio.volume = (Settings.instance.sfx_slider.value * 0.2f);
			}
			AppOn.HairGame.AppOn_HairGame.Instance.CutSelect_Audio.loop = true;
		}
	}

	public void growAudio(){
		if (Input.GetMouseButtonDown (0)  && SoundManager.instance.isClickInside() && !AppOn.HairGame.AppOn_HairGame.Instance.GrowSelect_Audio.isPlaying) {
			if (Settings.instance != null) {
				AppOn.HairGame.AppOn_HairGame.Instance.GrowSelect_Audio.volume = (Settings.instance.sfx_slider.value * 0.2f);
			}
			AppOn.HairGame.AppOn_HairGame.Instance.GrowSelect_Audio.Play ();
			AppOn.HairGame.AppOn_HairGame.Instance.GrowSelect_Audio.loop = true;
		}
	}
	public void drierAudio(){
		if ( !AppOn.HairGame.AppOn_HairGame.Instance.drier_Audio.isPlaying) {
			if (Settings.instance != null) {
				AppOn.HairGame.AppOn_HairGame.Instance.drier_Audio.volume = (Settings.instance.sfx_slider.value * 0.2f);
			}
			AppOn.HairGame.AppOn_HairGame.Instance.drier_Audio.Play ();
			AppOn.HairGame.AppOn_HairGame.Instance.drier_Audio.loop = true;
		}
	}
	public void straightener_curlerAudio(){
		if ( !AppOn.HairGame.AppOn_HairGame.Instance.straightener_curler_Audio.isPlaying) {
			if (Settings.instance != null) {
				AppOn.HairGame.AppOn_HairGame.Instance.straightener_curler_Audio.volume = (Settings.instance.sfx_slider.value * 0.2f);
			}
			AppOn.HairGame.AppOn_HairGame.Instance.straightener_curler_Audio.Play ();

//			AppOn.HairGame.AppOn_HairGame.Instance.straightener_curler_Audio.
			AppOn.HairGame.AppOn_HairGame.Instance.straightener_curler_Audio.loop = true;
		}
	}
	public void showerAudio(){
		if ( !AppOn.HairGame.AppOn_HairGame.Instance.shower_Audio.isPlaying) {
			if (Settings.instance != null) {
				AppOn.HairGame.AppOn_HairGame.Instance.shower_Audio.volume = (Settings.instance.sfx_slider.value * 0.2f);
			}
			AppOn.HairGame.AppOn_HairGame.Instance.shower_Audio.Play ();
			AppOn.HairGame.AppOn_HairGame.Instance.shower_Audio.loop = true;
		}
	}

	public void TurnOffSound(){
		AppOn.HairGame.AppOn_HairGame.Instance.TrimSelect_Audio.Stop ();
		AppOn.HairGame.AppOn_HairGame.Instance.GrowSelect_Audio.Stop ();
		AppOn.HairGame.AppOn_HairGame.Instance.CutSelect_Audio.Stop ();
		AppOn.HairGame.AppOn_HairGame.Instance.straightener_curler_Audio.Stop ();
		AppOn.HairGame.AppOn_HairGame.Instance.drier_Audio.Stop ();
		AppOn.HairGame.AppOn_HairGame.Instance.shower_Audio.Stop ();
		Audio.Stop ();


	}





		
}
