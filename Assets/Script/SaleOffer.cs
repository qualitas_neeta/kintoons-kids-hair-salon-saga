﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
public class SaleOffer : MonoBehaviour {
	public static bool IS_SHOW_SALE_OFFER = false;
	// Use this for initialization
	private static DateTime startDate;
	private int remainHours;
	public Text timeText;
	private static DateTime endDate;
	public Text priceText;
	public Text originalPriceText;
	static string startDateString="" ;
	static string endDateString="" ;
	public static bool LaunchGame;
	public static void load()
	{
//		PlayerPrefs.DeleteAll ();
		if (!LaunchGame) {
			startDateString = PlayerPrefs.GetString ("LaunchDate", "");
			if (startDateString == "") {
				startDate = DateTime.UtcNow;
				startDateString = startDate.ToString ();
				PlayerPrefs.SetString ("LaunchDate", startDateString);
			} else {
				DateTime.TryParse (startDateString, out startDate);
			}
			endDateString = PlayerPrefs.GetString ("endDate", "");
			if (endDateString == "") {
				endDate = startDate.AddHours (48).ToUniversalTime ();
				endDateString = endDate.ToString ();
				PlayerPrefs.SetString ("endDate", endDateString);
			} else {
				DateTime.TryParse (endDateString, out endDate);
			}
			LaunchGame = true;
		}
		if ((startDate < DateTime.Now.ToUniversalTime ()) && endDate > DateTime.Now.ToUniversalTime () && !GameManager.SprayBuy) {
			IS_SHOW_SALE_OFFER = true;
		}
		
//		Debug.Log ("startTime :: " + startDate + " endDate : :" + endDate+"");
	}


	void Start () {
		#if UNITY_ANDROID||UNITY_IOS
		#if IAP
		priceText.text = IAP_Manager.instance.getProductPrice (IAP_Manager.Buy_Complete_Package_In_Offer);
		originalPriceText.text=IAP_Manager.instance.getProductPrice (IAP_Manager.Product_CompletePackage);
		#endif
		#endif
	}
	
	// Update is called once per frame
	void Update () {
		if (LaunchGame) {
			checkTimer ();
		}
	}
	public void checkTimer()
	{
		string hours="00";
		string min="00";
		string second="00";
		int remainingHours;
		if (IS_SHOW_SALE_OFFER) {
			TimeSpan remainingTime =endDate.Subtract(DateTime.Now.ToUniversalTime());
			remainingHours = remainingTime.Hours + (remainingTime.Days * 24);
			if (remainingHours> 9) {
				hours = "" + remainingHours;
			} else {
				hours = "0" + remainingHours;
			}
			if (remainingTime.Minutes > 9) {
				min = "" + remainingTime.Minutes;
			} else {
				min = "0" + remainingTime.Minutes;
			}
			if (remainingTime.Seconds > 9) {
				second = "" + remainingTime.Seconds;
			} else {
				second = "0" + remainingTime.Seconds;
			}
			timeText.text = hours + ":" + min + ":" + second;
			if( remainingTime.TotalSeconds<0)
			{
				IS_SHOW_SALE_OFFER = false;
				GameManager.instance.HandleIAPButton ();
			}
		}
	}
	public void CanclePurchase()
	{
		if(GameManager.isFromFTUE){
			GameManager.instance.MainMenuButton (true);
			GameManager.isFromFTUE = false;
		}
		GameManager.instance.FullPackage_Offer = false;
		GameManager.instance.ButtonClick_Audio.Play ();
		gameObject.SetActive (false);
	}
	public void BuyPackage()
	{
		GameManager.instance.questionaireForLink.SelectQuestion ();
		GameManager.instance.FullPackage_Offer = true;
		gameObject.SetActive (false);
	}
	public void MoveToGame()
	{
		GameManager.instance.FullPackage_Offer = false;
		GameManager.instance.questionaireForLink.Questionaire.SetActive (false);
		GameManager.instance.Oops_panel.SetActive (false);
		if(GameManager.isFromFTUE){
			GameManager.instance.MainMenuButton (true);
			GameManager.isFromFTUE = false;
		}
		else{
			if (IS_SHOW_SALE_OFFER) {
				gameObject.SetActive (true);
			} else {
				gameObject.SetActive (false);
			}
		}
//		GameManager.instance.ButtonClick_Audio.Play ();
	}
}
