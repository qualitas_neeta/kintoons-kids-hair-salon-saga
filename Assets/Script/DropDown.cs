﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropDown : MonoBehaviour {

	public static DropDown instance;
	public Animator Dropdown_Anim;
	public Animator BG_Dropdown_Anim;
	public bool droped = false;
	public bool BG_Drop= false;
//	public GameObject FingerTutor2;
//	public GameObject FingerTutor3;
	public AudioSource DrawerOpen_Close;

	bool FirstStartAnimation;
	void Start () {
		instance = this;

	}

	void Update () {
		FirstStartAnimation = GameManager.instance.FirstStartAnim;
		if (Settings.instance != null) {
			DrawerOpen_Close.volume = (Settings.instance.sfx_slider.value * 0.2f);
		}
		if(FTUEHandler.isPlaying_AnyTutorial && droped &&FTUEHandler.CURRENT_TUTORIAL_INDEX ==FTUEHandler.handleCharacterDropDown){
			FTUEHandler.CompleteTutorial (FTUEHandler.handleCharacterDropDown);
			GameManager.instance.FingerTutor2.SetActive (false);
			FirstStartAnimation = false;
			GameManager.instance.timeRun++;
			PlayerPrefs.SetInt ("SavedFristRun", GameManager.instance.timeRun);
			PlayerPrefs.Save ();
			if (FTUEHandler.CanPlayHelp(FTUEHandler.handleChangeCharacter)) {
				FTUEHandler.setTutorial (FTUEHandler.handleChangeCharacter);
				GameManager.instance.FingerTutor3.SetActive (true);
			}
		}
	}

	public void DropdownMenu(){
		AppOn.HairGame.AppOn_HairGame.Instance.disableDrierBlowAnim ();
		AppOn.HairGame.AppOn_HairGame.Instance.disableShower ();
		AppOn.HairGame.AppOn_HairGame.Instance.stopShower ();
		AppOn.HairGame.AppOn_HairGame.Instance.Deselection ();
		AppOn.HairGame.AppOn_HairGame.Instance._cur_hair_style = "None";
		int timeRun = GameManager.instance.timeRun;

		PlayerPrefs.SetInt ("SavedFristRun", 2);
		GameManager.instance.timeRun++;
		PlayerPrefs.Save ();
		DrawerOpen_Close.Play ();
		if(!droped){
			
			Dropdown_Anim.SetTrigger("DropDown");
			droped = true;
//			FingerTutor2.SetActive (false);
			if(FirstStartAnimation && GameManager.instance.timeRun <= 3){
//				FingerTutor3.SetActive (true);
				FirstStartAnimation = false;
				GameManager.instance.timeRun++;
				PlayerPrefs.Save ();
			}
		}
		else{
			Dropdown_Anim.SetTrigger("DropUp");
			AppOn.HairGame.AppOn_HairGame.Instance.CombHair (false);
//			FingerTutor3.SetActive (false);
			droped = false;
//			BothUp ();
		}
	}

	public void DropUp(){
		if (droped) {
			Dropdown_Anim.SetBool("DropUp",true);
			droped = false;
		}
	}

	public void BG_DrawerDrop(){
		AppOn.HairGame.AppOn_HairGame.Instance.disableDrierBlowAnim ();
		AppOn.HairGame.AppOn_HairGame.Instance.stopShower ();
		AppOn.HairGame.AppOn_HairGame.Instance.disableShower();
		DrawerOpen_Close.Play ();
		if(!BG_Drop){
			AppOn.HairGame.AppOn_HairGame.Instance.Deselection ();
			AppOn.HairGame.AppOn_HairGame.Instance._cur_hair_style = "None";
			BG_Dropdown_Anim.SetTrigger ("DrawerDropDown");
			BG_Drop = true;
		}
		else {
			BG_Dropdown_Anim.SetTrigger ("DrawerDropUp");
			BG_Drop = false;
//			BothUp ();
		}
	}

	public void BothUp(){
		if(BG_Drop){
			Debug.Log ("DrawerDropUp anim fire");
			BG_Dropdown_Anim.SetTrigger ("DrawerDropUp");
			BG_Drop = false;
		}
		if (droped) {
			Dropdown_Anim.SetTrigger ("DropUp");
			droped = false;
		}
	}
}