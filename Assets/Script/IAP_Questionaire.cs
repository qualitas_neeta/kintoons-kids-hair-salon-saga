﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using AppOn.HairGame;
public class IAP_Questionaire : MonoBehaviour {

	public static IAP_Questionaire instance;

	public GameObject Questionaire;
	public Text Question_Text;
	public Text OptionA;
	public Text OptionB;
	public Text OptionC;
	public Text OptionD;
	Text CorrectAns;
	GameObject goClicked;

	// Use this for initialization
	void Start () {
		instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SelectQuestion(){
		goClicked = EventSystem.current.currentSelectedGameObject;
		GameManager.instance.ButtonClick_Audio.Play ();
		Questionaire.SetActive (true); 

		GameManager.instance.AvatarBuy_Panel.SetActive (false);
		GameManager.instance.CallGrownUp_Panel.SetActive (false);
		int i = Random.Range (0, 3);

		if (i == 0)
			Question1 ();
		else if (i == 1)
			Question2 ();
		else if (i == 2)
			Question3 ();
		else if (i == 3)
			Question4 ();
	}

	public void Question1(){
		Question_Text.text = "2 + 2 = ?";
		OptionA.text = "2";
		OptionB.text = "4";
		OptionC.text = "6";
		OptionD.text = "8";
		CorrectAns = OptionB;
	}

	public void Question2(){
		Question_Text.text = "3 + 6 = ?";
		OptionA.text = "3";
		OptionB.text = "7";
		OptionC.text = "9";
		OptionD.text = "5";
		CorrectAns = OptionC;
	}
	public void Question3(){
		Question_Text.text = "20 - 10 = ?";
		OptionA.text = "10";
		OptionB.text = "20";
		OptionC.text = "30";
		OptionD.text = "40";
		CorrectAns = OptionA;
	}
	public void Question4(){
		Question_Text.text = "8 - 4 = ?";
		OptionA.text = "8";
		OptionB.text = "6";
		OptionC.text = "0";
		OptionD.text = "4";
		CorrectAns = OptionD;
	}

	public void AnswerCheck(){
		GameManager.instance.ButtonClick_Audio.Play ();
		var go = EventSystem.current.currentSelectedGameObject;
		Text go_text = go.GetComponentInChildren<Text> ();

		if (go_text.text == CorrectAns.text) {
			if (GameManager.instance.FullPackage||GameManager.instance.FullPackage_From_MainMenu) {
				#if UNITY_ANDROID||UNITY_IOS
				#if IAP
				IAP_Manager.instance.BuyProduct_CompletePackage ();
				#endif
				#endif
			}
			else if (GameManager.instance.FullPackage_Offer) {
				#if UNITY_ANDROID||UNITY_IOS
				#if IAP
				IAP_Manager.instance.BuyProductID (IAP_Manager.Buy_Complete_Package_In_Offer);
				#endif
				#endif

			}
            else if (GameManager.instance.removeAds) {
                    #if UNITY_ANDROID||UNITY_IOS
				#if IAP
                IAP_Manager.instance.BuyProductID (IAP_Manager.removeAds);
				#endif
                    #endif
                GameManager.instance.removeAds = false;
            }
			else if (GameManager.instance.forparent_bool) {
				GameManager.instance.ForParent_Panel.SetActive (true);
				GameManager.instance.ForParentPanel_Bool = true;

				GameManager.instance.forparent_bool = false;
				GameManager.instance.Avatar_Panel.SetActive (false);
				if(GameManager.LinkIndex!=-1){
					GameManager.instance.MoveToURL ();
				}
			}else if(GameManager.LinkIndex!=-1){
				GameManager.instance.MoveToURL ();
			}
		} else {
            GameManager.instance.removeAds = false;
			GameManager.instance.Oops_panel.SetActive (true);
		}
		Questionaire.SetActive (false);

	}
}
