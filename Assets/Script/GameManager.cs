﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.SceneManagement;
using AppOn.HairGame;
#if ADS
using KidozSDK;
#endif

public class GameManager : MonoBehaviour {

	private static bool IS_PREMIUM_BUILD;//Ajay
    public static bool IS_ADS_ENABLE;
    public static string RMS_ADS_REMOVE="RMS_ADS_REMOVE";
	public static GameManager instance;
	public GameObject Janet;
	public GameObject Merle;
	public GameObject Paolo;
	public GameObject UGA;
	public GameObject Shop_Panel;
	public GameObject restoreButtonForIOS;
	public GameObject Accessories_Panel;
	public GameObject Locked_Button;
	public GameObject AvatarBuy_Panel;
	public GameObject allAvatarBuy_Panel;
	public GameObject CallGrownUp_Panel;
	public GameObject DrawerConatainer;
	public GameObject Camera_Button;
//	public GameObject ShopButton_UI;
	public GameObject HomeButton;
	public Button BgSelectionButton;
	public Button AvtarSelectionButton;
//	public Sprite[] spAvatar;
	public bool allBuy;
	public static bool BoughtUga;
	public static bool BoughtPaulo;
	public static bool BoughtJanet;
	public static bool SprayBuy;
	public GameObject DollarImg_Paolo;
	public GameObject DollarImg_UGA;
	public GameObject DollarImg_Merle;
	public GameObject hairs;
	public GameObject JanetHairs;
	public GameObject MerleHairs;
	public GameObject UGAHairs;
	public GameObject PaoloHairs;
	public IAP_Questionaire questionaireForLink;
	public static bool splashAnimStart=false;
	int i = 0;
	public Animator Merle_Anim;
	public Animator Paulo_Anim;
	public Animator Janet_Anim;
	public Animator UGA_Anim;
	public GameObject[] hairTemplateArray;
	public float noTouchTime;
	public bool touch;
	public Sprite[] hair_bottom_sprite;
	public GameObject MainMenu;
	public GameObject mainMenuControls;
	public Text creditVersionText;
//	public GameObject pleaseWaitMessage;
//	public Animator StartAnim;

	public GameObject[] SprayButtons; 
//	public Image UGA_Img;
	public static int Avatar_No = AppOn_HairGame.CHARACTER_MERLE;//0;
	public Animator FingerAnim;
	public bool FirstStartAnim = false;
	public int timeRun;
	public bool boughtCompletePackage ;
	public bool FullPackage = false;
	public bool FullPackage_Offer = false;
    public bool removeAds = false;
	public bool FullPackage_From_MainMenu = false;
	public bool BG1Change;
	public bool BG2Change;
	public bool BuyAvatar = false;
	GameObject goClicked;
//	public Button allBuyButton;
	public Image CenterPeice;
	public Sprite[] CenterImg;
	public GameObject Oops_panel;
	public GameObject BuyAllSection_panel;
	public GameObject BlackAlpha;
	public GameObject ForParent_Panel;
	public GameObject BuySign;
	public GameObject BuySign1;
	public GameObject BuySign2;
	public GameObject BuySign3;
	public GameObject Avatar_Panel;
	public AudioSource CharacterChange;
	public AudioSource MenuSelection_Audio;
	public AudioSource ButtonClick_Audio;
//	public GameObject TopLayout;
	public bool MainMenu_Bool;
	public bool ForParentPanel_Bool;
	public bool ShopPanel_Bool;
	public GameObject Settings_PopUp;
	public Button _settingButton;
	public GameObject FingerTutor2;
	public GameObject FingerTutor3;
	public GameObject dragHandObject;
	public GameObject hairGrowTutorial;
	public Animator SplashScreen_Anim;
	public GameObject Quit_PopUp;
//	public Text BuyAllSection_text;
	public static BuyData GameData;
	private static bool isfromGamePlay=false;
	static bool canChangeTemplate=true;
	public Button[] SelectAvtarPanelBtn;
	// Use this for initialization
	public GameObject SaleOfferPopup;
	public GameObject SaleOfferButton;
	public GameObject buyFullVersionButton;
	public Text SaleOfferTimeText;
	public static int currentGameLaunchCounter=0;
	private static int rateUsPopUpCounter=0;
	public GameObject LoadingScreen;
    public GameObject BtnRemoveAds;
    void Awake()
    {
        instance = this;
//              PlayerPrefs.DeleteAll ();
        #if UNITY_ANDROID
        IS_PREMIUM_BUILD=true;
        #elif UNITY_IOS
        IS_PREMIUM_BUILD=false;
        #endif
        IS_ADS_ENABLE = false;

        if (!PlayerPrefs.HasKey(RMS_ADS_REMOVE))// && PlayerPrefs.GetInt(RMS_ADS_REMOVE)!=1)
            IS_ADS_ENABLE = true;
        else if (PlayerPrefs.HasKey(RMS_ADS_REMOVE) && PlayerPrefs.GetInt(RMS_ADS_REMOVE) == 1)
        {
            IS_ADS_ENABLE = false;
            BtnRemoveAds.SetActive(false);
        }
       
        FTUEHandler.load ();

        rateUsPopUpCounter = PlayerPrefs.GetInt ("rateUsPopUpCounter",0);
        String isShowOfferPopup = PlayerPrefs.GetString ("isShowSaleOfferPopupFTUE","false");
        if(isShowOfferPopup=="true")
        {
            isShowSaleOfferPopupFTUE=true;
        }
        else{
            isShowSaleOfferPopupFTUE=false;
        }
        if(PlayerPrefs.GetString("isShowRateUsPopup","true")=="true")
        {
            isShowRateUsPopup=true;
        }
        else{
            isShowRateUsPopup=false;
        }
        if(FTUEHandler.isAllFTUEComplete() && rateUsPopUpCounter >= 3){
            isShowRateUsPopup = false;
            PlayerPrefs.SetString("isShowRateUsPopup","false");
        }
        if(IS_PREMIUM_BUILD||SprayBuy)
        {
            SaleOfferButton.SetActive (false);
            SaleOfferPopup.SetActive (false);
            SaleOffer.IS_SHOW_SALE_OFFER=false;
            buyFullVersionButton.SetActive (false);
        }
       
    }

	void Start () {

		#if UNITY_ANDROID
		 path = Application.persistentDataPath +@"/";
		#elif UNITY_IOS
		 path = Application.persistentDataPath +"/";
		#endif
        #if firebase
		Firebase.Analytics.FirebaseAnalytics.SetAnalyticsCollectionEnabled (true);
        #endif
//		MainMenu.SetActive (true);
//		mainMenuControls.SetActive (true);
//		MainMenu_Bool = true;
        MenuMainShow();
//		StartAnim.enabled = false;
//		instance = this;
		Accessories_Panel.SetActive (false);
	
		timeRun = PlayerPrefs.GetInt ("SavedFristRun", 0);
		Avatar_No = AppOn_HairGame.CHARACTER_MERLE;//AppOn_HairGame.CHARACTER_JANET;
//		Show_Panels (false);
		Avatar_Panel.SetActive (false);
		HandleIAPButton ();
        #if ADS
//     if (AdsManager.IS_AD_SHOWN) 
        if(IS_ADS_ENABLE)
        {
			if(KidozManager.Instance!=null)
            	AdsManager.Insatnce.Initilize();
//            KidozManager.Instance.init();
        }
        #endif 
//		currentGameLaunchCounter = PlayerPrefs.GetInt ("currentGameLaunchCounter", 0);
	}
//	static bool  isShowRateUsPopup=false;
	private static bool isShowSaleOfferPopupFTUE;
	
	// Update is called once per frame
	public ParticleSystem offerParticle;
	void Update () {
		if(MainMenu.activeSelf&& SaleOfferButton.activeSelf)
		{
			if((SaleOfferPopup.activeSelf||Settings_PopUp.activeSelf||Quit_PopUp.activeSelf
				||Oops_panel.activeSelf
				||questionaireForLink.Questionaire.activeSelf) )
			{
				if (offerParticle.gameObject.activeSelf) {
					offerParticle.gameObject.SetActive (false);
				}
			}
			else{
				if (!offerParticle.gameObject.activeSelf) {
					offerParticle.gameObject.SetActive (true);
				}
			}
		}
		Check_Touch ();
		if(Avatar_No ==AppOn_HairGame.CHARACTER_MERLE){
			if (FTUEHandler.CanPlayHelp(FTUEHandler.handleCharacterDropDown) &&Avatar_Panel.activeSelf && !dragHandObject.activeSelf  && !ForParentPanel_Bool ) {
				Shop_Panel.SetActive (false);
				ShopPanel_Bool = false;
				Accessories_Panel.SetActive (true);
				DrawerConatainer.SetActive (true);
				btnOpenCam.SetActive (true);
				Avatar_Panel.SetActive (true);

				FingerTutor2.SetActive (true);
				FTUEHandler.setTutorial (FTUEHandler.handleCharacterDropDown);

				FirstStartAnim = true;
				timeRun++;
				iscalled = true;
				PlayerPrefs.SetInt ("SavedFristRun", timeRun);
				PlayerPrefs.Save ();
			}
			else if (FTUEHandler.CanPlayHelp(FTUEHandler.handleDragPanel) &&  Accessories_Panel.activeSelf && !dragHandObject.activeSelf && Avatar_Panel.activeSelf  && !ForParentPanel_Bool) {
				Shop_Panel.SetActive (false);
				FTUEHandler.setTutorial (FTUEHandler.handleDragPanel);
				ShopPanel_Bool = false;
				Accessories_Panel.SetActive (true);
				btnOpenCam.SetActive (true);
				DrawerConatainer.SetActive (true);
				Avatar_Panel.SetActive (true);
				FingerAnim.SetTrigger ("StartTutor");
				timeRun++;
				PlayerPrefs.SetInt ("SavedFristRun", timeRun);
				PlayerPrefs.Save ();


			}else if(timeRun == 0)
			{
				iscalled = true;
			}


		}

		if(Input.GetKeyDown (KeyCode.Escape))
		{
			onBackPressed ();
		}
		if (Settings.instance != null) {
			CharacterChange.volume = (Settings.instance.sfx_slider.value * 0.2f);
			MenuSelection_Audio.volume = (Settings.instance.sfx_slider.value * 0.2f);
			ButtonClick_Audio.volume = (Settings.instance.sfx_slider.value * 0.2f);
		}
		if(SaleOffer.IS_SHOW_SALE_OFFER ){
			if (!SaleOfferPopup.activeSelf &&SaleOffer.LaunchGame) {
				SaleOfferPopup.GetComponent<SaleOffer> ().checkTimer ();
			}
			SaleOfferTimeText.text = SaleOfferPopup.GetComponent<SaleOffer> ().timeText.text;
		}
		FTUEHandler.Update ();
	}

	public void ShowHairGrowTutorial()
	{
		hairGrowTutorial.GetComponent<Animator> ().SetInteger ("tutorialID", 0);
		hairGrowTutorial.SetActive (true);
	}

	public void  Block_Unblock_UI( bool isEnable)
	{
		if(isEnable){
			AppOn_HairGame.Instance.cutButton.enabled = isEnable;
			AppOn_HairGame.Instance.trimButton.enabled = isEnable;
			AppOn_HairGame.Instance.combButton.enabled = isEnable;
			Accessories_Panel.GetComponent<ScrollRect> ().enabled = isEnable;
			enableDisableTopPanelButton (isEnable);
			AvtarSelectionButton.GetComponent<Button> ().enabled = isEnable;
			SelectAvtarPanelBtn [0].interactable = true;
		} else {
			switch (FTUEHandler.CURRENT_TUTORIAL_INDEX) {
			case FTUEHandler.handleDragPanel:
				enableDisableTopPanelButton (isEnable);
				AvtarSelectionButton.GetComponent<Button> ().enabled = isEnable;
				break;
			case FTUEHandler.handleChangeCharacter:
			case FTUEHandler.handleCharacterDropDown:
				enableDisableTopPanelButton (isEnable);
				Accessories_Panel.GetComponent<ScrollRect> ().enabled = isEnable;
				if (FTUEHandler.CURRENT_TUTORIAL_INDEX == FTUEHandler.handleChangeCharacter) {
					SelectAvtarPanelBtn [0].interactable = false;
					AvtarSelectionButton.GetComponent<Button> ().enabled = false;
				}
				break;
			case FTUEHandler.handleHairGrow:
			case FTUEHandler.handleHairGrowBottleSelction:
				AppOn_HairGame.Instance.cutButton.enabled = isEnable;
				AppOn_HairGame.Instance.trimButton.enabled = isEnable;
				AppOn_HairGame.Instance.combButton.enabled = isEnable;
				Accessories_Panel.GetComponent<ScrollRect> ().enabled = isEnable;
				enableDisableTopPanelButton (isEnable);
				AvtarSelectionButton.GetComponent<Button> ().enabled = isEnable;
				break;
			}
		}
	}

	public void CloseHairGrowTuorial()
	{
		hairGrowTutorial.SetActive (false);
		hairGrowTutorial.GetComponent<Animator> ().SetInteger ("tutorialID",-1);
	}
	void onBackPressed()
	{
		if(ScreenShot.shot||LoadingScreen.activeSelf)
		{
			return;
		}
		if (  Shop_Panel.activeSelf) {
			if (BuyAllSection_panel.activeSelf) {
				AvatarBuy_CancelButton ();
			} else if (CallGrownUp_Panel.activeSelf) {
				AvatarBuy_CancelButton ();
			} else if (IAP_Questionaire.instance != null && IAP_Questionaire.instance.Questionaire.activeSelf) {
				ShopClose_Button ();
			}
			else
				ShopClose_Button ();
		}		
		else if ( MainMenu.activeSelf) {
			if (Settings_PopUp.activeSelf ){
				Settings_CloseButton();
		
			}else if (SaleOfferPopup.activeSelf ){
				if(questionaireForLink.gameObject.activeSelf)
				{
					GameManager.instance.FullPackage_Offer = false;
					questionaireForLink.gameObject.SetActive (false);
				} else {
					SaleOfferPopup.GetComponent<SaleOffer> ().CanclePurchase ();
				}

			}else if(questionaireForLink.gameObject.activeSelf)
			{
				GameManager.instance.FullPackage_From_MainMenu = false;
				questionaireForLink.gameObject.SetActive (false);
			}   else {
				if (!Quit_PopUp.activeSelf) {
					Quit_PopUp.SetActive (true);
				}else{
					Quit_PopUp.SetActive (false);
				}

			}
		}
		else if ( ForParent_Panel.activeSelf) {
			if(questionaireForLink.gameObject.activeSelf)
			{
				LinkIndex = -1;
				questionaireForLink.gameObject.SetActive (false);
			} else {
				ForParent_MenuButton ();
			}
		}
		else{
			if (BG_Change.instance.BuyBG_Panel.activeSelf) {
				AvatarBuy_CancelButton ();
			}  else if (AvatarBuy_Panel.activeSelf) {
				AvatarBuy_CancelButton ();
			} else if (CallGrownUp_Panel.activeSelf) {
				AvatarBuy_CancelButton ();
			} else if (IAP_Questionaire.instance != null && IAP_Questionaire.instance.Questionaire.activeSelf) {
				ShopClose_Button ();
			} else if (Oops_panel.activeSelf) {
				ShopClose_Button ();
			}else if(CamPanel.activeSelf)
			{
				PhoneCamera.Instance.close ();
			}else {
				if(!checkAvatarBuyStatus())
				{
					SelectAvatar (AppOn_HairGame.CHARACTER_MERLE);//0
				}else
					Home_Button ();
			}
		}

	}

	bool checkAvatarBuyStatus(){
		if (Avatar_No == AppOn_HairGame.CHARACTER_JANET&& BoughtJanet) {
			return true;
		} else if (Avatar_No == AppOn_HairGame.CHARACTER_UGA && BoughtUga) {
			return true;
		} else if (Avatar_No == AppOn_HairGame.CHARACTER_PAULO && BoughtPaulo) {
			return true;
		} else if (Avatar_No ==  AppOn_HairGame.CHARACTER_MERLE) {
			return true;
		}
		return false;
	}

	public void CheckAnimationComplete(){
		if(CharacterSoundManager.IsEntryAnimPlaying){
			return;
		}
		int x = UnityEngine.Random.Range (2, 6);
		if(Avatar_No ==  AppOn_HairGame.CHARACTER_MERLE)
			Merle_Anim.SetInteger ("State", x);
		if(Avatar_No ==AppOn_HairGame.CHARACTER_PAULO)
			Paulo_Anim.SetInteger ("State", x);
		if(Avatar_No == AppOn_HairGame.CHARACTER_UGA)
			UGA_Anim.SetInteger ("State", x);
		if(Avatar_No == AppOn_HairGame.CHARACTER_JANET)
			Janet_Anim.SetInteger ("State", x);
		AnimCheck = false;
		noTouchTime = Time.time;
		StartCoroutine ("Delaytouch");
	
	}



	public bool AnimCheck = false;
	public void Check_Touch(){

		if (Input.GetMouseButtonUp (0)) {
			noTouchTime = Time.time;

		}
		if ((Time.time - noTouchTime) > 7) {
			CheckAnimationComplete ();
			noTouchTime = Time.time;
		}
	}
	public static bool isFromFTUE=false;

	public void PlayButtonAnim(){
        
		if (splashAnimStart)
			return;
		if (FTUEHandler.isAllFTUEComplete() &&!isShowSaleOfferPopupFTUE )// call only once in game life
		{
			if(SaleOffer.IS_SHOW_SALE_OFFER &&!SprayBuy)
			{
				OpenSaleOfferPopup ();
				isFromFTUE = true;
			}
			else{
                #if ADS
                if (IS_ADS_ENABLE) {
//					AdsManager.Insatnce.Show ();
                    KidozManager.Instance.ShowAds();
				}
                #endif
				MenuSelection_Audio.Play ();
				SplashScreen_Anim.SetTrigger ("Pressed");
			}

			isShowSaleOfferPopupFTUE=true;
			PlayerPrefs.SetString("isShowSaleOfferPopupFTUE","true");
		}
		else{
            #if ADS
            if (IS_ADS_ENABLE && FTUEHandler.isAllFTUEComplete())//ajay
            {
//				AdsManager.Insatnce.Show ();
				if(KidozManager.Instance!=null)
                	KidozManager.Instance.ShowAds();
			}
            #endif
			MenuSelection_Audio.Play ();
			SplashScreen_Anim.SetTrigger ("Pressed");
		}
		/*
		 else if(FTUEHandler.isAllFTUEComplete() && isShowSaleOfferPopupFTUE &&isShowRateUsPopup)
		{
			isShowRateUsPopup = true;
		}
		 */
	}

	bool iscalled = false;
	public void MainMenuButton(bool isFrom_MainMenuFTUE=false){
		MainMenu.SetActive (false);
		mainMenuControls.SetActive (false);
		MainMenu_Bool = false;
		DrawerConatainer.SetActive (false);
		btnOpenCam.SetActive (false);
		Accessories_Panel.SetActive (false);
		Avatar_Panel.SetActive (false);
		LoadingScreen.SetActive (true);
		StartCoroutine (MainMenuDelay(isFrom_MainMenuFTUE));
	}
	static bool _isFrom_MainMenuFTUE;
	IEnumerator MainMenuDelay(bool isFrom_MainMenuFTUE){
		yield return new WaitForSeconds (.5f);
		Camera_Button.GetComponent<Button> ().enabled = true;
		_isFrom_MainMenuFTUE = isFrom_MainMenuFTUE;
		try{
			Load ();	

		}catch(Exception e)
		{
			Debug.Log ("Hair Saloon : Laod failed " + e.StackTrace);
		}
		Avatar_No = AppOn_HairGame.CHARACTER_MERLE;
		bool isShowRateUsPopup = true;
		if(isFrom_MainMenuFTUE){
			DrawerConatainer.SetActive (true);
			btnOpenCam.SetActive (true);
			Accessories_Panel.SetActive (true);
			Avatar_Panel.SetActive (true);
			Show_Panels (false);
		}
		else if (FTUEHandler.isAllFTUEComplete() && isShowSaleOfferPopupFTUE && isShowRateUsPopup)
		{
			DrawerConatainer.SetActive (true);
			btnOpenCam.SetActive (true);
			Accessories_Panel.SetActive (true);
			Avatar_Panel.SetActive (true);
			Show_Panels (false);
			Avatar_Panel.SetActive (true);
//			rateUsPopUpCounter++;
//			PlayerPrefs.SetInt ("rateUsPopUpCounter", rateUsPopUpCounter);
//			isShowRateUsPopup = false;
//			LoadNextScreen ();
//			ShowRateUsPopup ();
		}
		else if (timeRun > 1 && !SprayBuy && !iscalled) 
		{
//			DrawerConatainer.SetActive (true);
//			btnOpenCam.SetActive (true);
//			Accessories_Panel.SetActive (true);
//			Avatar_Panel.SetActive (true);
			LoadNextScreen ();
			Shop_Button ();
			iscalled = true;
			isfromGamePlay = false;
			Avatar_Panel.SetActive (false);
			Accessories_Panel.SetActive (false);
			Locked_Button.SetActive (false);
			DrawerConatainer.SetActive (false);
			btnOpenCam.SetActive (false);
		} else {
			DrawerConatainer.SetActive (true);
			btnOpenCam.SetActive (true);
			Accessories_Panel.SetActive (true);
			Avatar_Panel.SetActive (true);
			Show_Panels (false);
			Avatar_Panel.SetActive (true);
		}
	}


	IEnumerator Delaytouch(){
		yield return new WaitForSeconds (1f);

	}

	public void Shop_Button(bool isFromIndividualBuy=false){
		if(Input.GetMouseButtonUp(0) &&!isFromIndividualBuy)
			ButtonClick_Audio.Play ();
//		i = 1;
		Shop_Panel.SetActive (true);

		#if UNITY_IOS
		restoreButtonForIOS.SetActive (true);
		#else
		restoreButtonForIOS.SetActive (false);
		#endif
		if (!isFromIndividualBuy) {
			StartCoroutine ("Delay");
			AppOn.HairGame.AppOn_HairGame.Instance._cur_hair_style = "None";
		}
		ShopPanel_Bool = true;
		isfromGamePlay=true;
	}
	public void ShopClose_Button(){
        removeAds = false;
		if(Input.GetMouseButtonUp(0))
			ButtonClick_Audio.Play ();
		if(IAP_Questionaire.instance !=null)
			IAP_Questionaire.instance.Questionaire.SetActive (false);
		if(LinkIndex!=-1 &&!Shop_Panel.activeSelf){
			questionaireForLink.Questionaire.SetActive (false);
			Oops_panel.SetActive (false);
			ForParent_Panel.SetActive (true);
			LinkIndex = -1;
		} else if(FullPackage_From_MainMenu){
			FullPackage_From_MainMenu = false;
			questionaireForLink.Questionaire.SetActive (false);
			GameManager.instance.Oops_panel.SetActive (false);
//			MainMenu.SetActive (true);
		}else if(FullPackage_Offer ){
			FullPackage_Offer = false;
			SaleOfferPopup.GetComponent<SaleOffer> ().MoveToGame ();
		}else {
			questionaireForLink.Questionaire.SetActive (false);
			Oops_panel.SetActive (false);
			Shop_Panel.SetActive (false);
			ShopPanel_Bool = false;
			if (isfromGamePlay) {
				if(SprayBuy){
//					ShopButton_UI.SetActive (false);
					AppOn_HairGame.Instance.ResetDefaultAccessories ();

				}
				isfromGamePlay = false;
//				SelectAvatar (AppOn_HairGame.CHARACTER_MERLE);

			} else {
				LoadingScreen.SetActive (true);
				DrawerConatainer.SetActive (false);
				btnOpenCam.SetActive (false);
				Accessories_Panel.SetActive (false);
				Avatar_Panel.SetActive (false);
				LinkIndex = -1;
				ButtonClick_Audio.Play ();
				StartCoroutine ("StartGame");
			}
		}
	}
	public void enableFromPurchaseScreen()
	{
		LoadingScreen.SetActive (false);
		DrawerConatainer.SetActive (true);
		btnOpenCam.SetActive (true);
		Accessories_Panel.SetActive (true);
		Avatar_Panel.SetActive (true);
		LinkIndex = -1;
		FullPackage = false;
		forparent_bool = false;
		BlackAlpha.SetActive (false);
	}
	IEnumerator StartGame()
	{
		yield return new WaitForSeconds (0.5f);
		Oops_panel.SetActive (false);
		BlackAlpha.SetActive (false);
		Camera_Button.GetComponent<Button> ().enabled = true;
		DrawerConatainer.SetActive (true);
		btnOpenCam.SetActive (true);
		Accessories_Panel.SetActive (true);
		Avatar_Panel.SetActive (true);
		if(SprayBuy){
//			ShopButton_UI.SetActive (false);
			AppOn_HairGame.Instance.ResetDefaultAccessories ();

		}
		if ((Avatar_No == AppOn_HairGame.CHARACTER_UGA && !BoughtUga)
			|| (Avatar_No == AppOn_HairGame.CHARACTER_PAULO && !BoughtPaulo)
			|| (Avatar_No == AppOn_HairGame.CHARACTER_JANET && !BoughtJanet) ||
			Avatar_No == AppOn_HairGame.CHARACTER_MERLE) {

			Show_Panels (false);
			SelectAvatar (AppOn_HairGame.CHARACTER_MERLE);

		} else {
			AppOn.HairGame.AppOn_HairGame.Instance.LateStart ();
			hairStyle ();
		}
	}
	public static void destroyAllAccesories()
	{
		List<Transform> listToDestroy = new List<Transform>();
		if( DragDrop.realTargetParent != null){
			for (int i = 0; i < DragDrop.realTargetParent.childCount; i++) {


				if (DragDrop.realTargetParent.GetChild (i).GetComponent<DragDrop> () != null) {
					listToDestroy.Add (DragDrop.realTargetParent.GetChild (i));
				}
			}

			foreach (Transform tr in listToDestroy) {
				Destroy (tr.gameObject);
			}
		}
	}
	public void enableDisableTopPanelButton(bool isEnable)
	{
		Camera_Button.GetComponent<Button> ().enabled = isEnable;
		HomeButton.GetComponent<Button> ().enabled = isEnable;
//		ShopButton_UI.GetComponentInChildren<Button> ().enabled = isEnable;
		BgSelectionButton.GetComponent<Button> ().enabled = isEnable;
		btnOpenCam.GetComponentInChildren<Button> ().enabled = isEnable;
	}

	private static Animator templateAnimation;

	public void ChangeTemplate(TemplateSelection templateSelection)
	{
		if(FTUEHandler.isPlaying_AnyTutorial)
		{
			return;
		}
		if ((AppOn_HairGame.templateIndex != templateSelection.index)||AppOn_HairGame.isAnyChangeDoneInHairStyle) {
			if (!canChangeTemplate)
				return;
			GameManager.instance.ButtonClick_Audio.Play ();
			AppOn_HairGame.isAnyChangeDoneInHairStyle = false;
			canChangeTemplate = false;
			LoadingScreen.gameObject.SetActive (true);
			StartCoroutine (chageHairTemplate (templateSelection));
		}
	}
	IEnumerator chageHairTemplate(TemplateSelection templateSelection)
	{
		yield return new WaitForSeconds (.9f);
		if(templateAnimation!=null){
			templateAnimation.SetBool ("selected", false);
		}
		templateAnimation=templateSelection.GetComponent<Animator>();
		templateAnimation.SetBool ("selected", true);
		AppOn_HairGame.templateIndex = templateSelection.index;
		enableDisableTopPanelButton (true);
		destroyAllAccesories();
		Camera_Button.GetComponent<Button> ().enabled = true;
		resetHairs ();
		AppOn_HairGame.Instance._cur_hair_style="None";
		AppOn_HairGame.Instance.Deselection ();
	}
	public void SelectAvatar(int id){//Ajay
		if (!canChangeTemplate)
			return;
		AppOn_HairGame.templateIndex = 0;
		resetTemplateSelection ();
		if((id == AppOn_HairGame.CHARACTER_UGA && !BoughtUga)
			|| (id == AppOn_HairGame.CHARACTER_PAULO && !BoughtPaulo)
			|| (id == AppOn_HairGame.CHARACTER_JANET && !BoughtJanet))
		{
			if( FTUEHandler.isPlaying_AnyTutorial  &&FTUEHandler.CURRENT_TUTORIAL_INDEX ==FTUEHandler.handleChangeCharacter)
			{
				FingerTutor3.SetActive (false);
				FTUEHandler.CompleteTutorial (FTUEHandler.handleChangeCharacter);
			}
           
			LoadingScreen.gameObject.SetActive (false);
			canChangeTemplate = true;
			isfromGamePlay = true;
			Shop_Button(true);
			ButtonClick_Audio.Play ();
			DropDown.instance.BothUp ();
		}
		else{
            #if ADS
            if(IS_ADS_ENABLE && FTUEHandler.isAllFTUEComplete())//if (AdsManager.IS_AD_SHOWN)
            {
                //AdsManager.Insatnce.Show ();
				if(KidozManager.Instance!=null)
                	KidozManager.Instance.ShowAds();
            }
            #endif
			enableDisableTopPanelButton (true);
			DragBottomPanel.Instance.init ();
			AppOn.HairGame.AppOn_HairGame.Instance.Deselection ();
			timeRun++;
			PlayerPrefs.SetInt ("SavedFristRun", timeRun);
			PlayerPrefs.Save ();
			destroyAllAccesories ();
			if (id == Avatar_No) {
				CharacterChange.Stop ();
			} else {
				CharacterChange.Play ();
			}


			if(id == Avatar_No)
			{
				Camera_Button.GetComponent<Button> ().enabled = true;
				resetHairs ();
			}else{
				for(int i=0;i<SelectAvtarPanelBtn.Length;i++){
					SelectAvtarPanelBtn[i].interactable=false;
				}
				Avatar_No = id;

				if( FTUEHandler.isPlaying_AnyTutorial  &&FTUEHandler.CURRENT_TUTORIAL_INDEX ==FTUEHandler.handleChangeCharacter)
				{
					FingerTutor3.SetActive (false);
					FTUEHandler.CompleteTutorial (FTUEHandler.handleChangeCharacter);
				}
				enableDisableTopPanelButton (true);
				Camera_Button.GetComponent<Button> ().enabled = true;
				StartCoroutine ("Delay");
			}
		}
	}
		
	public void AvatarBuy_Button(){
		enableDisableTopPanelButton (true);
		ButtonClick_Audio.Play ();
//		BlackAlpha.SetActive (true);
//		Locked_Button.SetActive (false);
		AvatarBuy_Panel.SetActive (true);
		goClicked = EventSystem.current.currentSelectedGameObject;
		if (goClicked.name == "Buy") {
			BuyAvatar = true;
			FullPackage = false;
			allBuy = false;
		}
		if (Avatar_No == AppOn_HairGame.CHARACTER_UGA) {
			CenterPeice.sprite = CenterImg [0];	
		}
		if (Avatar_No == AppOn_HairGame.CHARACTER_PAULO) {
			CenterPeice.sprite = CenterImg [1];	
		}
		if (Avatar_No == AppOn_HairGame.CHARACTER_JANET) {
			CenterPeice.sprite = CenterImg [2];	
		}

		AppOn.HairGame.AppOn_HairGame.Instance.Deselection ();
	}

	public void allAvatarBuy(){
		ButtonClick_Audio.Play ();
		allAvatarBuy_Panel.SetActive (true);
//		BlackAlpha.SetActive (true);
	}

	public void buyAllSection(){
		ButtonClick_Audio.Play ();
		BuyAllSection_panel.SetActive (true);
//		BlackAlpha.SetActive (true);
	}

	public void AvatarBuy_CancelButton(){
		ButtonClick_Audio.Play ();
		if (Locked_Button.activeSelf) {
			enableDisableTopPanelButton (false);
		}
		AvatarBuy_Panel.SetActive (false);
		allAvatarBuy_Panel.SetActive (false);
		CallGrownUp_Panel.SetActive (false);
		BuyAllSection_panel.SetActive (false);
		BlackAlpha.SetActive (false);
		BG_Change.instance.BuyBG_Panel.SetActive (false);
		BG_Change.current_bg_id_to_purchase = -1;
		AppOn.HairGame.AppOn_HairGame.Instance.Deselection ();
	}

	public void CallAGrownUp(){
		ButtonClick_Audio.Play ();
		CallGrownUp_Panel.SetActive (true);
		allAvatarBuy_Panel.SetActive (false);
		AvatarBuy_Panel.SetActive (false);
		BuyAllSection_panel.SetActive (false);
		BG_Change.instance.BuyBG_Panel.SetActive (false);
		goClicked = EventSystem.current.currentSelectedGameObject;
		if (goClicked.name == "CompleteBuy_Button") {
			FullPackage = true;
			allBuy = false;
		}
		if(goClicked.name == "Buy_Button"){
			allBuy = true;
			FullPackage = false;
		}
	}

	void hairStyle(){
		AppOn.HairGame.AppOn_HairGame.Instance.LoadDefaultHair ();
		if(DropDown.instance!=null && DropDown.instance.droped){
			DropDown.instance.DropUp ();
		}

	}
		

	IEnumerator Delay(){
		yield return new WaitForSeconds (0.1f);
		AppOn.HairGame.AppOn_HairGame.Instance.Deselection ();
		if (i != 1) {
			Show_Panels (true);
		}
		i = 0;
	}


	public Animator getAnimator()
	{
		if(Avatar_No == AppOn_HairGame.CHARACTER_JANET)
		{
			return Janet_Anim;
		}
		else if(Avatar_No == AppOn_HairGame.CHARACTER_UGA)
		{
			return UGA_Anim;
		}
		else if(Avatar_No == AppOn_HairGame.CHARACTER_PAULO)
		{
			return Paulo_Anim;
		}
		else if(Avatar_No ==  AppOn_HairGame.CHARACTER_MERLE)
		{
			return Merle_Anim;
		}
		else{
			return null;
		}
	}


	public void callEntryAnim()
	{
		if (Avatar_No ==AppOn_HairGame.CHARACTER_JANET) {
			Janet.SetActive (true);

			Merle.SetActive (false);
			Paolo.SetActive (false);
			UGA.SetActive (false);
			StartCoroutine( EntryAnim(Janet_Anim));
		}
		else if (Avatar_No ==  AppOn_HairGame.CHARACTER_MERLE) {
			Merle.SetActive (true);

			Janet.SetActive (false);
			Paolo.SetActive (false);
			UGA.SetActive (false);

			StartCoroutine( EntryAnim(Merle_Anim));
		}
		else if (Avatar_No == AppOn_HairGame.CHARACTER_PAULO){
			Paolo.SetActive (true);

			Janet.SetActive (false);
			Merle.SetActive (false);
			UGA.SetActive (false);
			StartCoroutine( EntryAnim(Paulo_Anim));
		}	
		else if (Avatar_No ==AppOn_HairGame.CHARACTER_UGA) {
			UGA.SetActive (true);

			Paolo.SetActive (false);
			Janet.SetActive (false);
			Merle.SetActive (false);
			StartCoroutine( EntryAnim(UGA_Anim));
		}
		StartCoroutine ("ResetTemplateAccess");
	}
	IEnumerator ResetTemplateAccess()
	{
		yield return new WaitForSeconds(0.5f);
		for(int i=0;i<hairTemplateArray.Length;i++)
		{
			if(i==Avatar_No)
			{
				hairTemplateArray [i].SetActive (true);
			}
			else{
				hairTemplateArray [i].SetActive (false);
			}
		}
		LoadingScreen.gameObject.SetActive (false);
		canChangeTemplate = true;
	}
	IEnumerator EntryAnim(Animator charanim){
//		charanim.transform.localScale=Vector3.zero;
		yield return new WaitForSeconds(0.01f);
		LoadNextScreen ();
//		dfs
		charanim.transform.localScale=Vector3.one;
		charanim.SetInteger ("State",1);

	}
	public void resetHairs()
	{
		if (Avatar_No == AppOn_HairGame.CHARACTER_JANET ) {
			AppOn.HairGame.AppOn_HairGame.Instance.LateStart ();

			hairs.transform.SetParent (JanetHairs.transform);
			hairs.transform.localPosition=new Vector3(0,0.76f,0);//Vector3.zero;
			hairs.transform.localEulerAngles=Vector3.zero;
			hairs.transform.localScale = Vector3.one;
		}
		if (Avatar_No ==  AppOn_HairGame.CHARACTER_MERLE) {
			AppOn.HairGame.AppOn_HairGame.Instance.LateStart ();
			hairs.transform.SetParent (MerleHairs.transform);
			hairs.transform.localPosition=new Vector3(0,0.76f,0);//Vector3.zero;
			hairs.transform.localEulerAngles=Vector3.zero;
			hairs.transform.localScale = Vector3.one;
		}
		if (Avatar_No == AppOn_HairGame.CHARACTER_PAULO){
			AppOn.HairGame.AppOn_HairGame.Instance.LateStart ();
			hairs.transform.SetParent (PaoloHairs.transform);
			hairs.transform.localPosition=new Vector3(0,0.3f,0);//Vector3.zero;
			hairs.transform.localEulerAngles=Vector3.zero;
			hairs.transform.localScale = Vector3.one;
		}	
		if (Avatar_No == AppOn_HairGame.CHARACTER_UGA) {
			AppOn.HairGame.AppOn_HairGame.Instance.LateStart ();
			hairs.transform.SetParent (UGAHairs.transform);
			hairs.transform.localPosition=new Vector3(0,1.2f,0);//Vector3.zero;
			hairs.transform.localEulerAngles=Vector3.zero;
			hairs.transform.localScale = Vector3.one;
		}
		hairStyle();
	}
	public void Show_Panels(bool sendEvent = true){
		Janet.SetActive (false);
		Merle.SetActive (false);
		Paolo.SetActive (false);
		UGA.SetActive (false);
		if (Avatar_No == AppOn_HairGame.CHARACTER_JANET) {
		
			AppOn.HairGame.AppOn_HairGame.Instance.LateStart ();
			hairs.transform.SetParent (JanetHairs.transform);
			hairs.transform.localPosition=new Vector3(0,0.76f,0);
			hairs.transform.localEulerAngles=Vector3.zero;
			hairs.transform.localScale = Vector3.one;
			if (!BoughtJanet) {
				AppOn.HairGame.AppOn_HairGame.Instance._cur_hair_style = "None";
				AppOn.HairGame.AppOn_HairGame.Instance.Deselection ();
			}
			if (sendEvent) {
				GoogleAnalytics.Instance.LogCustomEvent ("Janet", "CharacterSelected");
			}
		}
		else if (Avatar_No == AppOn_HairGame.CHARACTER_MERLE) {
			AppOn.HairGame.AppOn_HairGame.Instance.LateStart ();
			hairs.transform.SetParent (MerleHairs.transform);

			hairs.transform.localPosition=new Vector3(0,0.76f,0);//Vector3.zero;
			hairs.transform.localEulerAngles=Vector3.zero;
			hairs.transform.localScale = Vector3.one;
			GoogleAnalytics.Instance.LogCustomEvent ("Merle", "CharacterSelected");
		}
		else if (Avatar_No == AppOn_HairGame.CHARACTER_PAULO){
			AppOn.HairGame.AppOn_HairGame.Instance.LateStart ();
			hairs.transform.SetParent (PaoloHairs.transform);

			hairs.transform.localPosition=new Vector3(0,0.3f,0);//Vector3.zero;
			hairs.transform.localEulerAngles=Vector3.zero;
			hairs.transform.localScale = Vector3.one;
			if (!BoughtPaulo) {
				AppOn.HairGame.AppOn_HairGame.Instance._cur_hair_style = "None";
				AppOn.HairGame.AppOn_HairGame.Instance.Deselection ();
			}
			GoogleAnalytics.Instance.LogCustomEvent ("Paolo", "CharacterSelected");
	    }	
		else if (Avatar_No == AppOn_HairGame.CHARACTER_UGA) {
			AppOn.HairGame.AppOn_HairGame.Instance.LateStart ();
			hairs.transform.SetParent (UGAHairs.transform);

			hairs.transform.localPosition=new Vector3(0,1.2f,0);//Vector3.zero;
			hairs.transform.localEulerAngles=Vector3.zero;
			hairs.transform.localScale = Vector3.one;
			if (!BoughtUga) {
				AppOn.HairGame.AppOn_HairGame.Instance._cur_hair_style = "None";
				AppOn.HairGame.AppOn_HairGame.Instance.Deselection ();
			}
			GoogleAnalytics.Instance.LogCustomEvent ("UGA", "CharacterSelected");
		}
		
		Accessories_Panel.SetActive (true);
		Locked_Button.SetActive (false);
		btnOpenCam.SetActive (true);
		DrawerConatainer.SetActive (true);
		AvatarBuy_Panel.SetActive (false);
		if (!Shop_Panel.activeSelf) {
			hairStyle ();
		}

		if (BG1Change) {
			BuySign.SetActive (false);
		}
		if (BG2Change) {
			BuySign1.SetActive (false);
		}
		if (GameData.BG3_Purchase) {
			BuySign2.SetActive (false);
		}if (GameData.BG4_Purchase) {
			BuySign3.SetActive (false);
		}
		if (Avatar_No == AppOn_HairGame.CHARACTER_UGA && !BoughtUga||
			Avatar_No ==AppOn_HairGame.CHARACTER_PAULO && !BoughtPaulo||
			Avatar_No == AppOn_HairGame.CHARACTER_JANET && !BoughtJanet) {
			
			AppOn.HairGame.AppOn_HairGame.Instance._cur_hair_style = "None";
			AppOn.HairGame.AppOn_HairGame.Instance.Deselection ();
		}

		if(SprayBuy){
			SprayButtons [0].SetActive (true);
			SprayButtons [1].SetActive (true);
			SprayButtons [2].SetActive (true);
			SprayButtons [3].SetActive (true);
		}

		if(BoughtJanet)
			DollarImg_Merle.SetActive (false);
		if(BoughtPaulo)
			DollarImg_Paolo.SetActive (false);
		if(BoughtUga)
			DollarImg_UGA.SetActive (false);
	}

	public static string path = "";


	public void restoreBuyChar(int current_AvatarNo)
	{
		GameData.Avatar_No = current_AvatarNo;
		if (current_AvatarNo == AppOn_HairGame.CHARACTER_UGA) {
			BoughtUga = true;
			GameData.Bought1 = true;
		}
		if (current_AvatarNo == AppOn_HairGame.CHARACTER_PAULO) {
			BoughtPaulo = true;
			GameData.Bought2 = true;
		}
		if (current_AvatarNo == AppOn_HairGame.CHARACTER_JANET) {
			BoughtJanet = true;
			GameData.Bought3 = true;
		}
		if(BoughtJanet)
			DollarImg_Merle.SetActive (false);
		if(BoughtPaulo)
			DollarImg_Paolo.SetActive (false);
		if(BoughtUga)
			DollarImg_UGA.SetActive (false);
		writeData ();
	
	}

	public void writeData()
	{
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (path + "BuyInfo.dat");
		bf.Serialize (file, GameData);
		file.Close ();
	}
	public void BuyCharacter(int current_AvatarNo, bool handleCharChange = true){

//		BinaryFormatter bf = new BinaryFormatter ();
//		FileStream file = File.Create (path + "BuyInfo.dat");
//		BuyData data = new BuyData ();
		if(GameData==null)
		{
			Debug.Log ("BuyCharacter :" + GameData);
		}
		if (handleCharChange) {
			Avatar_No = current_AvatarNo;
			GameData.Avatar_No = Avatar_No;
			if (Avatar_No == AppOn_HairGame.CHARACTER_UGA) {
				BoughtUga = true;
				GoogleAnalytics.Instance.LogCustomEvent ("UGA", "CharacterBought");
			}
			else if (Avatar_No ==AppOn_HairGame.CHARACTER_PAULO) {
				BoughtPaulo = true;
				GoogleAnalytics.Instance.LogCustomEvent ("Paolo", "CharacterBought");
			}
			else if (Avatar_No == AppOn_HairGame.CHARACTER_JANET) {
				BoughtJanet = true;
				GoogleAnalytics.Instance.LogCustomEvent ("Janet", "CharacterBought");
			}
			if (allBuy) {
				BoughtUga = true;
				BoughtPaulo = true;
				BoughtJanet = true;
				GoogleAnalytics.Instance.LogCustomEvent ("All", "CharacterBought");
			}
			GameData.Bought1 = BoughtUga;
			GameData.Bought2 = BoughtPaulo;
			GameData.Bought3 = BoughtJanet;
			GameData.BG1_Purchase = BG1Change;
			GameData.BG2_Purchase = BG2Change;
			GameData.SprayBuy =SprayBuy ;
			Camera_Button.GetComponent<Button> ().enabled = true;
			StartCoroutine ("Delay");
		}else{
			GameData.Avatar_No = current_AvatarNo;
			if (GameData.Avatar_No  == AppOn_HairGame.CHARACTER_UGA) {
				BoughtUga = true;
			}
			else if (GameData.Avatar_No  == AppOn_HairGame.CHARACTER_PAULO) {
				BoughtPaulo = true;
			}
			else if (GameData.Avatar_No  ==AppOn_HairGame.CHARACTER_JANET) {
				BoughtJanet = true;
			}
			if(BoughtJanet)
				DollarImg_Merle.SetActive (false);
			if(BoughtPaulo)
				DollarImg_Paolo.SetActive (false);
			if(BoughtUga)
				DollarImg_UGA.SetActive (false);
			GameData.Bought1 = BoughtUga;
			GameData.Bought2 = BoughtPaulo;
			GameData.Bought3 = BoughtJanet;
			Camera_Button.GetComponent<Button> ().enabled = true;
		}
	
		writeData ();
//		bf.Serialize (file, data);
//		file.Close ();

	}
//	public void restoreBuyBg1(BuyData data)
//	{
//		data.Avatar_No = Avatar_No;
//		BG1Change = true;
//		data.BG1_Purchase = BG1Change;
//		if (BG1Change) {
//			BuySign.SetActive (false);
//		}
//		if (BG2Change) {
//			BuySign1.SetActive (false);
//		}
//	}

	public void BuyBG(int bgID)
	{
		restoreBuyBG(bgID);
		BG_Change.instance.BG_Button (bgID);
	}
	public void restoreBuyBG(int bgID)
	{
		switch(bgID)
		{
		case BG_Change.BG_GERMANY:
			GameData.BG2_Purchase = true;
			BG2Change = true;
			BuySign1.SetActive (false);

			break;
		case BG_Change.BG_JAPAN:
			GameData.BG3_Purchase = true;
			BuySign2.SetActive (false);
			break;
		case BG_Change.BG_UK:
			GameData.BG1_Purchase = true;
			BG1Change = true;
			BuySign.SetActive (false);
			break;
		case BG_Change.BG_USA:
			GameData.BG4_Purchase = true;
			BuySign3.SetActive (false);
			break;
		}
		Camera_Button.GetComponent<Button> ().enabled = true;
		writeData ();
//		BinaryFormatter bf = new BinaryFormatter ();
//		FileStream file = File.Create (path + "BuyInfo.dat");
//		bf.Serialize (file, GameData);
//		file.Close ();
	}
	void OnApplicationQuit()
	{
		timeRun++;

		PlayerPrefs.SetInt ("SavedFristRun", timeRun);
		PlayerPrefs.Save ();
	}

//	public void restoreBuyBg2(BuyData data)
//	{
//		data.Avatar_No = Avatar_No;
//		BG2Change = true;
//
//		data.BG2_Purchase = BG2Change;
//
//		if (BG1Change) {
//			BuySign.SetActive (false);
//		}
//		if (BG2Change) {
//			BuySign1.SetActive (false);
//		}
//	}

	public void Load(){
		if(File.Exists(path + "BuyInfo.dat")){
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (path + "BuyInfo.dat", FileMode.Open);
			GameData= (BuyData)bf.Deserialize (file);
//			Debug.Log (path);
			file.Close ();
			BoughtUga = GameData.Bought1;
			BoughtPaulo = GameData.Bought2;
			BoughtJanet = GameData.Bought3;
			SprayBuy = GameData.SprayBuy;
			BG1Change = GameData.BG1_Purchase;
			BG2Change = GameData.BG2_Purchase;
		
			if (BG1Change){
				BuySign.SetActive (false);
			}
			if (BG2Change){
				BuySign1.SetActive (false);
			}
			if ( GameData.BG3_Purchase){
				BuySign2.SetActive (false);
			}
			if (GameData.BG4_Purchase){
				BuySign3.SetActive (false);
			}
			if(BoughtJanet)
				DollarImg_Merle.SetActive (false);
			if(BoughtPaulo)
				DollarImg_Paolo.SetActive (false);
			if(BoughtUga)
				DollarImg_UGA.SetActive (false);
			HandleIAPButton ();
		}
		else{
			if (GameData == null) {
				GameData = new BuyData ();
			}
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Create (path + "BuyInfo.dat");
			bf.Serialize (file, GameData);
			file.Close ();
		}

		setPremiumBuild ();
	}

	public void setPremiumBuild()
	{
		if(IS_PREMIUM_BUILD)
		{
			
			Avatar_No = GameData.Avatar_No=2;
			BoughtUga = GameData.Bought1=true;
			BoughtPaulo = GameData.Bought2=true;
			BoughtJanet = GameData.Bought3=true;
			SprayBuy = GameData.SprayBuy=true;
			BG1Change = GameData.BG1_Purchase=true;
			BG2Change = GameData.BG2_Purchase=true;
			GameData.BG3_Purchase = true;
			GameData.BG4_Purchase = true;
			ResetAllLockedObject ();
		}
	}

	public void restoreCompletePackage()
	{
		GameData.Avatar_No = Avatar_No;
		BG1Change = true;
		BG2Change = true;
		BoughtUga = true;
		BoughtPaulo = true;
		BoughtJanet = true;
		SprayBuy = true;
		GameData.Bought1 = BoughtUga;
		GameData.Bought2 = BoughtPaulo;
		GameData.Bought3 = BoughtJanet;
		GameData.SprayBuy = SprayBuy;
		GameData.BG1_Purchase = BG1Change;
		GameData.BG2_Purchase = BG2Change;
		GameData.BG3_Purchase = true;
		GameData.BG4_Purchase = true;

		writeData ();
		if(SprayBuy){
			SprayButtons [0].SetActive (true);
			SprayButtons [1].SetActive (true);
			SprayButtons [2].SetActive (true);
			SprayButtons [3].SetActive (true);
		}
		ResetAllLockedObject ();
	}
	public void ResetAllLockedObject()
	{
		if (BG1Change) {
			BuySign.SetActive (false);
		}
		if (BG2Change) {
			BuySign1.SetActive (false);
		}
		if (GameData.BG3_Purchase) {
			BuySign2.SetActive (false);
		}
		if (GameData.BG4_Purchase) {
			BuySign3.SetActive (false);
		}
		if(BoughtJanet)
			DollarImg_Merle.SetActive (false);
		if(BoughtPaulo)
			DollarImg_Paolo.SetActive (false);
		if(BoughtUga)
			DollarImg_UGA.SetActive (false);
		HandleIAPButton ();
	}

	public void BuyCompletePackage(){
		GoogleAnalytics.Instance.LogCustomEvent ("CompletPackage", "CharacterBought");

		BG1Change = true;
		BG2Change = true;
		BoughtUga = true;
		BoughtPaulo = true;
		BoughtJanet = true;
		SprayBuy = true;
		SprayBuy = boughtCompletePackage;
		GameData.Avatar_No = Avatar_No;
		GameData.Bought1 = BoughtUga;
		GameData.Bought2 = BoughtPaulo;
		GameData.Bought3 = BoughtJanet;
		GameData.SprayBuy = SprayBuy;
		GameData.BG1_Purchase = BG1Change;
		GameData.BG2_Purchase = BG2Change;
		GameData.BG3_Purchase = true;
		GameData.BG4_Purchase = true;
	

		writeData ();
		if(SprayBuy){
			SprayButtons [0].SetActive (true);
			SprayButtons [1].SetActive (true);
			SprayButtons [2].SetActive (true);
			SprayButtons [3].SetActive (true);
		}
		ResetAllLockedObject ();
		enableDisableTopPanelButton (true);
		Camera_Button.GetComponent<Button> ().enabled = true;
		if (!MainMenu.activeSelf || FullPackage_From_MainMenu||FullPackage_Offer) {
			ShopClose_Button ();
		}
		else 
		{
//			StartCoroutine ("Delay");
			if (SprayBuy) {
				AppOn_HairGame.Instance.ResetDefaultAccessories ();
			}
			if (isfromGamePlay) {
				isfromGamePlay = false;
				SelectAvatar (AppOn_HairGame.CHARACTER_MERLE);
			}
		}
		Shop_Panel.SetActive (false);
		ShopPanel_Bool = false;
	}


	public bool forparent_bool;
	public void ForParent_Button(){
		if (splashAnimStart)
			return;
		AppOn.HairGame.AppOn_HairGame.Instance.Deselection ();
		ButtonClick_Audio.Play ();
		Debug.Log ("ForParent_Button ");
		forparent_bool = true;
		Avatar_Panel.SetActive (false);
		Locked_Button.SetActive (false);
		ForParent_Panel.SetActive (true);
		ForParent_Panel.GetComponentInChildren<ScrollRect> ().normalizedPosition = new Vector2(0,1);
		ForParentPanel_Bool = true;
		creditVersionText.text = "Version ["+Application.version+"]";
		MainMenu.SetActive (false);
		mainMenuControls.SetActive (false);
		MainMenu_Bool = false;
		Accessories_Panel.SetActive (false);
		DrawerConatainer.SetActive (false);
		btnOpenCam.SetActive (false);
		Shop_Panel.SetActive (false);
		ShopPanel_Bool = false;
	}

	public void Home_Button(){
		AppOn_HairGame.templateIndex = 0;
		canChangeTemplate = true;
		GameManager.instance.ButtonClick_Audio.Play ();
        MenuMainShow();
       
		SoundManager.instance.TurnOffSound ();
		AppOn.HairGame.AppOn_HairGame.Instance.Deselection ();
		AppOn.HairGame.AppOn_HairGame.Instance._cur_hair_style = "None";
		destroyAllAccesories ();
		Locked_Button.SetActive (false);
		HandleIAPButton ();
		Avatar_Panel.SetActive (false);

		resetTemplateSelection ();
//		Avatar_Panel.SetActive (false);
	}
	public void resetTemplateSelection()
	{
		AppOn_HairGame.isAnyChangeDoneInHairStyle = true;
		if(templateAnimation!=null){
			templateAnimation.SetBool ("selected", false);
		}
	}

	public void ForParent_MenuButton(){
		if (SoundManager.instance != null) {
			SoundManager.instance.TurnOffSound ();
		}
		if(questionaireForLink.gameObject.activeSelf||Oops_panel.activeSelf)
		{
			return;
		}
		GameManager.instance.ButtonClick_Audio.Play ();
		GameManager.instance.forparent_bool = false;
		ForParent_Panel.SetActive (false);
		ForParentPanel_Bool = false;
        MenuMainShow();
//		MainMenu.SetActive (true);	
//		mainMenuControls.SetActive (true);
//		MainMenu_Bool = true;
		Accessories_Panel.SetActive (true);
		DrawerConatainer.SetActive (true);
		btnOpenCam.SetActive (true);
//		Shop_Panel.SetActive (true );
	}


	public void Settings_Button(){
		if (splashAnimStart)
			return;
		GameManager.instance.ButtonClick_Audio.Play ();
		Settings_PopUp.SetActive (true);
	}

	public void Settings_CloseButton(){
		GameManager.instance.ButtonClick_Audio.Play ();
		Settings_PopUp.SetActive (false);
		Quit_PopUp.SetActive (false);
		_settingButton.GetComponent<Settings> ().SaveData ();
	}

	public void Quit_YesButton(){
		Application.Quit ();
	}

	public GameObject CamPanel;
	public GameObject CameraPanelUpperlayer;
	public GameObject BottomPanel;
	public GameObject btnOpenCam;
	public Transform avtar;
	void onPermit()
	{
        
	}

	void notPermit()
	{
		
	}
	void getPermissionForExternalStorage()
	{
		#if UNITY_ANDROID
		if(UniAndroidPermission.IsPermitted (AndroidPermission.WRITE_EXTERNAL_STORAGE)){
            
		}else{
			UniAndroidPermission.RequestPremission (AndroidPermission.WRITE_EXTERNAL_STORAGE,onPermit,notPermit);

		}
		#endif
	}
	public  void OpenCam(){
		#if UNITY_ANDROID
		Debug.Log ("hi CAMERA permission gets called");
        Debug.Log ("==================  "+UniAndroidPermission.IsPermitted (AndroidPermission.CAMERA));
		if(UniAndroidPermission.IsPermitted (AndroidPermission.CAMERA)){
            
		}else{
			UniAndroidPermission.RequestPremission (AndroidPermission.CAMERA,onPermit,notPermit);

		}
		Invoke("getPermissionForExternalStorage",0.1f);
		#endif
		
        if(PhoneCamera.Instance!=null){
            PhoneCamera.Instance.StartCamera();
        }
        GameManager.instance.ButtonClick_Audio.Play ();
        AppOn.HairGame.AppOn_HairGame.Instance.disableDrierBlowAnim ();
        AppOn.HairGame.AppOn_HairGame.Instance.disableShower ();
        AppOn.HairGame.AppOn_HairGame.Instance.stopShower ();
        CamPanel.SetActive(true);
        btnOpenCam.SetActive(false);
        DrawerConatainer.SetActive(false);
        CameraPanelUpperlayer.SetActive(true);
        switch(Avatar_No){
            case AppOn_HairGame.CHARACTER_JANET:
                avtar.localScale=new Vector3(0.37f,0.37f,0.37f);
                avtar.transform.position=new Vector3(avtar.transform.position.x-100,avtar.transform.position.y+42,avtar.transform.position.z);//avtar.transform.position.y+20
                break;
            case AppOn_HairGame.CHARACTER_MERLE:
                avtar.localScale=new Vector3(0.25f,0.25f,0.25f);
                avtar.transform.position=new Vector3(avtar.transform.position.x-100,avtar.transform.position.y+35,avtar.transform.position.z);
                break;
            case AppOn_HairGame.CHARACTER_PAULO:
                avtar.localScale=new Vector3(0.35f,0.35f,0.35f);
                avtar.transform.position=new Vector3(avtar.transform.position.x-85,avtar.transform.position.y+10,avtar.transform.position.z);
                break;
            case AppOn_HairGame.CHARACTER_UGA:
                avtar.localScale=new Vector3(0.25f,0.25f,0.25f);
                avtar.transform.position=new Vector3(avtar.transform.position.x-100,avtar.transform.position.y+80,avtar.transform.position.z);
                break;
        }

	}




	public void PrivacyPolicy(){
		LinkIndex = 0;
		questionaireForLink.SelectQuestion ();
	}
	public void TermsAndCondition(){
		LinkIndex = 1;
		questionaireForLink.SelectQuestion ();
	}
	public void AppOnLogo(){
		LinkIndex = 2;
		questionaireForLink.SelectQuestion ();
	}
	public void KinsaneLogo(){
		LinkIndex = 3;
		questionaireForLink.SelectQuestion ();
	}
	public void SFXText(){
		LinkIndex = 4;
		questionaireForLink.SelectQuestion ();
	}

	public static int LinkIndex=-1;
	public void MoveToURL()
	{
		switch(LinkIndex)
		{
		case 0:
			Application.OpenURL("https://kinsane.com/privacy-policy");
			LinkIndex = -1;
			break;
		case 1:
			Application.OpenURL("https://kinsane.com/terms-of-use");
			LinkIndex = -1;
			break;
		case 2:
			Application.OpenURL("http://www.appon.co.in/");
			LinkIndex = -1;
			break;
		case 3:
			Application.OpenURL("https://kinsane.com/");
			LinkIndex = -1;
			break;
		case 4:
			Application.OpenURL("https://www.zapsplat.com");
			LinkIndex = -1;
			break;
		}
	}
	public GameObject rateUs;
	public static bool isShowRateUsPopup;
	public void ShowRateUsPopup()
	{
		if(Input.GetMouseButtonUp(0))
			ButtonClick_Audio.Play ();
//		i = 1;

//		Accessories_Panel.SetActive (false);
//		Locked_Button.SetActive (false);
//		DrawerConatainer.SetActive (false);
//		btnOpenCam.SetActive (false);
		rateUs.SetActive (true);
	}

	public void closeRateUsPopup()
	{
		StartCoroutine ("Delay");

		AppOn.HairGame.AppOn_HairGame.Instance._cur_hair_style = "None";
		ButtonClick_Audio.Play ();
		Oops_panel.SetActive (false);

		BlackAlpha.SetActive (false);
		Camera_Button.GetComponent<Button> ().enabled = true;
		DrawerConatainer.SetActive (true);
		btnOpenCam.SetActive (true);
		Accessories_Panel.SetActive (true);
		Avatar_Panel.SetActive (true);
		if (isfromGamePlay) {
			isfromGamePlay = false;
			Debug.Log (" hi from gameplay");
		} else {
			if ((Avatar_No == AppOn_HairGame.CHARACTER_UGA && !BoughtUga)
				|| (Avatar_No == AppOn_HairGame.CHARACTER_PAULO && !BoughtPaulo)
				|| (Avatar_No == AppOn_HairGame.CHARACTER_JANET && !BoughtJanet) ||
				Avatar_No == AppOn_HairGame.CHARACTER_MERLE) {
				Show_Panels (false);
			}
			Debug.Log (" hi not from gameplay");
			AppOn.HairGame.AppOn_HairGame.Instance.LateStart ();
			hairStyle ();
		}
	}

	public void HandleIAPButton()
	{
//		ShopButton_UI.SetActive (false);
		SaleOfferButton.SetActive (false);
		if(SprayBuy||IS_PREMIUM_BUILD)
		{
			SaleOfferButton.SetActive (false);
			SaleOfferPopup.SetActive (false);
			SaleOffer.IS_SHOW_SALE_OFFER=false;
			buyFullVersionButton.SetActive (false);
//			ShopButton_UI.SetActive (false);
		}
		else{
			buyFullVersionButton.SetActive (true);
			if(!FTUEHandler.isAllFTUEComplete()){
				return;
			}
			SaleOffer.load ();
			if(!SaleOffer.IS_SHOW_SALE_OFFER)
			{
				SaleOfferButton.SetActive (false);
			}
			else{
				SaleOfferButton.SetActive (true);
			}
		}
	}

	public void OpenSaleOfferPopup()
	{
		if (splashAnimStart)
			return;
		GameManager.instance.ButtonClick_Audio.Play ();
		SaleOfferPopup.SetActive (true);
		isFromFTUE = false;

	}
	public void BuyFullVersion()
	{
		if (splashAnimStart)
			return;
		FullPackage_From_MainMenu = true;
		questionaireForLink.SelectQuestion ();
	}
	public void updateGameLaunchCounter()
	{
//		currentGameLaunchCounter++;
//		PlayerPrefs.SetInt ("currentGameLaunchCounter", currentGameLaunchCounter);
//		PlayerPrefs.Save ();
	}

    public void AdsRemove()
    {
        if (IS_ADS_ENABLE)
        {
            questionaireForLink.SelectQuestion();
            removeAds = true;
        }
    }

    public void OnSuccessRemoveAds()
    {
        IS_ADS_ENABLE = false;
        PlayerPrefs.SetInt(RMS_ADS_REMOVE, 1);
        BtnRemoveAds.SetActive(false);
    }

    public void MenuMainShow()
    {
        #if ADS
        if (IS_ADS_ENABLE) {
            AdsManager.Insatnce.Show ();
        }
        #endif
        MainMenu.SetActive (true);
        MainMenu_Bool = true;
        mainMenuControls.SetActive (true); 
    }

	public void LoadNextScreen()
	{
		if (canChangeTemplate) {
			DragBottomPanel.Instance.init ();
		}

		LoadingScreen.SetActive (false);
		DrawerConatainer.SetActive (true);
		btnOpenCam.SetActive (true);
		Accessories_Panel.SetActive (true);
		Avatar_Panel.SetActive (true);
		Avatar_Panel.SetActive (true);
		if ( !_isFrom_MainMenuFTUE&&FTUEHandler.isAllFTUEComplete() && isShowSaleOfferPopupFTUE && isShowRateUsPopup )
		{
			rateUsPopUpCounter++;
			PlayerPrefs.SetInt ("rateUsPopUpCounter", rateUsPopUpCounter);
			isShowRateUsPopup = false;
			LoadNextScreen ();
			ShowRateUsPopup ();
		}
	}
}

[Serializable]
public class BuyData{
	public int Avatar_No;
	public bool Bought1;
	public bool Bought2;
	public bool Bought3;
	public bool SprayBuy;
	public bool BG1_Purchase;
	public bool BG2_Purchase;
	public bool BG3_Purchase;
	public bool BG4_Purchase;
}