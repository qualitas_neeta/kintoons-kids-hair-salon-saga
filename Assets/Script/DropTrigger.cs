﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropTrigger : MonoBehaviour {

	public static  DropTrigger Instance;

	public DragDrop script;


	void Start(){
		Instance = this;
		if (script != null) {
			script.NotInPosition ();
		}
	}

	public void OnTriggerEnter(){
//		Debug.Log("Enter");
		if (script != null) {
			script.IsInPosition ();
		}
	
	}

//	public void OnTriggerStay(){
////		script.IsInPosition ();
////		soundPosCheck = true;
//
//	}

	public void OnTriggerExit(){
//		Debug.Log("Exit");
		if (script != null) {
			script.NotInPosition ();
		}
	}
}
