﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
public class CapturePhoto : MonoBehaviour {
	string imagename="SomeLevel{0}";
	public GameObject ArCamera;
	public GameObject OpenCaptureShotView;
	public GameObject CloseBGView;
	Texture2D screenCap;
	float width,height;
	// Use this for initialization
	void Start () {
//		screenCap = new Texture2D (((Screen.width)/2) + ((Screen.width)/4)  , ((Screen.height)/2) + ((Screen.height)/4), TextureFormat.RGBA32, false);
		width=(Screen.width/1.7f);
		height= ((Screen.height)/1.7f);
		screenCap = new Texture2D ((int)width  , (int)height, TextureFormat.RGBA32, false);
	}

public 	void OpenCamera(){
		ArCamera.SetActive (true);
		OpenCaptureShotView.SetActive (true);
		CloseBGView.SetActive (false);
	}

public 	void CloseCamera(){
		ArCamera.SetActive (false);
		OpenCaptureShotView.SetActive (false);
		CloseBGView.SetActive (true);
	}

	IEnumerator Capture(){
		
		yield return new WaitForEndOfFrame ();
		//		StartCoroutine ("Delay");
		screenCap.ReadPixels (new Rect ( (Screen.width)/4.4f ,(Screen.height)/4.4f,width, height), 0, 0);
		Color32 []color = screenCap.GetPixels32 ();
		for(int i =0 ;i< color.Length;i++)
		{
			color[i] = new Color32(color[i].r,color[i].g,color[i].b,255);
		}
		screenCap.SetPixels32 (color);
		screenCap.Apply();

		NativeGallery.SaveToGallery (screenCap,"Hair saloon", "screenShot_HairSallon.png");
		
//		#if UNITY_ANDROID && !UNITY_EDITOR
//		if(UniAndroidPermission.IsPermitted (AndroidPermission.WRITE_EXTERNAL_STORAGE)){
//			NativeGallery.SaveToGallery (screenCap,"Hair saloon", "screenShot_HairSallon.png");
//		}else{
//			UniAndroidPermission.RequestPremission (AndroidPermission.WRITE_EXTERNAL_STORAGE, () => {
//				// add permit action
//				NativeGallery.SaveToGallery (screenCap,"Hair saloon", "screenShot_HairSallon.png");
//			}, () => {
//				// add not permit action
//			});
//		}
//		
//		
//		#elif UNITY_EDITOR
//		
//		var bytes = screenCap.EncodeToJPG();
//		{
//			string Screen_Shot_File_Name = path;
//			do {
//				Screenshot_Count++;
//				Screen_Shot_File_Name = "Screenshot_" + Screenshot_Count + ".jpg";
//				
//			} while (System.IO.File.Exists (Screen_Shot_File_Name));
//			System.IO.File.WriteAllBytes (Screen_Shot_File_Name, bytes);
//			
//			
//			
//		}
//		#endif
		
		
		
	}

	public void TakeShot(){

//		Debug.Log (Application.persistentDataPath +":"+imagename);
//		ScreenCapture.CaptureScreenshot(imagename+".png");


		StartCoroutine( Capture());

		if(File.Exists (Application.persistentDataPath+Path.DirectorySeparatorChar+imagename+".png"))
		
		{
			Debug.Log ("File exits unity");
			NativeGallery.SaveToGallery (Application.persistentDataPath+Path.DirectorySeparatorChar+imagename+".png",Application.productName,".png",true);
		}
	}
}
