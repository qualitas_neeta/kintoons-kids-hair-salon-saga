﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RateUsPopup : MonoBehaviour {

	public Sprite filledStar;
	public Sprite emptyStar;
	private static int totalStarCount = 0;
	private bool[] isRateStarArray;
	public Image[] rateUsStarArray;
	public GameObject rateUSPopup;
	public GameObject feedbackText;
	// Use this for initialization
	void Start () {
		totalStarCount = 0;
		if (isRateStarArray == null) {
			isRateStarArray = new bool[rateUsStarArray.Length];
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void CancleRating()
	{
		if(feedbackGiven)
		{
			return;
		}
		if( totalStarCount!=0)
		{
			StartCoroutine ("rateTheGame");
		}
		else{
			GameManager.instance.rateUs.SetActive (false);
//			GameManager.instance.closeRateUsPopup ();
		}
	}
	IEnumerator MoveToGamePlay()
	{
		yield return new WaitForSeconds (0.3f);
		feedbackText.SetActive (false);
		GameManager.instance.rateUs.SetActive (false);
//		GameManager.instance.closeRateUsPopup ();
	}
	public void CalculateRating(int index)
	{
		if(feedbackGiven)
		{
			return;
		}
		isRateStarArray [index] = !isRateStarArray [index];
		if(isRateStarArray[index])
		{
			rateUsStarArray [index].sprite = filledStar;
			totalStarCount++;
		}
		else{
			rateUsStarArray [index].sprite = emptyStar;
			totalStarCount--;
		}
		if (!isRateStarArray [index]) {
			for (int i = index+1; i < isRateStarArray.Length; i++) {
				if (isRateStarArray [i]) {
					isRateStarArray [i] = !isRateStarArray [i];
					rateUsStarArray [i].sprite = emptyStar;
					totalStarCount--;
				}
			}
		}
		else if(index!=0 && isRateStarArray [index]){
			for (int i = 0; i < index; i++) {
				if (!isRateStarArray [i]) {
					isRateStarArray [i] = !isRateStarArray [i];
					rateUsStarArray [i].sprite = filledStar;
					totalStarCount++;
				}
			}
		}
		StartCoroutine ("rateTheGame");
	}
	static bool feedbackGiven=false;
	IEnumerator rateTheGame()
	{
		feedbackGiven=true;
		yield return new WaitForSeconds (1f);
		if(totalStarCount!=0 && totalStarCount<=3)
		{
			GameManager.isShowRateUsPopup = false;
			PlayerPrefs.SetString("isShowRateUsPopup","false");
			rateUSPopup.SetActive (false);
			feedbackText.SetActive (true);
			StartCoroutine ("MoveToGamePlay");
		}
		else if(totalStarCount>=4)
		{
			GameManager.isShowRateUsPopup = false;
			PlayerPrefs.SetString("isShowRateUsPopup","false");
			BG_Change.instance.BGChange_Audio.Stop ();
			GameManager.instance.rateUs.SetActive (false);
//			GameManager.instance.closeRateUsPopup ();
//			StartCoroutine ("GoToUrl");
			#if UNITY_ANDROID
			Application.OpenURL("market://details?id=com.Kinsane.KinToons_HairSalon2");
			#elif UNITY_IPHONE//
			Application.OpenURL("itms-apps://itunes.apple.com/app/id1371369068");//hairsalonsaga=1371369068//=== nursary rhyem dj 1328793608
			#endif
		}
	}

}
