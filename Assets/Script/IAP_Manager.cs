﻿#if UNITY_ANDROID||UNITY_IOS
#if IAP
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using System.Collections;



// Placing the Purchaser class in the CompleteProject namespace allows it to interact with ScoreManager,
// one of the existing Survival Shooter scripts.
// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
public class IAP_Manager : MonoBehaviour, IStoreListener
{
	public static IAP_Manager instance;

	private static IStoreController m_StoreController;
	// The Unity Purchasing system.
	private static IExtensionProvider m_StoreExtensionProvider;
	// The store-specific Purchasing subsystems.

	// Product identifiers for all products capable of being purchased:
	// "convenience" general identifiers for use with Purchasing, and their store-specific identifier
	// counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers
	// also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

	// General product identifiers for the consumable, non-consumable, and subscription products.
	// Use these handles in the code to reference which product to purchase. Also use these values
	// when defining the Product Identifiers on the store. Except, for illustration purposes, the
	// kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
	// specific mapping to Unity Purchasing's AddProduct, below.
	//	public static string Product_CompletePackage = "android.test.purchased";
#if UNITY_ANDROID||UNITY_EDITOR
	public const string Product_CompletePackage = "kintoons_hairsalon_completepack";
	public const string Buy_Complete_Package_In_Offer = "kintoons_hairsalon_completepack_discount";
    public const string removeAds = "com.hairsalonsaga.removeads";
#elif UNITY_IOS
public const string Product_CompletePackage = "kintoon.hairsalonsaga.completepack";
public const string Buy_Complete_Package_In_Offer = "kintoon.hairsalonsaga.completepack.discount";
#endif
	//	public const string Janet_Buy = "kintoons_hairsalon_character_01_merle";
//	public const string Janet_Buy = "kintoons_hairsalon_character_04_janet";
//
//	public const string Paolo_Buy = "kintoons_hairsalon_character_02_paolo";
//	public const string Uga_Buy = "kintoons_hairsalon_character_03_uga";
//	public const string AllCharacters_Buy = "kintoons_hairsalon_character_04_all";
//	public const string BG1_Buy = "kintoons_hairsalon_background2";
//	public const string BG2_Buy = "kintoons_hairsalon_background3";
//	public const string BG3_Buy = "kintoons_hairsalon_background4";
//	public const string BG4_Buy = "kintoons_hairsalon_background5";
	// Apple App Store-specific product identifier for the subscription product.
	//	private static string kProductNameAppleSubscription =  "com.unity3d.subscription.new";

	// Google Play Store-specific product identifier subscription product.
	//	private static string kProductNameGooglePlaySubscription =  "com.unity3d.subscription.original";

	void Awake ()
	{
		instance = this;
	}

	Dictionary<string ,string> localisedPrices = new Dictionary<string, string> ();

	void Start ()
	{
		// If we haven't set up the Unity Purchasing reference
		GameManager.instance.Load ();
		if (m_StoreController == null) {
			// Begin to configure our connection to Purchasing
			InitializePurchasing ();
		}
	}


	public string getProductPrice (string id)
	{
		switch (id) {
		case Product_CompletePackage:
			string value = "$ 4.99";
			localisedPrices.TryGetValue (id, out value);
			if (value == null || value.Equals ("")) {
				#if UNITY_ANDROID
					value = "$ 4.99";
				#elif UNITY_IOS
					value = "$ 2.99";
				#endif
			}
			return value;
            case removeAds:
                value = "$ 0.99";
                localisedPrices.TryGetValue (id, out value);
                if (value == null || value.Equals ("")) {
                 #if UNITY_ANDROID
                    value = "$ 0.99";
                    #elif UNITY_IOS
                    value = "$ 1.99";
                    #endif

                }
                break;
		case Buy_Complete_Package_In_Offer:
			value = "$ 3.99";
			localisedPrices.TryGetValue (id, out value);
			if (value == null || value.Equals ("")) {
				#if UNITY_ANDROID
					value = "$ 3.99";
				#elif UNITY_IOS
					value = "$ 1.99";
				#endif
				
			}
			return value;
		}
		return "$ 0.99";
	}

	void checkProduct ()
	{
		localisedPrices.Clear ();
		foreach (Product product in m_StoreController.products.all) {
			localisedPrices.Add (product.definition.id, product.metadata.localizedPriceString);
//			Debug.Log ("product.transactionID : " + product.definition.id + " localizedPrice :" + product.metadata.localizedPriceString);
			if (product != null && product.hasReceipt) {
				if (product.definition.id == Product_CompletePackage||
					product.definition.id == Buy_Complete_Package_In_Offer) {
					
					Debug.Log ("Complete_Bought");
					GameManager.instance.restoreCompletePackage ();
				}
                if (product.definition.id ==removeAds) {

                    Debug.Log ("remove ads restore");
                    GameManager.instance.OnSuccessRemoveAds ();
                }
//				else if (product.definition.id == Janet_Buy) {
//					Debug.Log ("Janet_Bought");
//					GameManager.instance.restoreBuyChar (AppOn.HairGame.AppOn_HairGame.CHARACTER_JANET);
//				} else if (product.definition.id == Uga_Buy) {
//					Debug.Log ("Uga_Bought");
//					GameManager.instance.restoreBuyChar (AppOn.HairGame.AppOn_HairGame.CHARACTER_UGA);
//
//				} else if (product.definition.id == Paolo_Buy) {
//					Debug.Log ("Paulo_Bought");
//					GameManager.instance.restoreBuyChar (AppOn.HairGame.AppOn_HairGame.CHARACTER_PAULO);
//				} else if (product.definition.id == BG1_Buy) {
//					Debug.Log ("BG1_Bought");
//					GameManager.instance.restoreBuyBG (BG_Change.BG_UK);
//				
//				} else if (product.definition.id == BG2_Buy) {
//					Debug.Log ("BG2_Bought");
//					GameManager.instance.restoreBuyBG (BG_Change.BG_GERMANY);
//				} else if (product.definition.id == BG3_Buy) {
//					Debug.Log ("BG2_Bought");
//					GameManager.instance.restoreBuyBG (BG_Change.BG_JAPAN);
//				} else if (product.definition.id == BG4_Buy) {
//					Debug.Log ("BG2_Bought");
//					GameManager.instance.restoreBuyBG (BG_Change.BG_USA);
//				}

			}

		}

	}

	public void InitializePurchasing ()
	{
		// If we have already connected to Purchasing ...
		if (IsInitialized ()) {
			// ... we are done here.
			return;
		}

		// Create a builder, first passing in a suite of Unity provided stores.
		var builder = ConfigurationBuilder.Instance (StandardPurchasingModule.Instance ());

		// Add a product to sell / restore by way of its identifier, associating the general identifier
		// with its store-specific identifiers.
//		builder.AddProduct(kProductIDConsumable, ProductType.Consumable);
		// Continue adding the non-consumable product.
		builder.AddProduct (Product_CompletePackage, ProductType.NonConsumable);
		builder.AddProduct (Buy_Complete_Package_In_Offer, ProductType.NonConsumable);
        builder.AddProduct (removeAds, ProductType.NonConsumable);
//		builder.AddProduct (Janet_Buy, ProductType.NonConsumable);
//		builder.AddProduct (Paolo_Buy, ProductType.NonConsumable);
//		builder.AddProduct (Uga_Buy, ProductType.NonConsumable);
//		builder.AddProduct (AllCharacters_Buy, ProductType.NonConsumable);
//		builder.AddProduct (BG1_Buy, ProductType.NonConsumable);
//		builder.AddProduct (BG2_Buy, ProductType.NonConsumable);
//		builder.AddProduct (BG3_Buy, ProductType.NonConsumable);
//		builder.AddProduct (BG4_Buy, ProductType.NonConsumable);
		// And finish adding the subscription product. Notice this uses store-specific IDs, illustrating
		// if the Product ID was configured differently between Apple and Google stores. Also note that
		// one uses the general kProductIDSubscription handle inside the game - the store-specific IDs 
		// must only be referenced here. 
//		builder.AddProduct(kProductIDSubscription, ProductType.Subscription, new IDs(){
//			{ kProductNameAppleSubscription, AppleAppStore.Name },
//			{ kProductNameGooglePlaySubscription, GooglePlay.Name },
//		});

		// Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
		// and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
		UnityPurchasing.Initialize (this, builder);
	}


	private bool IsInitialized ()
	{
		// Only say we are initialized if both the Purchasing references are set.

		return m_StoreController != null && m_StoreExtensionProvider != null;
	}


	public void BuyConsumable ()
	{
		// Buy the consumable product using its general identifier. Expect a response either 
		// through ProcessPurchase or OnPurchaseFailed asynchronously.
//		BuyProductID(kProductIDConsumable);
	}


	public void BuyProduct_CompletePackage ()
	{
		// Buy the non-consumable product using its general identifier. Expect a response either 
		// through ProcessPurchase or OnPurchaseFailed asynchronously.
		BuyProductID (Product_CompletePackage);
	}

	public void BuyProduct_Janet ()
	{
		// Buy the non-consumable product using its general identifier. Expect a response either 
		// through ProcessPurchase or OnPurchaseFailed asynchronously.
//		BuyProductID (Janet_Buy);
	}

	public void BuyProduct_Uga ()
	{
		// Buy the non-consumable product using its general identifier. Expect a response either 
		// through ProcessPurchase or OnPurchaseFailed asynchronously.
//		BuyProductID (Uga_Buy);
	}

	public void BuyProduct_Paulo ()
	{
		// Buy the non-consumable product using its general identifier. Expect a response either 
		// through ProcessPurchase or OnPurchaseFailed asynchronously.
//		BuyProductID (Paolo_Buy);
	}

	public void BuyProduct_AllCharacter ()
	{
		// Buy the non-consumable product using its general identifier. Expect a response either 
		// through ProcessPurchase or OnPurchaseFailed asynchronously.
//		BuyProductID (AllCharacters_Buy);
	}

	//	public void BuyProduct_BG1()
	//	{
	//		// Buy the non-consumable product using its general identifier. Expect a response either
	//		// through ProcessPurchase or OnPurchaseFailed asynchronously.
	//		BuyProductID(BG1_Buy);
	//	}
	//	public void BuyProduct_BG2()
	//	{
	//		// Buy the non-consumable product using its general identifier. Expect a response either
	//		// through ProcessPurchase or OnPurchaseFailed asynchronously.
	//		BuyProductID(BG2_Buy);
	//	}


	public void BuySubscription ()
	{
		// Buy the subscription product using its the general identifier. Expect a response either 
		// through ProcessPurchase or OnPurchaseFailed asynchronously.
		// Notice how we use the general product identifier in spite of this ID being mapped to
		// custom store-specific identifiers above.
//		BuyProductID(kProductIDSubscription);
	}

	private bool isPurches=false;
	public void BuyProductID (string productId)
	{
		// If Purchasing has been initialized ...
		if (IsInitialized ()) {
			isPurches=true;
			// ... look up the Product reference with the general product identifier and the Purchasing 
			// system's products collection.
			Product product = m_StoreController.products.WithID (productId);

			// If the look up found a product for this device's store and that product is ready to be sold ... 
			if (product != null && product.availableToPurchase) {
				Debug.Log (string.Format (" BuyProductID Purchasing product asychronously: '{0}'", product.definition.id));
				// ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
				// asynchronously.
				m_StoreController.InitiatePurchase (product);
			}
			// Otherwise ...
			else {
				// ... report the product look-up failure situation  
				Debug.Log (" BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
			}
		}
		// Otherwise ...
		else {
			// ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
			// retrying initiailization.
			Debug.Log (" BuyProductID FAIL. Not initialized.");
		}
	}


	// Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google.
	// Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
	public void RestorePurchases ()
	{
		// If Purchasing has not yet been set up ...
		if (!IsInitialized ()) {
			// ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
			Debug.Log ("RestorePurchases FAIL. Not initialized.");
			return;
		}

		// If we are running on an Apple device ... 
		if (Application.platform == RuntimePlatform.IPhonePlayer ||
		    Application.platform == RuntimePlatform.OSXPlayer) {
			// ... begin restoring purchases
			Debug.Log ("RestorePurchases started ...");

			// Fetch the Apple store-specific subsystem.
			var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions> ();
			// Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
			// the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
			apple.RestoreTransactions ((result) => {
				// The first phase of restoration. If no more responses are received on ProcessPurchase then 
				// no purchases are available to be restored.
				if (result) {
					checkProduct ();
				}
				Debug.Log ("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
			});
		}
		// Otherwise ...
		else {
			// We are not running on an Apple device. No work is necessary to restore purchases.
			Debug.Log ("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
		}
	}


	//
	// --- IStoreListener
	//

	public void OnInitialized (IStoreController controller, IExtensionProvider extensions)
	{
		// Purchasing has succeeded initializing. Collect our Purchasing references.
		Debug.Log (" OnInitialized: PASS");

		// Overall Purchasing system, configured with products for this application.
		m_StoreController = controller;
		// Store specific subsystem, for accessing device-specific store features.
		m_StoreExtensionProvider = extensions;
		#if UNITY_ANDROID
		checkProduct ();
		#elif UNITY_IOS
		localisedPrices.Clear ();
		foreach (Product product in m_StoreController.products.all) {

		localisedPrices.Add (product.definition.id, product.metadata.localizedPriceString);
		}
//		RestorePurchases();
		#endif

	}


	public void OnInitializeFailed (InitializationFailureReason error)
	{
		// Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
		Debug.Log (" OnInitializeFailed InitializationFailureReason:" + error);
	}


	public PurchaseProcessingResult ProcessPurchase (PurchaseEventArgs args)
	{
		bool isTrue = false;
		// A consumable product has been purchased by this user.
//		if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable, StringComparison.Ordinal))
//		{
//			Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
//			// The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
//		}
		// Or ... a non-consumable product has been purchased by this user.
		if (String.Equals (args.purchasedProduct.definition.id, Product_CompletePackage, StringComparison.Ordinal)||
			String.Equals (args.purchasedProduct.definition.id, Buy_Complete_Package_In_Offer, StringComparison.Ordinal) ) {
			Debug.Log (string.Format (" ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			GameManager.instance.BuyCompletePackage ();
			isTrue = true;
			// TODO: The non-consumable item has been successfully purchased, grant this item to the player.
		}
        else if (String.Equals (args.purchasedProduct.definition.id, removeAds, StringComparison.Ordinal)) {
            Debug.Log (string.Format (" ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            GameManager.instance.OnSuccessRemoveAds();
            isTrue = true;
            // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
        }
//		else if (String.Equals (args.purchasedProduct.definition.id, Janet_Buy, StringComparison.Ordinal)) {
//			Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
//			GameManager.instance.BuyCharacter (GameManager.Avatar_No);
//			isTrue = true;
//			// TODO: The non-consumable item has been successfully purchased, grant this item to the player.
//		} else if (String.Equals (args.purchasedProduct.definition.id, Paolo_Buy, StringComparison.Ordinal)) {
//			Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
//			GameManager.instance.BuyCharacter (GameManager.Avatar_No);
//			isTrue = true;
//			// TODO: The non-consumable item has been successfully purchased, grant this item to the player.
//		} else if (String.Equals (args.purchasedProduct.definition.id, Uga_Buy, StringComparison.Ordinal)) {
//			Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
//			GameManager.instance.BuyCharacter (GameManager.Avatar_No);
//			isTrue = true;
//			// TODO: The non-consumable item has been successfully purchased, grant this item to the player.
//		} else if (String.Equals (args.purchasedProduct.definition.id, AllCharacters_Buy, StringComparison.Ordinal)) {
//			Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
//			GameManager.instance.BuyCharacter (0);
//			isTrue = true;
//			// TODO: The non-consumable item has been successfully purchased, grant this item to the player.
//		} else if (String.Equals (args.purchasedProduct.definition.id, BG1_Buy, StringComparison.Ordinal)) {
//			Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
//			GameManager.instance.BuyBG (BG_Change.BG_UK);
//			isTrue = true;
//			// TODO: The non-consumable item has been successfully purchased, grant this item to the player.
//		} else if (String.Equals (args.purchasedProduct.definition.id, BG2_Buy, StringComparison.Ordinal)) {
//			Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
//			GameManager.instance.BuyBG (BG_Change.BG_GERMANY);
//			isTrue = true;
//			// TODO: The non-consumable item has been successfully purchased, grant this item to the player.
//		} else if (String.Equals (args.purchasedProduct.definition.id, BG3_Buy, StringComparison.Ordinal)) {
//			Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
//			GameManager.instance.BuyBG (BG_Change.BG_JAPAN);
//			isTrue = true;
//			// TODO: The non-consumable item has been successfully purchased, grant this item to the player.
//		} else if (String.Equals (args.purchasedProduct.definition.id, BG4_Buy, StringComparison.Ordinal)) {
//			Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
//			GameManager.instance.BuyBG (BG_Change.BG_USA);
//			isTrue = true;
//			// TODO: The non-consumable item has been successfully purchased, grant this item to the player.
//		}
		// Or ... a subscription product has been purchased by this user.
//		else if (String.Equals(args.purchasedProduct.definition.id, kProductIDSubscription, StringComparison.Ordinal))
//		{
//			Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
//			// TODO: The subscription item has been successfully purchased, grant this to the player.
//		}
		// Or ... an unknown product has been purchased by this user. Fill in additional products here....
		else {
//			Debug.Log (string.Format (" ProcessPurchase: FAIL. Unrecognized product: '{0},{1}'", args.purchasedProduct.definition.id, args.purchasedProduct.hasReceipt));
			isTrue = false;
		}

		if (isTrue && isPurches){
			Firebase.Analytics.FirebaseAnalytics.LogEvent (Firebase.Analytics.FirebaseAnalytics.EventEcommercePurchase);
		}
		isPurches=false;

		// Return a flag indicating whether this product has completely been received, or if the application needs 
		// to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
		// saving purchased products to the cloud, and when that save is delayed. 
		return PurchaseProcessingResult.Complete;
	}


	public void OnPurchaseFailed (Product product, PurchaseFailureReason failureReason)
	{
		// A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
		// this reason with the user to guide their troubleshooting actions.
		Debug.Log (string.Format (" OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}, PurchaseFailureReason: {2}", product.definition.storeSpecificId, failureReason, product.hasReceipt));
	}
}
#endif
#endif