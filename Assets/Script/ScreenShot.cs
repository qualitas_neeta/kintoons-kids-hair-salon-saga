﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
using AppOn.HairGame;


public class ScreenShot : MonoBehaviour {

	public static ScreenShot instance;

	Texture2D screenCap;
	public static bool shot = false;
	static int Screenshot_Count ;
	public Image Screenshot_Preview;
	public Animator ScreenshotPreview_Anim;
	public Animator ScreenshotBlink_Anim;
	public AudioSource TakeShot_Audio;


	// Use this for initialization
	void Start () {

		#if UNITY_EDITOR
			path = "";
		#elif UNITY_ANDROID
			path = Application.persistentDataPath +@"/";
		#elif UNITY_IOS
			path = Application.persistentDataPath +"/";
		#endif

		screenCap = new Texture2D (((Screen.width)/2) + ((Screen.width)/4)  , ((Screen.height)/2) + ((Screen.height)/4), TextureFormat.RGBA32, false);
		Screenshot_Preview.enabled = false;
		instance = this;
		shot = false;
        if (PlayerPrefs.HasKey("Screenshot_Count"))
            Screenshot_Count=PlayerPrefs.GetInt("Screenshot_Count");
        else
            Screenshot_Count = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if(Settings.instance !=null)
			TakeShot_Audio.volume = (Settings.instance.sfx_slider.value * 0.2f);
	}


	public static string path = "";

	IEnumerator Capture(){
		
		yield return new WaitForEndOfFrame ();
//		StartCoroutine ("Delay");
		screenCap.ReadPixels (new Rect ( (Screen.width)/8 ,(Screen.height)/8,((Screen.width)/2) + ((Screen.width)/4)  , ((Screen.height)/2) + ((Screen.height)/4) ), 0, 0);
		Color32 []color = screenCap.GetPixels32 ();
		for(int i =0 ;i< color.Length;i++)
		{
			color[i] = new Color32(color[i].r,color[i].g,color[i].b,255);
		}
		screenCap.SetPixels32 (color);
		screenCap.Apply();
		shot = true;
		screenCap.Apply ();

		#if UNITY_ANDROID && !UNITY_EDITOR
		if(UniAndroidPermission.IsPermitted (AndroidPermission.WRITE_EXTERNAL_STORAGE)){
        NativeGallery.SaveToGallery (screenCap,"Hair saloon", "screenShot_HairSallon"+Screenshot_Count+".png");
		}else{
		UniAndroidPermission.RequestPremission (AndroidPermission.WRITE_EXTERNAL_STORAGE, () => {
		// add permit action
        NativeGallery.SaveToGallery (screenCap,"Hair saloon", "screenShot_HairSallon"+Screenshot_Count+".png");
		}, () => {
		// add not permit action
		});
		}
		#elif UNITY_IOS
		NativeGallery.SaveToGallery (screenCap,"Hair saloon", "screenShot_HairSallon.png");
			
		#elif UNITY_EDITOR
	
		var bytes = screenCap.EncodeToJPG();
		 {
			string Screen_Shot_File_Name = path;
			do {
				Screenshot_Count++;
				Screen_Shot_File_Name = "Screenshot_" + Screenshot_Count + ".jpg";

			} while (System.IO.File.Exists (Screen_Shot_File_Name));
			System.IO.File.WriteAllBytes (Screen_Shot_File_Name, bytes);

		
			
		}
		#endif
        Screenshot_Count++;
        PlayerPrefs.SetInt("Screenshot_Count",Screenshot_Count);
	}

	IEnumerator Delay(){
		DropDown.instance.BothUp ();
		yield return new WaitForSeconds(1.01f);
		StartCoroutine ("Capture");
		StartCoroutine ("UploadScreenshot");
	}

	public void TakeShot(){
		GameManager.instance.ButtonClick_Audio.Play ();
		GameManager.instance.btnOpenCam.GetComponentInChildren<Button> ().enabled = false;
		GameManager.instance.Camera_Button.GetComponent<Button> ().interactable = false;
		GameManager.instance.HomeButton.GetComponent<Button>().interactable=false;
//		GameManager.instance.ShopButton_UI.GetComponentInChildren<Button>().interactable=false;
		GameManager.instance.BgSelectionButton.interactable=false;
		GameManager.instance.AvtarSelectionButton.interactable=false;
        if(GameManager.Avatar_No == AppOn_HairGame.CHARACTER_MERLE){
			GameManager.instance.Merle_Anim.SetInteger("State",14);
		}
		else if(GameManager.Avatar_No == AppOn_HairGame.CHARACTER_PAULO){
			GameManager.instance.Paulo_Anim.SetInteger("State",14);
		}
		else if(GameManager.Avatar_No == AppOn_HairGame.CHARACTER_JANET){
			GameManager.instance.Janet_Anim.SetInteger("State",14);
		}
		else if(GameManager.Avatar_No == AppOn_HairGame.CHARACTER_UGA){
			GameManager.instance.UGA_Anim.SetInteger("State",14);
		}

		StartCoroutine ("Delay");
//		StartCoroutine ("Capture");
//		StartCoroutine ("UploadScreenshot");
	}

	IEnumerator UploadScreenshot(){
		ScreenshotPreview_Anim.GetComponent<RectTransform>().sizeDelta=new Vector2(400,600);
//		ScreenshotPreview_Anim.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta=new Vector2(472.2f,630);
		ScreenshotBlink_Anim.SetTrigger ("Blink");
		Debug.Log ("screenShot blin called");
		TakeShot_Audio.Play ();

		ScreenshotPreview_Anim.SetTrigger ("ScreeshotPreview");
		Debug.Log ("ScreeshotPreview called");
		Texture2D texture = screenCap;
		Sprite sp = Sprite.Create (texture, new Rect (0, 0, texture.width, texture.height), Vector2.zero);
		Screenshot_Preview.sprite = sp;
		Screenshot_Preview.enabled = true;
		yield return new WaitForSeconds(5);
	//	Screenshot_Preview.enabled = false;
		ScreenshotPreview_Anim.SetTrigger ("idle");
		GameManager.instance.Camera_Button.GetComponent<Button> ().interactable = true;
		GameManager.instance.HomeButton.GetComponent<Button>().interactable=true;
//		GameManager.instance.ShopButton_UI.GetComponentInChildren<Button>().interactable=true;
		GameManager.instance.BgSelectionButton.interactable=true;
		GameManager.instance.AvtarSelectionButton.interactable=true;
		GameManager.instance.btnOpenCam.GetComponentInChildren<Button> ().enabled = true;
		shot = false;
	}
}