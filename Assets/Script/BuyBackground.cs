﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BuyBackground : MonoBehaviour {

	public Text text;
	public Image imgBgsprite;
	public Sprite[] bgsprite;
	// Use this for initialization
	void Start () {

	}
	void OnEnable()
	{
		switch(BG_Change.current_bg_id_to_purchase)
		{
		case BG_Change.BG_GERMANY:
//			text.text =  IAP_Manager.instance.getProductPrice (IAP_Manager.BG2_Buy);
			imgBgsprite.sprite = bgsprite[0];
			break;
		case BG_Change.BG_JAPAN:
//			text.text =  IAP_Manager.instance.getProductPrice (IAP_Manager.BG3_Buy);
			imgBgsprite.sprite = bgsprite[1];
			break;
		case BG_Change.BG_UK:
//			text.text =  IAP_Manager.instance.getProductPrice (IAP_Manager.BG1_Buy);
			imgBgsprite.sprite = bgsprite[2];
			break;
		case BG_Change.BG_USA:
//			text.text =  IAP_Manager.instance.getProductPrice (IAP_Manager.BG4_Buy);
			imgBgsprite.sprite = bgsprite[3];
			break;
		}
	}

	// Update is called once per frame
	void Update () {

	}
}
