﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using AppOn.HairGame;

public class ShowerHandler : MonoBehaviour,IDragHandler,IPointerDownHandler,IPointerUpHandler
{
	


	public Rigidbody2D rigidBody;
	public RectTransform rectTransoform;
	bool isStretch = false;
	public SpringJoint2D joint;

	void Awake ()
	{
		layermask = 1 << 8;
		rectTransoform = GetComponent<RectTransform> ();
	}

	public ShowerHandler ()
	{
		
	}

	public void OnPointerDown (PointerEventData data)
	{
		AppOn_HairGame.Instance.startShower ();
		rigidBody.velocity = Vector3.zero;
		joint.enabled = false;
		isStretch = true;
	}

	public void OnPointerUp (PointerEventData data)
	{
		AppOn_HairGame.Instance.stopShower();
		joint.enabled = true;
		isStretch = false;
	}

	public void LateUpdate ()
	{
		Vector3 pos = rectTransoform.localPosition;
		pos.x = Mathf.Clamp (pos.x, -320f, 300f);
		pos.y = Mathf.Clamp (pos.y, 220f, 1100f);
		rectTransoform.localPosition = pos;
	}

	int layermask;

	void FixedUpdate ()
	{
		if (AppOn_HairGame.Instance.shower1.IsAlive() ) {
			Vector3 fwd = transform.GetChild (0).TransformDirection (Vector3.down * 300);
			RaycastHit hit;
			if (Physics.Raycast (transform.GetChild (0).position, fwd, out hit, Mathf.Infinity, layermask) ) {
//				print ("There is something in front of the object!" + hit.collider.name);
				AppOn_HairGame.Instance.shower2.transform.position=new Vector3(transform.position.x-20, hit.point.y,0);
				AppOn_HairGame.Instance.shower2.Play();
			}else{
				AppOn_HairGame.Instance.shower2.Stop();
			}
		}else{
			AppOn_HairGame.Instance.shower2.Stop();
		}
	}

	public void OnDrag (PointerEventData data)
	{
		stretchShawer (data);
	}

	public void PlayStartAnimation ()
	{
		joint.enabled = true;
		rectTransoform.localPosition = new Vector3 (0, rectTransoform.localPosition.y - 40, 0);
	}

	public void stretchShawer (PointerEventData data)
	{
		Vector3 currentPosition = rectTransoform.localPosition;
		currentPosition.x += data.delta.x;
		currentPosition.y += data.delta.y;
		rectTransoform.localPosition = currentPosition;
	}


}
