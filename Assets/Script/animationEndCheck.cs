﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animationEndCheck : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void AnimEndCheck(){
		int value = PlayerPrefs.GetInt ("chaSelect_FtueHandled", 0);
		if(value <1){
			if (FTUEHandler.CanPlayHelp(FTUEHandler.handleCharacterDropDown)&&FTUEHandler.FTUE_Index[FTUEHandler.handleCharacterDropDown]==-1) {
				FTUEHandler.FTUE_Index [FTUEHandler.handleCharacterDropDown] = 0;
			}
			PlayerPrefs.SetInt ("chaSelect_FtueHandled", 1);
			PlayerPrefs.SetInt ("timeRun", 4);
			PlayerPrefs.Save ();
		}
	}
}
