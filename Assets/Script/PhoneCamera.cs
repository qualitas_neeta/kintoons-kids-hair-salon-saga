﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhoneCamera : MonoBehaviour
{

	private bool camAvailable;
	private WebCamTexture backCam, frontCam;
	private Texture defaultBackground;

	public RawImage background;
	public AspectRatioFitter fit;
	int imagenamecount = 0;
	public GameObject clickButton;
	Texture2D screenCap;

	public static PhoneCamera Instance;
	public static bool cameraOpen;
	private static bool giveAccessToCamera = false;
	static bool canCaptureCamera = true;
	public RectTransform Upperlayer, Lowerlayer, rightlayer, leftlayer;
	bool isFrontCamOpen;
	// Use this for initialization
	void Start ()
	{
		Instance = this;
		cameraOpen = true;
		defaultBackground = background.texture;
		Invoke ("GiveAccessToCamera",0.8f);

		WebCamDevice[] devices = WebCamTexture.devices;

		if (devices.Length == 0) {
			Debug.Log ("cam not available");
			camAvailable = false;
			return;
		}

		for (int i = 0; i < devices.Length; i++) {
#if UNITY_EDITOR 
			if (devices [i].isFrontFacing) {
				backCam=new WebCamTexture(devices[i].name);
			}
#else
			if(!devices[i].isFrontFacing){
				backCam=new WebCamTexture(devices[i].name);
			}else {
				frontCam=new WebCamTexture(devices[i].name);
			}
#endif


		}




		if (!isFrontCamOpen) {
			if (backCam == null) {
				Debug.Log ("unable to find back camera");
				return;
			}
			backCam.Play ();
			background.texture = backCam;
			camAvailable = true;

		} else {
			if (frontCam == null) {
				Debug.Log ("unable to find back camera");
				return;
			}
			frontCam.Play ();
			background.texture = frontCam;
			camAvailable = true;
		}

	}



	public void StartCamera ()
	{
		canCaptureCamera = true;
		isFrontCamOpen=false;
		cameraOpen = true;
		if (backCam && !isFrontCamOpen) {
			if (frontCam !=null && frontCam.isPlaying) {
				frontCam.Stop ();
			}
			backCam.Play ();
			background.texture = backCam;
		} else if (frontCam) {
			if (backCam.isPlaying) {
				backCam.Stop ();
			}
			frontCam.Play ();
			background.texture = frontCam;
		}
//		Upperlayer.localPosition = new Vector3 (0, background.rectTransform.rect.height / 2, 0);
//		Lowerlayer.localPosition = new Vector3 (0, -background.rectTransform.rect.height / 2, 0);
//		rightlayer.localPosition = new Vector3 (background.rectTransform.rect.width, 0, 0);
//		leftlayer.anchoredPosition = new Vector2 (-background.rectTransform.rect.width, 0);
		Invoke ("GiveAccessToCamera",0.8f);
	}
	void GiveAccessToCamera()
	{
		giveAccessToCamera = true;
	}
	public void ChangeCamera ()
	{
		if (!canCaptureCamera) {
			return;
		}
		GameManager.instance.ButtonClick_Audio.Play ();
		isFrontCamOpen = !isFrontCamOpen;
		if (backCam && !isFrontCamOpen) {
			if (frontCam.isPlaying) {
				frontCam.Stop ();
			}
			backCam.Play ();
			background.texture = backCam;
		} else if (frontCam) {
			if (backCam.isPlaying) {
				backCam.Stop ();
			}
			frontCam.Play ();
			background.texture = frontCam;
		}
	}

	// Update is called once per frame
//	void Update ()
//	{
//		if (!cameraOpen) {
//			return;
//		}
//
//		if ((!isFrontCamOpen &&!backCam) || (isFrontCamOpen && !frontCam))
//			return;
//
//		float ratio = (float)backCam.width / (float)backCam.height;
//		fit.aspectRatio = ratio;
//
//
//
//		float scaleY = backCam.videoVerticallyMirrored ? -1f : 1f;
//		background.rectTransform.localScale = new Vector3 (1, scaleY, 1);
//
//
//		int orient = -backCam.videoRotationAngle;
//		if(isFrontCamOpen){
//			orient = -frontCam.videoRotationAngle;
//		}
//		background.rectTransform.localEulerAngles = new Vector3 (0, 0, orient);
//
//
////		rightlayer.sizeDelta = new Vector2 (800, background.rectTransform.rect.height + 100);
////		leftlayer.sizeDelta = new Vector2 (800, background.rectTransform.rect.height + 100);
//
//
//		Upperlayer.localPosition = new Vector3 (0, background.rectTransform.rect.height / 2, 0);
////		Upperlayer.GetChild(0).GetComponent<RectTransform>().sizeDelta=new Vector2 (background.rectTransform.rect.width ,47);
//		Lowerlayer.localPosition = new Vector3 (0, -background.rectTransform.rect.height / 2, 0);
////		Lowerlayer.GetChild(0).GetComponent<RectTransform>().sizeDelta=new Vector2 (background.rectTransform.rect.width ,47);
//
//
//		rightlayer.anchoredPosition = new Vector2 (background.rectTransform.rect.size.x - 100, 0);
//		leftlayer.anchoredPosition = new Vector2 (-background.rectTransform.rect.size.x + 100, 0);
//
//		rightlayer.sizeDelta = new Vector2 (800, background.rectTransform.rect.height);
////		rightlayer.GetChild(0).GetComponent<RectTransform>().sizeDelta=new Vector2 ( background.rectTransform.rect.height ,47);
//		leftlayer.sizeDelta = new Vector2 (800, background.rectTransform.rect.height);
////		leftlayer.GetChild(0).GetComponent<RectTransform>().sizeDelta=new Vector2 ( background.rectTransform.rect.height,47);
//
////		rightlayer.sizeDelta = new Vector2 ( background.rectTransform.rect.height + 100,800);
////		leftlayer.sizeDelta = new Vector2 (background.rectTransform.rect.height + 100,800 );
//
//
//		Vector3 mousepos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
//		mousepos.z = 0;
//
//
//		x1 = Camera.main.WorldToScreenPoint (leftlayer.transform.position).x;
//		x2 = Camera.main.WorldToScreenPoint (rightlayer.transform.position).x;
//		y1 = Camera.main.WorldToScreenPoint (Upperlayer.transform.position).y;
//		y2 = Camera.main.WorldToScreenPoint (Lowerlayer.transform.position).y;
//		ww = x2 - x1;
//		hh = y1 - y2;
//		rectScreen = new Rect (x1, y2, ww, hh);
//		if (mousepos.y < (background.transform.position.y + 200) && mousepos.y > (background.transform.position.y - 100)) {
//			GameManager.instance.avtar.transform.position = mousepos;
//		}
//	}

	void Update ()
	{
		Debug.Log("================ "+cameraOpen);
		if (!cameraOpen) {
			return;
		}
		Debug.Log("================1|2 "+(!isFrontCamOpen &&!backCam) +" | " + (isFrontCamOpen && !frontCam));
		Debug.Log("================1|1     "+isFrontCamOpen+"|"+backCam +"        2|2 "+isFrontCamOpen +"|"+frontCam);

		if ((!isFrontCamOpen &&!backCam) || (isFrontCamOpen && !frontCam))
			return;

		float ratio = (float)backCam.width / (float)backCam.height;
		fit.aspectRatio = ratio;

		float scaleY = backCam.videoVerticallyMirrored ? -1f : 1f;
		background.rectTransform.localScale = new Vector3 (1, scaleY, 1);


		int orient = -backCam.videoRotationAngle;
		if(isFrontCamOpen){
			orient = -frontCam.videoRotationAngle;
		}
		background.rectTransform.localEulerAngles = new Vector3 (0, 0, orient);

		Upperlayer.localPosition = new Vector3 (0, background.rectTransform.rect.height / 2, 0);
		Lowerlayer.localPosition = new Vector3 (0, -background.rectTransform.rect.height / 2, 0);

		rightlayer.anchoredPosition = new Vector2 (background.rectTransform.rect.size.x-100 , 0);
		rightlayer.GetChild(0).GetComponent<RectTransform>().sizeDelta=new Vector2(background.rectTransform.rect.size.y,47);

		leftlayer.anchoredPosition = new Vector2 (-background.rectTransform.rect.size.x+ 100 , 0);
		leftlayer.GetChild(0).GetComponent<RectTransform>().sizeDelta=new Vector2(background.rectTransform.rect.size.y,47);

		Upperlayer.sizeDelta=new Vector2 (background.rectTransform.rect.size.x,800);
		Lowerlayer.sizeDelta=new Vector2 (background.rectTransform.rect.size.x,800);

		rightlayer.sizeDelta = new Vector2 (800, background.rectTransform.rect.height + 100);
		leftlayer.sizeDelta = new Vector2 (800, background.rectTransform.rect.height + 100);


		Vector3 mousepos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		mousepos.z = 0;
		x1 = Camera.main.WorldToScreenPoint (leftlayer.transform.position).x;
		x2 = Camera.main.WorldToScreenPoint (rightlayer.transform.position).x;
		y1 = Camera.main.WorldToScreenPoint (Upperlayer.transform.position).y;
		y2 = Camera.main.WorldToScreenPoint (Lowerlayer.transform.position).y;
		ww = x2 - x1;
		hh = y1 - y2;
		Debug.Log("================ x1:x2    y1:y2  "+x1+":"+x2+"    "+y1+":y2");
		Debug.Log("================ ww:hh = "+ww+":"+hh);
		rectScreen = new Rect (x1, y2, ww, hh);
		if (giveAccessToCamera) {
			if (mousepos.y < (background.transform.position.y + 200) && mousepos.y > (background.transform.position.y - 100)) {
				GameManager.instance.avtar.transform.position = mousepos;
			}
		}
	}

	IEnumerator SetCamera ()
	{
		yield return new WaitForEndOfFrame ();
	}

	float x1, x2, y1, y2, yy, ww, hh;
	public Rect rectScreen;
//		void OnGUI(){
//		if(GUI.Button(new Rect(100,100,100,100),"change cam")){
//			ChangeCamera();
//		}
//		}





	public void Click ()
	{
		GameManager.instance.ButtonClick_Audio.Play ();
		if (canCaptureCamera) {
			StartCoroutine (Capture ());
		}
	}

	public void close ()
	{
		if (!canCaptureCamera) {
			return;
		}
		GameManager.instance.ButtonClick_Audio.Play ();
		GameManager.instance.CameraPanelUpperlayer.SetActive (false);
		GameManager.instance.CamPanel.SetActive (false);
		GameManager.instance.btnOpenCam.SetActive (true);
		GameManager.instance.DrawerConatainer.SetActive (true);
		GameManager.instance.BottomPanel.SetActive (true);
		GameManager.instance.avtar.localScale = new Vector3 (0.7f, 0.7f, 0.7f);
		GameManager.instance.avtar.transform.position = new Vector3 (10, -38f, 0);
		if (backCam) {
			backCam.Stop ();
		}
		if(frontCam){
			frontCam.Stop();
		}
		if (AppOn.HairGame.AppOn_HairGame.Instance._cur_hair_style == "Hair_Wash") {
			AppOn.HairGame.AppOn_HairGame.Instance.enableShower ();
		}
		giveAccessToCamera = false;
		cameraOpen = false;
	}



	IEnumerator Capture ()
	{
		screenCap = new Texture2D ((int)ww, (int)hh, TextureFormat.RGBA32, false);
		canCaptureCamera = false;
		clickButton.GetComponent<Button> ().interactable = false;
		yield return new WaitForEndOfFrame ();
		//		StartCoroutine ("Delay");
		screenCap.ReadPixels (rectScreen/*new Rect ((Screen.width) /4.4f, (Screen.height) /2.9f, width, height)*/, 0, 0);
		Color32[] color = screenCap.GetPixels32 ();
		for (int i = 0; i < color.Length; i++) {
			color [i] = new Color32 (color [i].r, color [i].g, color [i].b, 255);
		}
		screenCap.SetPixels32 (color);
		screenCap.Apply ();
		imagenamecount = PlayerPrefs.GetInt ("imagename", imagenamecount);
		#if UNITY_ANDROID && !UNITY_EDITOR
		if(UniAndroidPermission.IsPermitted (AndroidPermission.WRITE_EXTERNAL_STORAGE)){
			NativeGallery.SaveToGallery (screenCap,"Hair saloon", "HairSallon"+imagenamecount+".png");
			imagenamecount++;
			PlayerPrefs.SetInt("imagename",imagenamecount);
		}else{
			UniAndroidPermission.RequestPremission (AndroidPermission.WRITE_EXTERNAL_STORAGE, () => {
			// add permit action
				NativeGallery.SaveToGallery (screenCap,"Hair saloon", "HairSallon"+imagenamecount+".png");
				imagenamecount++;
				PlayerPrefs.SetInt("imagename",imagenamecount);
		}, () => {

		});
		}
		#else
		NativeGallery.SaveToGallery (screenCap, "Hair saloon", "HairSallon" + imagenamecount + ".png");
		imagenamecount++;
		PlayerPrefs.SetInt ("imagename", imagenamecount);
		#endif


		yield return new WaitForSeconds (1.02f);
		StartCoroutine ("UploadScreenshot");

	}

	IEnumerator UploadScreenshot ()
	{
		if (ww > 480 || hh > 600) {
			ScreenShot.instance.ScreenshotPreview_Anim.GetComponent<RectTransform> ().sizeDelta = new Vector2 (480, 600);
		} else {
			ScreenShot.instance.ScreenshotPreview_Anim.GetComponent<RectTransform> ().sizeDelta = new Vector2 (ww, hh);
		}
		ScreenShot.instance.ScreenshotBlink_Anim.SetTrigger ("Blink");
		ScreenShot.instance.TakeShot_Audio.Play ();
		ScreenShot.instance.ScreenshotPreview_Anim.SetTrigger ("ScreeshotPreview");
		Texture2D texture = screenCap;
		Sprite sp = Sprite.Create (texture, new Rect (0, 0, texture.width, texture.height), Vector2.zero);
		ScreenShot.instance.Screenshot_Preview.sprite = sp;
		ScreenShot.instance.Screenshot_Preview.enabled = true;
		yield return new WaitForSeconds (5);
		ScreenShot.instance.ScreenshotPreview_Anim.SetTrigger ("idle");
		GameManager.instance.Camera_Button.GetComponent<Button> ().interactable = true;
		GameManager.instance.HomeButton.GetComponent<Button> ().interactable = true;
//		GameManager.instance.ShopButton_UI.GetComponentInChildren<Button> ().interactable = true;
		GameManager.instance.BgSelectionButton.interactable = true;
		GameManager.instance.AvtarSelectionButton.interactable = true;
		canCaptureCamera = true;
		clickButton.GetComponent<Button> ().interactable = true;
	}
}
