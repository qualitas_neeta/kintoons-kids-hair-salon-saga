﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AppOn.HairGame;
public class Interactable : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		int i = DragBottomPanel.Instance._currentPage;
		if (i == AppOn_HairGame.HAIR_STYLE_ACCESSORIES_4)
			gameObject.GetComponent<CanvasGroup> ().blocksRaycasts = true;
		else
			gameObject.GetComponent<CanvasGroup> ().blocksRaycasts = false;
	}
}
