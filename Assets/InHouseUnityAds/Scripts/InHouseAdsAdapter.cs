﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
namespace AdsPlugin.Api
{
	public class InHouseAdsAdapter : IAdsManager
	{
		public  event Action<string> OnLoadAd;
		public  event Action<string> OnAdLoadedSuccessfully;
		public  event Action<string> OnAdLoadedFailed;
		public  event Action<string> OnAdClosedDone;
		public  event Action<string> OnBtnClickedEvent;
		public  event Action<string> OnAdShowed;
		public  event Action<string> OnNoAdsAvailble;

		private static InHouseAdsAdapter instance;

		public static InHouseAdsAdapter Instance {
			get {
				if (instance == null) {
					instance = new InHouseAdsAdapter ();
				}
				return instance;
			}
		}

		public InHouseAdsAdapter(){
			client=InHouseAdsManager.Insatnce;
			client.OnLoadAd+=LoadAd;
			client.OnAdLoadedSuccessfully+=AdLoadedSuccessfully;
			client.OnAdLoadedFailed+=AdLoadedFailed;
			client.OnAdClosedDone+=AdClosedDone;
			client.OnBtnClickedEvent+=BtnClickedEvent;
			client.OnAdShowed+=AdShowed;
			client.OnNoAdsAvailble+=NoAdsAvailble;
		}
		private IInHouseAds client;

		void AdClosedDone(string type){
			if(OnAdClosedDone!=null){
				OnAdClosedDone(type);
			}
		}

		void LoadAd(string type){
			if(OnLoadAd!=null){
				OnLoadAd(type);
			}
		}

		void AdLoadedSuccessfully(string type){
			if(OnAdLoadedSuccessfully!=null){
				OnAdLoadedSuccessfully(type);
			}
		}

		void AdLoadedFailed(string type){
			if(OnAdLoadedFailed!=null){
				OnAdLoadedFailed(type);
			}
		}

		void BtnClickedEvent(string type){
			if(OnBtnClickedEvent!=null){
				OnBtnClickedEvent(type);
			}
		}

		void AdShowed(string type){
			if(OnAdShowed!=null){
				OnAdShowed(type);
			}
		}

		void NoAdsAvailble(string type){
			if(OnNoAdsAvailble!=null){
				OnNoAdsAvailble(type);
			}
		}


		public void LoadAd(){
			client.LoadAd();
		}

		public void ShowAd(){
			client.Show();
		}

		public bool IsLoaded(){
			return  client.IsLoaded();
		}


	}
}

