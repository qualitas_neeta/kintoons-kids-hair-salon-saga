﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using UnityEngine;
using System.Collections;
using System;

public class MasterController : MonoBehaviour
{
	public static string mainURL = "http://cross-promotion-tool.kinsane.com/";
	public const string adVersion="version1/";
	static MasterController insatnce;

	public event Action InitDone;
	public event Action InitFail;

	public static MasterController Insatnce {
		get {
			if (insatnce == null) {
				insatnce = (MasterController)FindObjectOfType (typeof(MasterController));

				if (FindObjectsOfType (typeof(MasterController)).Length > 1) {
					Debug.LogError ("[Singleton] Something went really wrong " +
					" - there should never be more than 1 singleton!" +
					" Reopening the scene might fix it.");
					DontDestroyOnLoad (insatnce.gameObject);
					return insatnce;
				}

				if (insatnce == null) {
					GameObject singleton = new GameObject ();
					insatnce = singleton.AddComponent<MasterController> ();
					singleton.name = "(singleton) " + typeof(MasterController).ToString ();

					DontDestroyOnLoad (singleton);

					Debug.Log ("[Singleton] An instance of " + typeof(MasterController) +
					" is needed in the scene, so '" + singleton +
					"' was created with DontDestroyOnLoad.");
				} else {
					Debug.Log ("[Singleton] Using instance already created: " +
					insatnce.gameObject.name);
					DontDestroyOnLoad (insatnce.gameObject);
				}
			}
			return	insatnce;
		}
	}


	int[,] resInfo = { { 240, 400 }, { 480, 640 }, { 640, 1024 }, { 1000, 1500 } };
	int[,] imagesTakenFromArtist = { { 240, 320 }, { 320, 480 }, { 480, 800 }, { 800, 1280 }, { 800, 1280 } };
	string[] resNames = { "l", "m", "h", "x", "xh" };


	public static MasterXml AdInfo;

	public static string bundleId;
	public static string appVersion;
	public static string platform;
	public static string resolution;
	public static float width;
	public static float height;
	public static float aspectRatio;
	static bool initDone;

	public void Initilize ()
	{
		if (initDone) {
			return;
		}
		if (AdInfo == null) {
			AdInfo = new MasterXml ();
		}

		width = Screen.width;
		height = Screen.height;

		CheckResolution ();
		bundleId = Application.identifier;
		appVersion = Application.version;
		#if UNITY_ANDROID
		platform = "android/";
		#elif UNITY_IOS
		platform="ios/";
		#endif


		mainURL+=platform;
		StartCoroutine (InitAds ());
	}

	IEnumerator InitAds ()
	{
		string url = mainURL+adVersion + bundleId + ".xml";
		Debug.Log(url);
		WWW www = new WWW (url);
		yield return www;
        if (www.error != null) {
			if (InitFail != null) {
				InitFail ();
			}
		} else {
			GetDeatils (www.text);
		}
	}

	void CheckResolution ()
	{
		string imageName = "image";
		bool lanscape = false;
		bool isRes = false;
		if (width > height) {
			lanscape = true;
			imageName += "_landscape";
		}
		for (int i = 0; i < resInfo.GetLength (0); i++) {
			if ((width <= resInfo [i, 0] && height <= resInfo [i, 1]) || (width <= resInfo [i, 1] && height <= resInfo [i, 0])) {
				imageName += "_" + resNames [i];
				isRes = true;
				break;
			}
		}
		Debug.Log (resInfo.GetLength (0));
		if (!isRes) {
			imageName += "_" + resNames [resInfo.GetLength (0)];
		}
		resolution = imageName;
	}

	void GetDeatils (string xmlText)
	{
		XmlDocument xml = new XmlDocument ();

		try {
			xml.LoadXml (xmlText); 
			AdInfo.StartFrom = int.Parse (xml.DocumentElement ["startFrom"].InnerText);
			AdInfo.ShowHouseAds = bool.Parse (xml.DocumentElement ["showHouseAds"].InnerText);
			AdInfo.ShowNormalAds = bool.Parse (xml.DocumentElement ["showNormalAds"].InnerText);
			AdInfo.Dir = xml.DocumentElement ["dir"].InnerText.Split (',');
			AdInfo.Frequency = int.Parse (xml.DocumentElement ["frequency"].InnerText);
			AdInfo.Alternate = bool.Parse (xml.DocumentElement ["alternate"].InnerText);
			AdInfo.Delay = int.Parse (xml.DocumentElement ["delay"].InnerText);
            AdInfo.ForceUpdate = float.Parse (xml.DocumentElement ["forceupdate"].InnerText);

//            Debug.Log("StartFrom :"+AdInfo.StartFrom + "---showhouseads :"+AdInfo.ShowHouseAds+ "---shownormalads:"+AdInfo.ShowNormalAds + "---frequency:"+AdInfo.Frequency+ "---alternet:"+AdInfo.Alternate + "---delay:"+  AdInfo.Delay + "---forceupdate:"+  AdInfo.ForceUpdate);
			initDone = true;
			if (AdInfo.ForceUpdate == -1) {

			} else if (AdInfo.ForceUpdate > float.Parse (appVersion)) {
				NativeBridge.Insatnce.ShowMessage ("Update Available", "Please update game", "Ok");
			}
			if (InitDone != null) {
				InitDone ();
			}
		} catch (Exception ex) {
			Debug.Log(ex.Message);
			if (InitFail != null) {
				InitFail ();
			}
		}
	}
}
