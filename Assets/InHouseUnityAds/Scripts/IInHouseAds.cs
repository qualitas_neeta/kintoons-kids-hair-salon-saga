﻿using System;
namespace AdsPlugin
{
interface IInHouseAds
{
	event Action<string> OnLoadAd;
	event Action<string> OnAdLoadedSuccessfully;
	event Action<string> OnAdLoadedFailed;
	event Action<string> OnAdClosedDone;
	event Action<string> OnBtnClickedEvent;
	event Action<string> OnAdShowed;
	event Action<string> OnNoAdsAvailble;

	bool IsLoaded ();

	void LoadAd ();

	void Show ();
}
}