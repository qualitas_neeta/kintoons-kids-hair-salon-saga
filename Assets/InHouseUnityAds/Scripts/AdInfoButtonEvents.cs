﻿
public struct AdInfoButtonEvents {
	
	private string click;

	public string Click {
		get {
			return click;
		}
		set {
			click = value;
		}
	}

	private string color;

	public string Color {
		get {
			return color;
		}
		set {
			color = value;
		}
	}

	private string al;

	public string Al {
		get {
			return al;
		}
		set {
			al = value;
		}
	}

	private string screenRes;

	public string ScreenRes {
		get {
			return screenRes;
		}
		set {
			screenRes = value;
		}
	}

	private string btnRect;

	public string BtnRect {
		get {
			return btnRect;
		}
		set {
			btnRect = value;
		}
	}
}
