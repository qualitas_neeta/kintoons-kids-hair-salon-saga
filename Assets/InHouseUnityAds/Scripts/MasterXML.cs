﻿
public class MasterXml
{
	private int delay;

	public int Delay {
		get {
			return delay;
		}
		set {
			delay = value;
		}
	}

	private float forceUpdate;

	public float ForceUpdate {
		get {
			return forceUpdate;
		}
		set {
			forceUpdate = value;
		}
	}

	private bool showHouseAds;

	public bool ShowHouseAds {
		get {
			return showHouseAds;
		}
		set {
			showHouseAds = value;
		}
	}

	private	bool showNormalAds;

	public bool ShowNormalAds {
		get {
			return showNormalAds;
		}
		set {
			showNormalAds = value;
		}
	}

	private	int startFrom;

	public int StartFrom {
		get {
			return startFrom;
		}
		set {
			startFrom = value;
		}
	}

	private	string[] dir;

	public string[] Dir {
		get {
			return dir;
		}
		set {
			dir = value;
		}
	}

	private	int frequency;

	public int Frequency {
		get {
			return frequency;
		}
		set {
			frequency = value;
		}
	}

	private bool alternate;

	public bool Alternate {
		get {
			return alternate;
		}
		set {
			alternate = value;
		}
	}
}
