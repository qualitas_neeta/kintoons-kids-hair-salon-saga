﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

namespace AdsPlugin
{
	public class InHouseAdsManager : MonoBehaviour, IInHouseAds
	{
		public event Action<string> OnLoadAd;
		public event Action<string> OnAdLoadedSuccessfully;
		public event Action<string> OnAdLoadedFailed;
		public event Action<string> OnAdClosedDone;
		public event Action<string> OnBtnClickedEvent;
		public event Action<string> OnAdShowed;
		public event Action<string> OnNoAdsAvailble;


		protected bool Shown;


		Texture2D bgTexture;
		[SerializeField]
		protected	CanvasScaler AdsCanvas;
		[SerializeField]
		protected RectTransform AdsContainer;
		[SerializeField]
		protected RectTransform AdsTint;
	
		static int directoryCounter = 0;
		bool FillAdLoaded;

		static InHouseAdsManager insatnce;

		public static InHouseAdsManager Insatnce {
			get {
				if (insatnce == null) {
					insatnce = (InHouseAdsManager)FindObjectOfType (typeof(InHouseAdsManager));

					if (FindObjectsOfType (typeof(InHouseAdsManager)).Length > 1) {
						DontDestroyOnLoad (insatnce.gameObject);
						Debug.LogError ("[Singleton] Something went really wrong " +
						" - there should never be more than 1 singleton!" +
						" Reopening the scene might fix it.");
						return insatnce;
					}

					if (insatnce == null) {
						GameObject singleton = new GameObject ();
						insatnce = singleton.AddComponent<InHouseAdsManager> ();
						singleton.name = "(singleton) " + typeof(InHouseAdsManager).ToString ();

						DontDestroyOnLoad (singleton);

						Debug.Log ("[Singleton] An instance of " + typeof(InHouseAdsManager) +
						" is needed in the scene, so '" + singleton +
						"' was created with DontDestroyOnLoad.");
					} else {
						DontDestroyOnLoad (insatnce.gameObject);
						Debug.Log ("[Singleton] Using instance already created: " +
						insatnce.gameObject.name);
					}
				}
				return	insatnce;
			}
		}


		void Awake ()
		{

			Canvas cnv = Insatnce.gameObject.GetComponent<Canvas> ();
			if (cnv == null) {
				cnv =	Insatnce.gameObject.AddComponent<Canvas> ();
				cnv.renderMode = RenderMode.ScreenSpaceOverlay;
				cnv.sortingOrder = -100000;
				AdsCanvas =	insatnce.gameObject.AddComponent<CanvasScaler> ();
				AdsCanvas.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
				AdsCanvas.matchWidthOrHeight = 1;
				Insatnce.gameObject.AddComponent<GraphicRaycaster> ();
				GameObject bgtint = new GameObject ("tint");
				AdsTint = bgtint.AddComponent<RectTransform> ();
				AdsTint.transform.parent = Insatnce.transform;
				AdsTint.anchorMin = new Vector2 (0, 0);
				AdsTint.anchorMax = new Vector2 (1, 1);
				AdsTint.anchoredPosition = new Vector2 (0, 0);
				Image colortint =	AdsTint.gameObject.AddComponent<Image> ();
				colortint.color = new Color (0, 0, 0, 0.8f);
				AdsTint.gameObject.SetActive (false);

				GameObject rect = new GameObject ("ContainerAds");
				AdsContainer = rect.AddComponent<RectTransform> ();
				AdsContainer.transform.parent = Insatnce.transform;
				AdsContainer.anchoredPosition = new Vector2 (0, 0);
				AdsContainer.gameObject.AddComponent<Image> ();
				AdsContainer.gameObject.SetActive (false);
			} else {
				insatnce = this;
			}


		}

		public void LoadAd ()
		{
			StartCoroutine (DownloadAd ());
		}

		public IEnumerator DownloadAd ()
		{
			if (OnLoadAd != null) {
				OnLoadAd ("InHouse");
			}
			string url = MasterController.mainURL + MasterController.AdInfo.Dir [directoryCounter] + "/adinfo.xml";
			Debug.Log(url);
			bool AdLoaded = false;
			WWW www = new WWW (url);
			yield return www;
			if (www.error == null) {
			
				AdInfo.Instance.events.Clear ();
				LoadFile (www.text);
				AdLoaded = true;

			
			}
			if (AdLoaded) {
				AdLoaded = false;
				url = MasterController.mainURL + MasterController.AdInfo.Dir [directoryCounter] + "/" + MasterController.resolution + ".jpg";
				Debug.Log(url);
				directoryCounter++;
				if (directoryCounter > (MasterController.AdInfo.Dir.Length - 1)) {
					directoryCounter = 0;
				}
				WWW wwwImage = new WWW (url);
				yield return wwwImage;
				if (www.error == null) {
					bgTexture = wwwImage.texture;
					AdLoaded = true;
					FillAdLoaded = true;
					if (OnAdLoadedSuccessfully != null) {
						OnAdLoadedSuccessfully ("InHouse");
					}
				}
			}

			if (!AdLoaded) {
				FillAdLoaded = false;
				if (OnAdLoadedFailed != null) {
					OnAdLoadedFailed ("InHouse");
				}
			}
		}

		public  bool IsLoaded ()
		{
			if (!FillAdLoaded && OnNoAdsAvailble != null) {
				OnNoAdsAvailble ("InHouse");
			}
			return FillAdLoaded;
		}

		public  void Show ()
		{
			if (Shown) {
				return;
			}

			Shown = true;
			AdsContainer.gameObject.SetActive (true);
			AdsTint.gameObject.SetActive (true);
			AdsCanvas.referenceResolution = new Vector2 (bgTexture.width, bgTexture.height);
			AdsContainer.sizeDelta = new Vector2 (bgTexture.width, bgTexture.height);
			AdsContainer.GetComponent<Image> ().sprite = Sprite.Create (bgTexture, new Rect (0, 0, bgTexture.width, bgTexture.height), new Vector2 (0.5f, 0.5f));
			AdsContainer.GetComponent<Image> ().preserveAspect=true;
			AdsCanvas.matchWidthOrHeight = 1;
			foreach (AdInfoButtonEvents btn in	AdInfo.Instance.events) {
				GameObject btnObj = new GameObject (btn.Click);
				btnObj.transform.parent = AdsContainer.transform;
				btnObj.transform.localScale = Vector3.one;
				Image btnimg =	btnObj.AddComponent<Image> ();
				RectTransform rectt = btnimg.GetComponent<RectTransform> ();
				rectt.anchorMax = new Vector2 (0, 0);
				rectt.anchorMin = new Vector2 (0, 0);
				rectt.pivot = new Vector2 (0, 0);

				string[] rectstr = btn.BtnRect.Split (',');

				rectt.sizeDelta = new Vector2 (int.Parse (rectstr [2]), int.Parse (rectstr [3]));
				rectt.anchoredPosition = new Vector2 (int.Parse (rectstr [0]), bgTexture.height - int.Parse (rectstr [1]) - int.Parse (rectstr [3]));
				Color col=Color.white;
//				ColorUtility.TryParseHtmlString ("#" + btn.Color, out col);
				col.a = 0;//float.Parse (btn.Al) / (float)255;
				btnimg.color = col;
				Button buttonComponent =	btnObj.AddComponent<Button> ();
				buttonComponent.onClick.AddListener (delegate {
					HandleClick (btn.Click);
				});
			}
			if (MasterController.width > MasterController.height) {
				AdsContainer.localScale = new Vector3 (AdsContainer.localScale.x-0.05f, AdsContainer.localScale.y-0.05f, AdsContainer.localScale.z);
			} else {
				MasterController.aspectRatio = MasterController.height / MasterController.width;
				float imageAspectRatio = (float)bgTexture.height / (float)bgTexture.width;

				float percentage = (imageAspectRatio / MasterController.aspectRatio);
				AdsContainer.localScale = new Vector3 (percentage-0.05f, percentage-0.05f, percentage);

			}


			if (OnAdShowed != null) {
				OnAdShowed ("InHouse");
			}
		}

		protected internal void HandleClick (string type)
		{
			if (type.Contains ("close")) {
				FillAdLoaded = false;
				Shown = false;
				for (int i = 0; i < AdsContainer.transform.childCount; i++) {
					Destroy (AdsContainer.transform.GetChild (i).gameObject);
				}
				if (OnAdClosedDone != null) {
					OnAdClosedDone ("InHouse");
				}
				AdsContainer.gameObject.SetActive (false);
				AdsTint.gameObject.SetActive (false);
			} else if (type.Contains ("http") || type.Contains ("itms") || type.Contains ("itms-app")) {
				Application.OpenURL (type);
				if (OnBtnClickedEvent != null) {
					OnBtnClickedEvent ("InHouse");
				}
			} else if(type.StartsWith("market://")) 
			{
				type+="&referrer=utm_source%3DHouseAds%26utm_medium%3D"+MasterController.bundleId+"%26utm_term%3DHouseAds%26utm_campaign%3DHouseads";
				Application.OpenURL (type);
				if (OnBtnClickedEvent != null) {
					OnBtnClickedEvent ("InHouse");
				}

				Debug.Log ("click type : " + type);
			}
			
			else {
				if (OnBtnClickedEvent != null) {
					OnBtnClickedEvent ("InHouse");
				}
				Debug.Log ("click type : " + type);
			}

		}

		protected internal void LoadFile (string path)
		{
			XmlDocument xml = new XmlDocument ();
			xml.LoadXml (path); 
			XmlNodeList elt = xml.SelectNodes ("b/event") as XmlNodeList;
			foreach (XmlNode node in elt) {
				AdInfoButtonEvents btn = new AdInfoButtonEvents ();
				btn.Click = node.Attributes ["click"].Value;
				btn.Color = node.Attributes ["color"].Value;
				btn.Al = node.Attributes ["al"].Value;
				switch (MasterController.resolution) {
				case "image_l":
					btn.BtnRect = node.Attributes ["rectl"].Value;
					break;
				case "image_m":
					btn.BtnRect = node.Attributes ["rectm"].Value;
					break;
				case "image_h":
					btn.BtnRect = node.Attributes ["recth"].Value;
					break;
				case "image_x":
					btn.BtnRect = node.Attributes ["rectx"].Value;
					break;
				case "image_xh":
					btn.BtnRect = node.Attributes ["rectxh"].Value;
					break;
				case "image_landscape_l":
					btn.BtnRect = node.Attributes ["lrectl"].Value;
					break;
				case "image_landscape_m":
					btn.BtnRect = node.Attributes ["lrectm"].Value;
					break;
				case "image_landscape_h":
					btn.BtnRect = node.Attributes ["lrecth"].Value;
					break;
				case "image_landscape_x":
					btn.BtnRect = node.Attributes ["lrectx"].Value;
					break;
				case "image_landscape_xh":
					btn.BtnRect = node.Attributes ["lrectxh"].Value;
					break;
				default:
					btn.BtnRect = node.Attributes ["lrectxh"].Value;
					break;
				}
				AdInfo.Instance.events.Add (btn);
			}
		}

	}






}