﻿using System;

namespace AdsPlugin.Api
{
public interface IAdListner
{
	event Action OnInitilizedSuccess;
	event Action OnInitilizedFail;
	event Action<string> OnAdLoad;
	event Action<string> OnAdLoadedSuccessfully;
	event Action<string> OnAdLoadedFailed;
	event Action<string> OnAdClosed;
	event Action<string> OnAdClicked;
	event Action<string> OnAdShowed;
	event Action<string> OnNoAdsAvailble;


	void InitilizedSuccess ();
	void InitilizedFail ();
	void AdLoad (string adType);
	void AdLoadedSuccess (string adType);
	void AdLoadedFail (string adType);
	void AdClosed (string adType);
	void AdClicked (string type);
	void AdShown (string AdType);
	void AdsNotAvailable (string adType);

}
}
