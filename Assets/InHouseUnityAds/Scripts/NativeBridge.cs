﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AdsPlugin.Bridge;
using System.Runtime.InteropServices;
public class NativeBridge : MonoBehaviour
{
	static NativeBridge insatnce;

	public static NativeBridge Insatnce {
		get {
			if (insatnce == null) {
				insatnce = (NativeBridge)FindObjectOfType (typeof(NativeBridge));

				if (FindObjectsOfType (typeof(NativeBridge)).Length > 1) {
					DontDestroyOnLoad (insatnce.gameObject);
					Debug.LogError ("[Singleton] Something went really wrong " +
					" - there should never be more than 1 singleton!" +
					" Reopening the scene might fix it.");
					return insatnce;
				}

				if (insatnce == null) {
					GameObject singleton = new GameObject ();
					insatnce = singleton.AddComponent<NativeBridge> ();
					singleton.name = "(singleton) " + typeof(NativeBridge).ToString ();

					DontDestroyOnLoad (singleton);

					Debug.Log ("[Singleton] An instance of " + typeof(NativeBridge) +
					" is needed in the scene, so '" + singleton +
					"' was created with DontDestroyOnLoad.");
				} else {
					DontDestroyOnLoad (insatnce.gameObject);
					Debug.Log ("[Singleton] Using instance already created: " +
					insatnce.gameObject.name);
				}
			}
			return	insatnce;
		}
	}

	public void ShowMessage (string title, string msg, string btnOk)
	{
		#if UNITY_ANDROID
		NativeAndroid.showMessage (insatnce.gameObject.name, title, msg, btnOk);
		#elif UNITY_IOS
		NativeIOS.showMessage (insatnce.gameObject.name, title, msg, btnOk);
		#endif
		
	}

	public void ShowDialog (string title, string msg, string btnOk, string cancel)
	{
		
		#if UNITY_ANDROID
		NativeAndroid.showDialog (insatnce.gameObject.name, title, msg, btnOk, cancel);
		#elif UNITY_IOS
		NativeIOS.showDialog (insatnce.gameObject.name, title, msg, btnOk,cancel);
		#endif
	}


	public void OnMessagePopUpCallBack (string buttonIndex)
	{
		Application.Quit ();
	}

	public void OnDialogPopUpCallBack (string buttonIndex)
	{
		
	}



}

namespace AdsPlugin.Bridge
{
	class NativeIOS
	{
		#if (UNITY_IPHONE && !UNITY_EDITOR) || DEBUG_MODE
		
		[DllImport("__Internal")]
		private static extern void _TAG_ShowDialog(string gameObjectName,string title, string message, string yes, string no);
		[DllImport("__Internal")]
		private static extern void _TAG_ShowMessage(string gameObjectName,string title, string message, string ok);
		#endif

		public static void showDialog (string gameObjectName, string title, string message, string yes, string no)
		{
			#if (UNITY_IPHONE && !UNITY_EDITOR)
			try {
			_TAG_ShowDialog(gameObjectName,title, message, yes, no);
			}catch(System.Exception ex){

			}
			#endif
		}

		public static void showMessage (string gameObjectName, string title, string message, string ok)
		{
			#if (UNITY_IPHONE && !UNITY_EDITOR)
			try{
				_TAG_ShowMessage(gameObjectName,title, message, ok);
			} catch (System.Exception ex) {}
			#endif
		}
	}

	class NativeAndroid
	{
		public static void showMessage (string gameObjectName, string title, string message, string ok)
		{
			#if (UNITY_ANDROID && !UNITY_EDITOR)
			try {
				var plugin = new AndroidJavaClass ("appon.unitynativeplugin.nativeplugin");
				plugin.CallStatic ("ShowMessagePopup", gameObjectName, title, message, ok);
			} catch (System.Exception ex) {
				
			}
			#endif
		}

		public static void showDialog (string gameObjectName, string title, string message, string ok, string cancel)
		{
			#if (UNITY_ANDROID && !UNITY_EDITOR)
			try {
				var plugin = new AndroidJavaClass ("appon.unitynativeplugin.nativeplugin");
				plugin.CallStatic ("ShowDialogPopup", gameObjectName, title, message, ok, cancel);
			} catch (System.Exception ex) {

			}
			#endif
		}
	}


}
