﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using AdsPlugin.Api;
using System;

public class AdsManager : MonoBehaviour,IAdListner
{

	private IAdsManager client;

	static int adCounter = 1;
	static int adFrequency = 1;
	static bool Alternate = true;

	static AdsManager insatnce;
	float AdShowTime;
	public static bool IS_AD_SHOWN = false;
	public static AdsManager Insatnce {
		get {
			if (insatnce == null) {
				insatnce = (AdsManager)FindObjectOfType (typeof(AdsManager));

				if (FindObjectsOfType (typeof(AdsManager)).Length > 1) {
					Debug.LogError ("[Singleton] Something went really wrong " +
					" - there should never be more than 1 singleton!" +
					" Reopening the scene might fix it.");
					DontDestroyOnLoad (insatnce.gameObject);
					return insatnce;
				}

				if (insatnce == null) {
					GameObject singleton = new GameObject ();
					insatnce = singleton.AddComponent<AdsManager> ();
					singleton.name = "(singleton) " + typeof(AdsManager).ToString ();

					DontDestroyOnLoad (singleton);

					Debug.Log ("[Singleton] An instance of " + typeof(AdsManager) +
					" is needed in the scene, so '" + singleton +
					"' was created with DontDestroyOnLoad.");
				} else {
					Debug.Log ("[Singleton] Using instance already created: " +
					insatnce.gameObject.name);
					DontDestroyOnLoad (insatnce.gameObject);
				}
			}
			return	insatnce;
		}
	}

	void Awake ()
	{
		MasterController.Insatnce.InitDone += InitilizedSuccess;
		MasterController.Insatnce.InitFail += InitilizedFail;

	}

	void Load (string adType = "")
	{
		try {
			client.LoadAd ();
		} catch (System.Exception ex) {
			
		}

	}

	public void Show ()
	{
		Debug.Log ((Time.time) + "<" + AdShowTime);
		if ((Time.time) < AdShowTime) {
			Debug.Log ("Dealy to show");
			return;
		}


		try {
			adCounter++;
			if (client == null) {
				client = GetAdapter ();
				client.OnAdLoadedFailed += AdLoadedFail;
				client.OnBtnClickedEvent += AdClosed;
				client.OnNoAdsAvailble += AdsNotAvailable;
				client.OnAdShowed += AdShown;
				client.OnLoadAd += AdLoad;
				client.OnAdLoadedSuccessfully += AdLoadedSuccess;
				client.OnAdClosedDone += AdClosed;
				Load ();
				return;
			} else if (!client.IsLoaded ()) {
				Load ();
				return;
			} else {
				if (adFrequency <= MasterController.AdInfo.Frequency) {
					adFrequency++;
					AdShowTime = Time.time + MasterController.AdInfo.Delay;
					client.ShowAd ();

				}
			}

		} catch (System.Exception ex) {

		}
	}



	IAdsManager GetAdapter ()
	{
		if (MasterController.AdInfo.Alternate) {
			if (Alternate) {
				Alternate = false;
				if ((MasterController.AdInfo.StartFrom - 1) <= adCounter) {
					client = InHouseAdsAdapter.Instance;
				}
				return client;
			} else {
				client = null;
//				client = new InterstitialAd ("");
				Alternate = true;
				return client;
			}
		}

		if (MasterController.AdInfo.ShowHouseAds) {
			if ((MasterController.AdInfo.StartFrom - 1) <= adCounter) {
				client = InHouseAdsAdapter.Instance;
			}
			return client;
		} else if (MasterController.AdInfo.ShowNormalAds) {
//			client = new InterstitialAd ("");
			client = null;
			return client;
		}


		return client;
	}


	public void Initilize ()
	{
		MasterController.Insatnce.Initilize ();

	}


	#region IAdListner implementation

	public event Action OnInitilizedSuccess;

	public event Action OnInitilizedFail;

	public event Action<string> OnAdLoad;

	public event Action<string> OnAdLoadedSuccessfully;

	public event Action<string> OnAdLoadedFailed;

	public event Action<string> OnAdClosed;

	public event Action<string> OnAdClicked;

	public event Action<string> OnAdShowed;

	public event Action<string> OnNoAdsAvailble;

	public void InitilizedSuccess ()
	{
		if (OnInitilizedSuccess != null) {
			OnInitilizedSuccess ();
		}
		client = GetAdapter ();
		try {
			client.OnAdLoadedFailed += AdLoadedFail;
			client.OnBtnClickedEvent += AdClosed;
			client.OnNoAdsAvailble += AdsNotAvailable;
			client.OnAdShowed += AdShown;
			client.OnLoadAd += AdLoad;
			client.OnAdLoadedSuccessfully += AdLoadedSuccess;
			client.OnAdClosedDone += AdClosed;
			Load ();
		} catch (Exception ex) {

		}
	}

	public void InitilizedFail ()
	{
		if (OnInitilizedFail != null) {
			OnInitilizedFail ();
		}
	}

	public void AdLoad (string adType)
	{
		if (OnAdLoad != null) {
			OnAdLoad (adType);
		}
	}

	public void AdLoadedSuccess (string adType)
	{
		if (OnAdLoadedSuccessfully != null) {
			OnAdLoadedSuccessfully (adType);
		}
	}

	public void AdLoadedFail (string adType)
	{
		if (OnAdLoadedFailed != null) {
			OnAdLoadedFailed (adType);
		}
	}

	public void AdClosed (string adType)
	{
		if (OnAdClosed != null) {
			OnAdClosed (adType);
		}
		client = GetAdapter ();
		Load ();
	}

	public void AdClicked (string adType)
	{
		if (OnAdClicked != null) {
			OnAdClicked (adType);
		}

	}

	public void AdShown (string AdType)
	{
		if (OnAdShowed != null) {
			OnAdShowed (AdType);
		}
	}

	public void AdsNotAvailable (string adType)
	{
		if (OnNoAdsAvailble != null) {
			OnNoAdsAvailble (adType);
		}
	}

	#endregion

}

