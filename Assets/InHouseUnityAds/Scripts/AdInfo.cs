﻿using System.Collections.Generic;
using System.Xml.Serialization;

public class AdInfo {

	private static  AdInfo instance;

	public static  AdInfo Instance {
		get {
			if(instance==null){
				instance=new AdInfo();
			}
			return instance;
		}
	}

	public List<AdInfoButtonEvents> events;

	public AdInfo(){
		events=new List<AdInfoButtonEvents>();
	}
}


