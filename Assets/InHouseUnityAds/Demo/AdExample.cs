﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class AdExample : MonoBehaviour {
	void OnEnable () {
		AdsManager.Insatnce.OnInitilizedSuccess+=Init;
		AdsManager.Insatnce.OnInitilizedFail+=InitFail;
		AdsManager.Insatnce.OnAdLoad+=LoadAd;
		AdsManager.Insatnce.OnAdLoadedSuccessfully+=AdLoadedSucc;
		AdsManager.Insatnce.OnAdLoadedFailed+=AdLoadedFail;
		AdsManager.Insatnce.OnAdClosed+=AdClose;
		AdsManager.Insatnce.OnAdClicked+=BtnClicked;
		AdsManager.Insatnce.OnAdShowed+=AdShow;
		AdsManager.Insatnce.OnAdLoadedFailed+=AdFailToLoad;
		AdsManager.Insatnce.OnNoAdsAvailble+=AdNotAvailable;
	}
//
	void OnDisable(){
		AdsManager.Insatnce.OnInitilizedSuccess-=Init;
		AdsManager.Insatnce.OnInitilizedFail-=InitFail;
		AdsManager.Insatnce.OnAdLoad-=LoadAd;
		AdsManager.Insatnce.OnAdLoadedSuccessfully-=AdLoadedSucc;
		AdsManager.Insatnce.OnAdLoadedFailed-=AdLoadedFail;
		AdsManager.Insatnce.OnAdClosed-=AdClose;
		AdsManager.Insatnce.OnAdClicked-=BtnClicked;
		AdsManager.Insatnce.OnAdShowed-=AdShow;
		AdsManager.Insatnce.OnAdLoadedFailed-=AdFailToLoad;
		AdsManager.Insatnce.OnNoAdsAvailble-=AdNotAvailable;
	}
		void OnGUI ()
		{
			if (GUI.Button (new Rect (0, 0, 200, 100), "init")) {
				AdsManager.Insatnce.Initilize();

			}
//	
			if (GUI.Button (new Rect (0, 150, 200, 100), "Show")) {
				AdsManager.Insatnce.Show();

				
			}

//		GUI.Box(new Rect(0,250,Screen.width,Screen.height-250),status);
		}
	string status;
	void Init(){
		status="Init successfull";
		Debug.Log("Init successfull");
	}

	void InitFail(){
		status="Init Fail";
		Debug.Log("Init Fail ");
	}
	void AdClose(string adtype){
		status="Close event called : "+adtype;
		Debug.Log("Close event called : "+adtype);
	}
	void AdLoadedSucc(string type){
		status="Ad Loaded Successfully : "+type;
		Debug.Log("Ad Loaded Successfully : "+type);
	}
	void AdLoadedFail(string type){
		status="Ad Loaded Failed : "+type;
		Debug.Log("Ad Loaded Failed : "+type);
	}

	void AdNotAvailable(string type){
		status="Ad Not avaliable Failed : "+type;
		Debug.Log("Ad Not avaliable Failed : "+type);
	}

	void LoadAd(string adtype){
		status="LoadAd event called : "+adtype;
		Debug.Log("LoadAd event called : "+adtype);
	}

	void AdLoaded(string adtype){
		status="AdLoaded event called : "+adtype;
		Debug.Log("AdLoaded event called : "+adtype);
	}

	void AdFailToLoad(string adtype){
		status="AdFailToLoad event called"+adtype;
		Debug.Log("AdFailToLoad event called");
	}

	void AdShow(string adType){
		status="AdShow event called--"+adType;
		Debug.Log("AdShow event called--"+adType);
	}

	void BtnClicked(string type){
		status="BtnClicked event called :"+type;
		Debug.Log("BtnClicked event called +"+type);
	}

}
