using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class SetCanvasBounds : MonoBehaviour {

	#if UNITY_ANDROID
	public GameObject avatarObject;
	#endif
	#if UNITY_IOS
	[DllImport("__Internal")]
    private extern static void GetSafeAreaImpl(out float x, out float y, out float w, out float h);
#endif

	private Rect GetSafeArea()
	{
		float x, y, w, h;
	#if UNITY_IOS
		GetSafeAreaImpl(out x, out y, out w, out h);
	#elif UNITY_ANDROID
			x = 0;
			y = 0;
			w = Screen.width;
			h = Screen.height;
	#else
		x = 0;
		y = 0;
		w = Screen.width;
		h = Screen.height;
	#endif
		return new Rect(x, y, w, h);
	}

	public RectTransform canvas;
	public RectTransform[] panel;
	Rect lastSafeArea = new Rect(0, 0, 0, 0);
	public GameObject[] iPhoneXObj;
	public GameObject[] iPhoneXObj_Low_Scale;
	public GameObject[] iPhoneXObj_Y_postion;
	public GameObject shopScreen;
	public GameObject loading_text;
	public GameObject splashScreen;
	public GameObject playButton;
	public GameObject OfferSaleAnimator;
	public static float aspectRatio ;
	// Use this for initialization
	void Start () {
		aspectRatio = ((float)Screen.height / (float)Screen.width);
		#if UNITY_IOS 
		OfferSaleAnimator.GetComponent <Animator>().enabled=false;
		if ((Screen.width == 1125 && Screen.height == 2436)) {
		Debug.Log ("iPhone X detected!");
		portUI();
		}
		if ((Screen.width == 1125 && Screen.height == 2436)||
		(Screen.width ==1334 && Screen.height == 750)) {
			Vector3 temp = splashScreen.transform.position;
			splashScreen.transform.position = new Vector3 (temp.x, temp.y - 80, temp.z);

			temp = playButton.transform.position;
			playButton.transform.position=new Vector3(temp.x,temp.y+30,temp.z);
		}
		#elif UNITY_ANDROID
		OfferSaleAnimator.GetComponent <Animator>().enabled=true;
		if (aspectRatio==2f) {
			Camera.main.orthographicSize=450;
			portUI();
		}
		#endif
	}
	void portUI()
	{
		
		float portPerc = 0.8f;
		for (int i = 0; i < iPhoneXObj.Length; i++) {
			if(iPhoneXObj [i]!=null && iPhoneXObj [i].transform!=null & iPhoneXObj [i].transform.localScale!=null)
				iPhoneXObj [i].transform.localScale = new Vector3 (iPhoneXObj [i].transform.localScale.x*portPerc,iPhoneXObj [i].transform.localScale.y*portPerc,iPhoneXObj [i].transform.localScale.z*portPerc);

		}
		portPerc = 0.85f;
		for (int i = 0; i < iPhoneXObj_Low_Scale.Length; i++) {
			if(iPhoneXObj_Low_Scale [i]!=null && iPhoneXObj_Low_Scale [i].transform!=null & iPhoneXObj_Low_Scale [i].transform.localScale!=null)
				iPhoneXObj_Low_Scale [i].transform.localScale = new Vector3 (iPhoneXObj_Low_Scale [i].transform.localScale.x*portPerc,iPhoneXObj_Low_Scale [i].transform.localScale.y*portPerc,iPhoneXObj_Low_Scale [i].transform.localScale.z*portPerc);
		}
		#if UNITY_IOS
		portPerc = 40f;
		for (int i = 0; i < iPhoneXObj_Y_postion.Length; i++) {
			if(iPhoneXObj_Y_postion [i]!=null && iPhoneXObj_Y_postion [i].transform!=null & iPhoneXObj_Y_postion [i].transform.localPosition!=null)
				iPhoneXObj_Y_postion [i].transform.localPosition = new Vector3 (
					iPhoneXObj_Y_postion [i].transform.localPosition.x
					,iPhoneXObj_Y_postion [i].transform.localPosition.y+portPerc,
					iPhoneXObj_Y_postion [i].transform.localPosition.z);
		}
		portPerc = 40f;
		loading_text.transform.localPosition = new Vector3 (
			loading_text.transform.localPosition.x
			,loading_text.transform.localPosition.y-portPerc,
			loading_text.transform.localPosition.z);
		#endif

		shopScreen.transform.localScale=new Vector3(1.24f,1.24f,1.24f);

	}
	void ApplySafeArea(Rect area)
	{
		var anchorMin = area.position;
		var anchorMax = area.position + area.size;
		anchorMin.x /= Screen.width;
		anchorMin.y /= Screen.height;
		anchorMax.x /= Screen.width;
		anchorMax.y /= Screen.height;
		for (int i = 0; i < panel.Length; i++) {
			panel[i].anchorMin = anchorMin;
			panel[i].anchorMax = anchorMax;
		}

		lastSafeArea = area;
	}

	// Update is called once per frame
	void Update () 
	{
		#if !UNITY_EDITOR
		Rect safeArea = GetSafeArea(); // or Screen.safeArea if you use a version of Unity that supports it
		if (safeArea != lastSafeArea)
			ApplySafeArea(safeArea);
		#endif
	}
}
